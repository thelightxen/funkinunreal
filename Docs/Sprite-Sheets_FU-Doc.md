# How to Setup your Sprite Sheets

### Extract Sprites #1:

- Import your sprite sheets as a texture, click by RMB on the asset and choose `Sprite Actions > Extract Sprites` from the context menu.
- Select `Sprite Extract Mode: Auto`.
- yaaaaaaaaaaaaaay.

### Extract Sprites #2 (if `auto` mode very crookedly):

- Import your sprite sheets as a texture, click by RMB on the asset and choose `Sprite Actions > Create Sprite` from the context menu.
- Do this as many times as you have SubTextures in the XML file.

***

### Configuring Sprites:
- In the Engine u have:
    - `Source UV`;
    - `Source Dimension`;
    - `Pivot Mode`;
    - `Source Texture Dimension` (in Advanced).
- In the Adobe Animate XML File u have:
    - `x`, `y`;
    - `width`, `height`;
    - `frameX`, `frameY`;
    - `frameWidth`, `frameheight`.

> soooo... 

- Source UV = `x`, `y`.
- Source Dimension = `width`, `height`.
- `Pivot Mode` = `Pivot Mode: Left Top` - `frameX`, `frameY`.

##### Change Pivot:

- Set `Pivot Mode` to `Left Top`, then to `Custom`. You got coordinates from `Left Top` mode.
- Then **subtract/add** `frameX` and `frameY` **from/to** `X` and `Y` (`Custom Pivot Mode` coordinates).

##### Example: 
1. `798,0` � `3` ----------- `X` = `798,0`, `frameX` = `-3`.
2. `798,0` + `3` ----------- `X` = `798,0`, `frameX` = `3`.

***

### Bugs with Sprite Editor (thx Unreal):

> Sometimes the Sprite Viewport can freeze when editing the Source UV.
> And when you change the coordinates, nothing changes.
>> **It's better to do this procedure always after editing the coordinates.**

- After you finish entering the coordinates, **subtract** `1` from `X` and `Y`, press `Enter`, and then **add** `99` after the `decimal point` and press `Enter` again.


##### Example: 
1. `798,0` � `1` = `797,0` + `0,99` = `798,0`.



// Copyright 2021 RLoris

#include "XmlLoader.h"
#include "HAL/PlatformFilemanager.h"
#include "HAL/FileManager.h"
#include "Misc/FileHelper.h"
#include "Runtime/Online/HTTP/Public/HttpModule.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"
#include "TimerManager.h"

UXmlLoader* UXmlLoader::LoadXml(UObject* WorldContextObject, const FXmlLoaderOptions& Options)
{
	UXmlLoader* Node = NewObject<UXmlLoader>();
	Node->Active = false;
	Node->WorldContextObject = WorldContextObject;
	Node->Options = Options;
	// not garbage collected
	Node->AddToRoot();
	return Node;
}

void UXmlLoader::Activate()
{
	if (nullptr == this->WorldContextObject)
	{
		FFrame::KismetExecutionMessage(TEXT("Invalid WorldContextObject. Cannot execute XmlLoader"), ELogVerbosity::Error);
		this->_Failed("Invalid WorldContextObject. Cannot execute XmlLoader");
		return;
	}
	if (this->Active)
	{
		FFrame::KismetExecutionMessage(TEXT("XmlLoader is already running, wait for completion before calling again"), ELogVerbosity::Warning);
		// this->_Failed("JsonLoader is already running, wait for completion");
		return;
	}
	this->Active = true;
	switch (this->Options.Source)
	{
	case(EXmlLoaderSource::BUFFER):
		this->LoadXmlFromBuffer(this->Options.From);
		break;
	case(EXmlLoaderSource::FILE):
		this->LoadXmlFromFile();
		break;
	case(EXmlLoaderSource::URL):
		this->LoadXmlFromUrl();
		break;
	}
}

void UXmlLoader::LoadXmlFromFile()
{
	IPlatformFile& file = FPlatformFileManager::Get().GetPlatformFile();
	if (file.FileExists(*this->Options.From))
	{
		FString Output;
		if (FFileHelper::LoadFileToString(Output, *this->Options.From))
		{
			this->LoadXmlFromBuffer(Output);
		}
		else
		{
			this->_Failed("XmlLoader: Could not load text content of file : " + this->Options.From);
		}
	}
	else
	{
		this->_Failed("XmlLoader: Invalid filepath provided : " + this->Options.From);
	}
}

void UXmlLoader::LoadXmlFromBuffer(const FString& Buffer)
{
	bool Result;
	UXmlNodeWrapper* Value = UXmlNodeWrapper::Parse(Buffer, Result);
	if (Result)
	{
		this->_Completed(Value);
	}
	else
	{
		this->_Failed("XmlLoader: Could not parse string into XmlNode");
	}
}

void UXmlLoader::LoadXmlFromUrl()
{
	auto Request = FHttpModule::Get().CreateRequest();
	Request->SetVerb("GET");
	Request->AppendToHeader("Accept", "text/xml");
	Request->AppendToHeader("Accept", "application/xml");
	Request->SetURL(this->Options.From);
	FTimerHandle Handle;
	if (this->WorldContextObject != nullptr)
	{
		this->WorldContextObject->GetWorld()->GetTimerManager().SetTimer(Handle, [Request, this]()
			{
				if (Request->GetStatus() == EHttpRequestStatus::Processing)
				{
					Request->CancelRequest();
					this->_Failed("XmlLoader: Http request timed out");
				}
			}
		, 5, false, (-1.0f));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("XmlLoader: WorldContext is invalid, cannot set timeout for http request but still performing the request"));
	}
	Request->OnProcessRequestComplete().BindLambda([this](FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSuccess)
		{
			if (!HttpRequest.IsValid() || !HttpResponse.IsValid())
			{
				FFrame::KismetExecutionMessage(TEXT("XmlLoader: Request or response is invalid, cannot complete http request"), ELogVerbosity::Error);
				this->_Failed("XmlLoader: Request or response is invalid, cannot complete http request");
				return;
			}
			if (bSuccess)
			{
				this->LoadXmlFromBuffer(HttpResponse->GetContentAsString());
			}
			else
			{
				this->_Failed("XmlLoader: Http request failed with error code : " + FString::FromInt(HttpResponse->GetResponseCode()));
			}
		});
	Request->ProcessRequest();
}

void UXmlLoader::_Completed(UXmlNodeWrapper* XmlNode)
{
	if (this->Completed.IsBound())
	{
		this->Completed.Broadcast(XmlNode, FString());
	}
	this->Active = false;
	this->RemoveFromRoot();
}

void UXmlLoader::_Failed(const FString& ErrorReason)
{
	UE_LOG(LogTemp, Warning, TEXT("XmlLoader: Failed reason : %s "), *ErrorReason);
	if (this->Failed.IsBound())
	{
		this->Failed.Broadcast(nullptr, ErrorReason);
	}
	this->Active = false;
	this->RemoveFromRoot();
}

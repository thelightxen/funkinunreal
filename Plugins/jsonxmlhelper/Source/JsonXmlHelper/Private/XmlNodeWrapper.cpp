// Copyright 2021 RLoris

#include "XmlNodeWrapper.h"
#include "UObject/TextProperty.h"

UXmlNodeWrapper* UXmlNodeWrapper::CreateXmlNode(FString TagName, bool& Valid)
{
	Valid = false;
	if (!UXmlNodeWrapper::IsValidTagName(TagName))
	{
		return nullptr;
	}
	Valid = true;
	auto Wrapper = NewObject<UXmlNodeWrapper>();
	Wrapper->Tag = TagName;
	return Wrapper;
}

UXmlNodeWrapper* UXmlNodeWrapper::GetRootNode()
{
	if (this->Parent == nullptr)
	{
		return this;
	}
	else
	{
		return this->Parent->GetRootNode();
	}
}

bool UXmlNodeWrapper::IsAttached(UXmlNodeWrapper* Other)
{
	return Other->GetRootNode() == this->GetRootNode();
}

TSharedRef<FJsonValue> UXmlNodeWrapper::XmlToCustom(FProperty* Property, UXmlNodeWrapper* Node)
{
	if (!Property || !Node)
	{
		return MakeShareable(new FJsonValueNull());
	}
	else if (FArrayProperty* arrayProperty = CastField<FArrayProperty>(Property))
	{
		TArray<TSharedPtr<FJsonValue>> Array;
		TArray< UXmlNodeWrapper*> Children;
		Node->GetChildren(Children);
		for (int ArrayIndex = 0; ArrayIndex < Children.Num(); ArrayIndex++)
		{
			Array.Add(UXmlNodeWrapper::XmlToCustom(arrayProperty->Inner, Children[ArrayIndex]));
		}
		return MakeShareable(new FJsonValueArray(Array));
	}
	else if (FSetProperty* setProperty = CastField<FSetProperty>(Property))
	{
		TArray<TSharedPtr<FJsonValue>> Array;
		TArray< UXmlNodeWrapper*> Children;
		Node->GetChildren(Children);
		for (int ArrayIndex = 0; ArrayIndex < Children.Num(); ArrayIndex++)
		{
			Array.Add(UXmlNodeWrapper::XmlToCustom(setProperty->ElementProp, Children[ArrayIndex]));
		}
		return MakeShareable(new FJsonValueArray(Array));
	}
	else if (FMapProperty* mapProperty = CastField<FMapProperty>(Property))
	{
		TSharedRef<FJsonObject> Object = MakeShareable<FJsonObject>(new FJsonObject());
		TArray< UXmlNodeWrapper*> Children;
		Node->GetChildren(Children);
		for (auto Child : Children)
		{
			if (auto Key = Child->FindChild("key"))
			{
				if (auto Val = Child->FindChild("value"))
				{
					Object->SetField(Key->GetContent(), UXmlNodeWrapper::XmlToCustom(mapProperty->ValueProp, Val));
				}
			}
		}
		return MakeShareable(new FJsonValueObject(Object));
	}
	else if (FStructProperty* structProperty = CastField<FStructProperty>(Property))
	{
		TSharedRef<FJsonObject> Object = MakeShareable<FJsonObject>(new FJsonObject());
		for (TFieldIterator<FProperty> It(structProperty->Struct);It;++It)
		{
			FProperty* Prop = *It;
			UXmlNodeWrapper* Result;
			if ((Result = Node->FindChild(Prop->GetAuthoredName())))
			{
				Object->SetField(Prop->GetName(), UXmlNodeWrapper::XmlToCustom(Prop, Result));
			}
			else if ((Result = Node->FindChild(Prop->GetName())))
			{
				Object->SetField(Prop->GetName(), UXmlNodeWrapper::XmlToCustom(Prop, Result));
			}
		}
		return MakeShareable(new FJsonValueObject(Object));
	}
	else if (FObjectProperty* objectProperty = CastField<FObjectProperty>(Property))
	{
		FString Content = Node->GetContent();
		TArray< UXmlNodeWrapper*> Children;
		Node->GetChildren(Children);
		if (Content.IsEmpty() && Children.Num() == 0)
		{
			return MakeShareable(new FJsonValueNull());
		}
		if (objectProperty->PropertyClass->IsNative())
		{
			return MakeShareable(new FJsonValueString(Content));
		}
		TSharedRef<FJsonObject> Object = MakeShared<FJsonObject>();
		for (TFieldIterator<FProperty> It(objectProperty->PropertyClass); It; ++It)
		{
			FProperty* Prop = *It;
			UXmlNodeWrapper* Result;
			if ((Result = Node->FindChild(Prop->GetAuthoredName())))
			{
				Object->SetField(Prop->GetName(), UXmlNodeWrapper::XmlToCustom(Prop, Result));
			}
			else if ((Result = Node->FindChild(Prop->GetName())))
			{
				Object->SetField(Prop->GetName(), UXmlNodeWrapper::XmlToCustom(Prop, Result));
			}
		}
		return MakeShareable(new FJsonValueObject(Object));
	}
	// scalar
	else if (FBoolProperty* BoolProperty = CastField<FBoolProperty>(Property))
	{
		return MakeShareable<FJsonValueBoolean>(new FJsonValueBoolean((Node->GetContent().Equals("true", ESearchCase::IgnoreCase) ? true : false)));
	}
	else if (FNumericProperty* NumericProperty = CastField<FNumericProperty>(Property))
	{
		return MakeShareable<FJsonValueNumber>(new FJsonValueNumber(FCString::Atod(*Node->GetContent())));
	}
	else if (FStrProperty* StringProperty = CastField<FStrProperty>(Property))
	{
		return MakeShareable<FJsonValueString>(new FJsonValueString(UXmlNodeWrapper::ConvertContent(Node->GetContent())));
	}
	else
	{
		// other
		return MakeShareable<FJsonValueString>(new FJsonValueString(UXmlNodeWrapper::ConvertContent(Node->GetContent())));
	}
}

UXmlNodeWrapper* UXmlNodeWrapper::CustomToXml(FProperty* Property, void* ValuePtr)
{
	if (ValuePtr == NULL || Property == NULL)
	{
		return nullptr;
	}
	// array
	else if (FArrayProperty* arrayProperty = CastField<FArrayProperty>(Property))
	{
		auto Node = NewObject<UXmlNodeWrapper>();
		Node->Tag = Property->GetAuthoredName();
		auto Helper = FScriptArrayHelper::CreateHelperFormInnerProperty(arrayProperty->Inner, ValuePtr);
		for (int32 ArrayIndex = 0; ArrayIndex < Helper.Num(); ArrayIndex++)
		{
			auto Child = UXmlNodeWrapper::CustomToXml(arrayProperty->Inner, Helper.GetRawPtr(ArrayIndex));
			if (Child)
			{
				Child->Tag = "value";
			}
			Node->AddChild(Child, Node);
		}
		return Node;
	}
	// set
	else if (FSetProperty* setProperty = CastField<FSetProperty>(Property))
	{
		auto Node = NewObject<UXmlNodeWrapper>();
		Node->Tag = Property->GetAuthoredName();
		auto Helper = FScriptSetHelper::CreateHelperFormElementProperty(setProperty->ElementProp, ValuePtr);
		for (int32 ArrayIndex = 0; ArrayIndex < Helper.Num(); ++ArrayIndex)
		{
			auto Child = UXmlNodeWrapper::CustomToXml(setProperty->ElementProp, Helper.GetElementPtr(ArrayIndex));
			if (Child)
			{
				Child->Tag = "item";
			}
			Node->AddChild(Child, Node);
		}
		return Node;
	}
	// map 
	else if (FMapProperty* mapProperty = CastField<FMapProperty>(Property))
	{
		auto Node = NewObject<UXmlNodeWrapper>();
		Node->Tag = Property->GetAuthoredName();
		auto Helper = FScriptMapHelper::CreateHelperFormInnerProperties(mapProperty->KeyProp, mapProperty->ValueProp, ValuePtr);
		for (int32 ArrayIndex = 0; ArrayIndex < Helper.Num(); ++ArrayIndex)
		{
			auto Child = NewObject<UXmlNodeWrapper>();
			Child->Tag = "item";
			auto Key = UXmlNodeWrapper::CustomToXml(mapProperty->KeyProp, Helper.GetKeyPtr(ArrayIndex));
			if (Key)
			{
				Key->Tag = "key";
			}
			Child->AddChild(Key, Child);
			auto Value = UXmlNodeWrapper::CustomToXml(mapProperty->ValueProp, Helper.GetValuePtr(ArrayIndex));
			if (Value)
			{
				Value->Tag = "value";
			}
			Child->AddChild(Value, Child);
			Node->AddChild(Child, Node);
		}
		return Node;
	}
	// struct
	else if (FStructProperty* structProperty = CastField<FStructProperty>(Property))
	{
		auto Node = NewObject<UXmlNodeWrapper>();
		Node->Tag = Property->GetAuthoredName();
		for (TFieldIterator<FProperty> It(structProperty->Struct); It; ++It)
		{
			FProperty* Prop = *It;
			auto Child = UXmlNodeWrapper::CustomToXml(Prop, Prop->ContainerPtrToValuePtr<void*>(ValuePtr));
			if (Child)
			{
				Child->Tag = Prop->GetAuthoredName();
			}
			Node->AddChild(Child, Node);
		}
		return Node;
	}
	// object
	else if (FObjectProperty* objectProperty = CastField<FObjectProperty>(Property))
	{
		void* PropValue = objectProperty->GetObjectPropertyValue(ValuePtr);
		if (PropValue == NULL)
		{
			auto Node = NewObject<UXmlNodeWrapper>();
			Node->Tag = Property->GetAuthoredName();
			return Node;
		}
		if (objectProperty->PropertyClass->IsNative())
		{
			auto Node = NewObject<UXmlNodeWrapper>();
			Node->Tag = Property->GetAuthoredName();
			objectProperty->ExportTextItem(Node->Content, ValuePtr, nullptr, nullptr, 0, 0);
			return Node;
		}
		auto Node = NewObject<UXmlNodeWrapper>();
		Node->Tag = Property->GetAuthoredName();
		TSharedRef<FJsonObject> JsonObject = MakeShared<FJsonObject>();
		for (TFieldIterator<FProperty> It(objectProperty->PropertyClass); It; ++It)
		{
			FProperty* Prop = *It;
			auto Child = UXmlNodeWrapper::CustomToXml(Prop, Prop->ContainerPtrToValuePtr<void*>(PropValue));
			if (Child)
			{
				Child->Tag = Prop->GetAuthoredName();
			}
			Node->AddChild(Child, Node);
		}
		return Node;
	}
	// text
	else if (FTextProperty* textProperty = CastField<FTextProperty>(Property))
	{
		auto Node = NewObject<UXmlNodeWrapper>();
		Node->Tag = Property->GetAuthoredName();
		Node->Content = textProperty->GetPropertyValue(ValuePtr).ToString();
		return Node;
	}
	// scalar
	else
	{
		auto Node = NewObject<UXmlNodeWrapper>();
		Node->Tag = Property->GetAuthoredName();
		Property->ExportTextItem(Node->Content, ValuePtr, nullptr, nullptr, 0, 0);
		return Node;
	}
}

FString UXmlNodeWrapper::Stringify(int32 Depth)
{
	FString Inner;
	for (auto Child : this->Children)
	{
		Inner += Child->Stringify((Depth > -1) ? (Depth + 1) : Depth);
	}
	Inner += UXmlNodeWrapper::EscapeContent(this->Content);
	return UXmlNodeWrapper::CreateTagNode(this->Tag, Inner, this->Attributes, Depth, (Depth > -1) && !this->IsLeafNode());
}

UXmlNodeWrapper* UXmlNodeWrapper::CreateXmlNode(UXmlNodeWrapper* Parent, FXmlNode* Node)
{
	auto Wrapper = NewObject<UXmlNodeWrapper>();
	Wrapper->Parent = Parent;
	Wrapper->Content = Node->GetContent();
	Wrapper->Tag = Node->GetTag();
	for (auto Attribute : Node->GetAttributes())
	{
		Wrapper->Attributes.Add(Attribute.GetTag(), Attribute.GetValue());
	}
	for (auto Child : Node->GetChildrenNodes())
	{
		Wrapper->Children.Add(UXmlNodeWrapper::CreateXmlNode(Wrapper, Child));
	}
	return Wrapper;
}

UXmlNodeWrapper* UXmlNodeWrapper::Parse(const FString& XmlString, bool& Success)
{
	Success = false;
	FXmlFile* File = new FXmlFile();
	if (File->LoadFile(XmlString, EConstructMethod::ConstructFromBuffer) && File->IsValid())
	{
		auto Root = File->GetRootNode();
		if (Root != nullptr)
		{
			Success = true;
			return UXmlNodeWrapper::CreateXmlNode(nullptr, Root);
		}
	}
	File->Clear();
	delete File;
	return nullptr;
}

UXmlNodeWrapper::~UXmlNodeWrapper()
{
}

bool UXmlNodeWrapper::IsRootNode()
{
	return this->Parent == nullptr;
}

bool UXmlNodeWrapper::IsLeafNode()
{
	return this->Children.Num() == 0;
}

bool UXmlNodeWrapper::GetParent(UXmlNodeWrapper*& Node)
{
	Node = this->Parent;
	return this->Parent != nullptr;
}

void UXmlNodeWrapper::GetChildren(TArray<UXmlNodeWrapper*>& Out)
{
	Out = this->Children;
}

int32 UXmlNodeWrapper::GetChildrenCount()
{
	return this->Children.Num();
}

void UXmlNodeWrapper::GetChildrenTags(TArray<FString>& Tags)
{
	for (auto Child : this->Children)
	{
		Tags.Add(Child->GetTag());
	}
}

bool UXmlNodeWrapper::GetAttribute(FString Name, FString& Value)
{
	auto Attr = this->Attributes.Find(Name);
	if (Attr)
	{
		Value = (*Attr);
	}
	return Attr != nullptr;
}

void UXmlNodeWrapper::GetAttributes(TMap<FString, FString>& Values)
{
	Values = this->Attributes;
}

FString UXmlNodeWrapper::GetTag()
{
	return this->Tag;
}

FString UXmlNodeWrapper::GetContent()
{
	return this->Content;
}

bool UXmlNodeWrapper::GetFirstChild(UXmlNodeWrapper*& Child)
{
	if (this->Children.Num() > 0)
	{
		Child = this->Children[0];
		return true;
	}
	return false;
}

bool UXmlNodeWrapper::GetLastChild(UXmlNodeWrapper*& Child)
{
	if (this->Children.Num() > 0)
	{
		Child = this->Children.Last();
		return true;
	}
	return false;
}

bool UXmlNodeWrapper::GetNext(UXmlNodeWrapper*& Next)
{
	if (this->Parent)
	{
		int32 Index = -1;
		if (this->Parent->Children.Find(this, Index))
		{
			Index++;
			if (this->Parent->Children.IsValidIndex(Index))
			{
				Next = this->Parent->Children[Index];
				return true;
			}
		}
	}
	return false;
}

bool UXmlNodeWrapper::GetPrev(UXmlNodeWrapper*& Prev)
{
	if (this->Parent)
	{
		int32 Index = -1;
		if (this->Parent->Children.Find(this, Index))
		{
			Index--;
			if (this->Parent->Children.IsValidIndex(Index))
			{
				Prev = this->Parent->Children[Index];
				return true;
			}
		}
	}
	return false;
}

bool UXmlNodeWrapper::HasNode(FString Pattern)
{
	return this->GetNode(this, Pattern) != nullptr;
}

bool UXmlNodeWrapper::FindNode(FString Pattern, UXmlNodeWrapper*& Result)
{
	Result = this->GetNode(this, Pattern);
	return Result != nullptr;
}

void UXmlNodeWrapper::SetContent(FString NewContent, UXmlNodeWrapper*& This)
{
	This = this;
	this->Content = NewContent;
}

bool UXmlNodeWrapper::AddAttribute(FString Name, FString Value, UXmlNodeWrapper*& This)
{
	This = this;
	if (UXmlNodeWrapper::IsValidAttribute(Name))
	{
		this->Attributes.Add(Name, Value);
		return true;
	}
	return false;
}

bool UXmlNodeWrapper::SetTag(FString NewTagName, UXmlNodeWrapper*& This)
{
	This = this;
	if (UXmlNodeWrapper::IsValidTagName(NewTagName))
	{
		this->Tag = NewTagName;
		return true;
	}
	return false;
}

bool UXmlNodeWrapper::AddChild(UXmlNodeWrapper* const& Child, UXmlNodeWrapper*& This, int32 Index)
{
	This = this;
	if (Child && !Child->IsAttached(this))
	{
		if (Index == -1)
		{
			return (this->Children.Add(Child) >= 0);
		}
		else
		{
			return (this->Children.Insert(Child, Index) >= 0);
		}
	}
	return false;
}

bool UXmlNodeWrapper::AddNext(UXmlNodeWrapper* const& Next, UXmlNodeWrapper*& This)
{
	if (Next && !Next->IsAttached(this))
	{
		if (this->Parent)
		{
			auto Idx = this->Parent->Children.Find(this);
			if (Idx >= 0)
			{
				return (this->Parent->Children.Insert(Next, Idx + 1) >= 0);
			}
		}
	}
	return false;
}

bool UXmlNodeWrapper::AddPrev(UXmlNodeWrapper* const& Prev, UXmlNodeWrapper*& This)
{
	This = this;
	if (Prev && !Prev->IsAttached(this))
	{
		if (this->Parent)
		{
			auto Idx = this->Parent->Children.Find(this);
			if (Idx >= 0)
			{
				return (this->Parent->Children.Insert(Prev, FMath::Max(0, Idx - 1)) >= 0);
			}
		}
	}
	return false;
}

bool UXmlNodeWrapper::Detach(UXmlNodeWrapper*& This)
{
	This = this;
	if (this->Parent)
	{
		if (this->Parent->Children.Remove(this) > 0)
		{
			this->Parent = nullptr;
			return true;
		}
	}
	return false;
}

void UXmlNodeWrapper::Stringify(FString& XmlString, bool& Success, bool PrettyPrint)
{
	XmlString = TEXT("<?xml version='1.0' encoding='UTF-8' ?>") + this->Stringify(PrettyPrint ? 0 : -1);
	Success = !XmlString.IsEmpty();
}

UXmlNodeWrapper* UXmlNodeWrapper::FindChild(const FString& TagName)
{
	for (auto Child : Children)
	{
		if (Child->Tag.Equals(TagName))
		{
			return Child;
		}
	}
	return nullptr;
}

bool UXmlNodeWrapper::IsValidTagName(const FString& TagName)
{
	if (TagName.Len() == 0)
	{
		return false;
	}
	FString First = TagName.Left(1);
	if (First.IsNumeric() || First == TEXT("-") || First == TEXT("."))
	{
		return false;
	}
	auto IsInvalidTagChar = [](TCHAR C)
	{
		return !(((C >= 65) && (C <= 90)) || ((C >= 97) && (C <= 122)) || (C == 95) || (C == 45) || (C == 46));
	};

	return (INDEX_NONE == TagName.FindLastCharByPredicate(IsInvalidTagChar));
}

bool UXmlNodeWrapper::IsValidAttribute(const FString& Attribute)
{
	return UXmlNodeWrapper::IsValidTagName(Attribute);
}

FString UXmlNodeWrapper::EscapeAttribute(const FString& Value)
{
	FString Source = Value;
	Source.ReplaceInline(TEXT("&"), TEXT("&amp;"));
	Source.ReplaceInline(TEXT("<"), TEXT("&lt;"));
	Source.ReplaceInline(TEXT(">"), TEXT("&gt;"));
	Source.ReplaceInline(TEXT("\""), TEXT("&quot;"));
	Source.ReplaceInline(TEXT("'"), TEXT("&apos;"));
	return Source;
}

FString UXmlNodeWrapper::EscapeContent(const FString& TagName)
{
	FString Source = TagName;
	Source.ReplaceInline(TEXT("&"), TEXT("&amp;"));
	Source.ReplaceInline(TEXT("<"), TEXT("&lt;"));
	Source.ReplaceInline(TEXT(">"), TEXT("&gt;"));
	return Source;
}

FString UXmlNodeWrapper::ConvertAttribute(const FString& Value)
{
	FString Source = Value;
	Source.ReplaceInline(TEXT("&apos;"), TEXT("'"));
	Source.ReplaceInline(TEXT("&quot;"), TEXT("\""));
	Source.ReplaceInline(TEXT("&gt;"), TEXT(">"));
	Source.ReplaceInline(TEXT("&lt;"), TEXT("<"));
	Source.ReplaceInline(TEXT("&amp;"), TEXT("&"));
	return Source;
}

FString UXmlNodeWrapper::ConvertContent(const FString& Content)
{
	FString Source = Content;
	Source.ReplaceInline(TEXT("&gt;"), TEXT(">"));
	Source.ReplaceInline(TEXT("&lt;"), TEXT("<"));
	Source.ReplaceInline(TEXT("&amp;"), TEXT("&"));
	return Source;
}

FString UXmlNodeWrapper::CreateTagNode(const FString& Tag, const FString& Content, TMap<FString, FString> Attributes, int32 Depth, bool WithCR)
{
	FString Tab;
	if (Depth > -1)
	{
		for (int32 space = 0; space < (Depth * 2); space++)
		{
			Tab += " ";
		}
	}
	FString AttributesNode;
	for (auto Attr : Attributes)
	{
		AttributesNode += " " + Attr.Key + "=\"" + UXmlNodeWrapper::EscapeAttribute(Attr.Value) + "\"";
	}
	FString TagNode;
	if (Depth > -1)
	{
		TagNode = LINE_TERMINATOR;
	}
	TagNode += Tab + "<" + Tag + AttributesNode + ">" + Content;
	if (Depth > -1 && WithCR)
	{
		TagNode += LINE_TERMINATOR + Tab;
	}
	TagNode += "</" + Tag + ">";
	return TagNode;
}

UXmlNodeWrapper* UXmlNodeWrapper::GetNode(UXmlNodeWrapper* Current, const FString& Pattern)
{
	FString Start = Pattern;
	FString Rest;
	Pattern.Split("/", &Start, &Rest);
	if (Start.IsNumeric())
	{
		auto Arr = Current->Children;
		int32 Idx = FCString::Atoi(*Start);
		if (Idx > -1 && Idx < Arr.Num())
		{
			if (Rest.IsEmpty())
			{
				return Arr[Idx];
			}
			else
			{
				return this->GetNode(Arr[Idx], Rest);
			}
		}
	}
	else {
		UXmlNodeWrapper* Res = Current->FindChild(Start);
		if (Res)
		{
			if (Rest.IsEmpty())
			{
				return Res;
			}
			else
			{
				return this->GetNode(Res, Rest);
			}
		}
	}
	return nullptr;
}

// Copyright 2021 RLoris

#include "JsonLoader.h"
#include "HAL/PlatformFilemanager.h"
#include "HAL/FileManager.h"
#include "Misc/FileHelper.h"
#include "Runtime/Online/HTTP/Public/HttpModule.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"
#include "TimerManager.h"

UJsonLoader* UJsonLoader::LoadJson(UObject* WorldContextObject, const FJsonLoaderOptions& Options)
{
	UJsonLoader* Node = NewObject<UJsonLoader>();
	Node->Active = false;
	Node->WorldContextObject = WorldContextObject;
	Node->Options = Options;
	// not garbage collected
	Node->AddToRoot();
	return Node;
}

void UJsonLoader::Activate()
{
	if (nullptr == this->WorldContextObject)
	{
		FFrame::KismetExecutionMessage(TEXT("Invalid WorldContextObject. Cannot execute JsonLoader"), ELogVerbosity::Error);
		this->_Failed("Invalid WorldContextObject. Cannot execute JsonLoader");
		return;
	}
	if (this->Active)
	{
		FFrame::KismetExecutionMessage(TEXT("JsonLoader is already running, wait for completion before calling again"), ELogVerbosity::Warning);
		// this->_Failed("JsonLoader is already running, wait for completion");
		return;
	}
	this->Active = true;
	switch (this->Options.Source)
	{
		case(EJsonLoaderSource::BUFFER):
			this->LoadJsonFromBuffer(this->Options.From);
			break;
		case(EJsonLoaderSource::FILE):
			this->LoadJsonFromFile();
			break;
		case(EJsonLoaderSource::URL):
			this->LoadJsonFromUrl();
			break;
	}
}

void UJsonLoader::LoadJsonFromFile()
{
	IPlatformFile& file = FPlatformFileManager::Get().GetPlatformFile();
	if (file.FileExists(*this->Options.From))
	{
		FString Output;
		if (FFileHelper::LoadFileToString(Output, *this->Options.From))
		{
			this->LoadJsonFromBuffer(Output);
		}
		else
		{
			this->_Failed("JsonLoader: Could not load text content of file : " + this->Options.From);
		}
	}
	else
	{
		this->_Failed("JsonLoader: Invalid filepath provided : " + this->Options.From);
	}
}

void UJsonLoader::LoadJsonFromBuffer(const FString& Buffer)
{
	bool Result;
	UJsonValueWrapper* Value = UJsonValueWrapper::Parse(Buffer, Result);
	if (Result)
	{
		this->_Completed(Value);
	}
	else
	{
		this->_Failed("JsonLoader: Could not parse string into JsonValue");
	}
}

void UJsonLoader::LoadJsonFromUrl()
{
	auto Request = FHttpModule::Get().CreateRequest();
	Request->SetVerb("GET");
	Request->SetHeader("Accept", "application/json");
	Request->SetURL(this->Options.From);
	FTimerHandle Handle;
	if (this->WorldContextObject != nullptr)
	{
		this->WorldContextObject->GetWorld()->GetTimerManager().SetTimer(Handle, [Request, this]()
			{
				if (Request->GetStatus() == EHttpRequestStatus::Processing)
				{
					Request->CancelRequest();
					this->_Failed("JsonLoader: Http request timed out");
				}
			}
		, 5, false, (-1.0f));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("JsonLoader: WorldContext is invalid, cannot set timeout for http request but still performing the request"));
	}
	Request->OnProcessRequestComplete().BindLambda([this](FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSuccess)
		{
			if (!HttpRequest.IsValid() || !HttpResponse.IsValid())
			{
				FFrame::KismetExecutionMessage(TEXT("JsonLoader: Request or response is invalid, cannot complete http request"), ELogVerbosity::Error);
				this->_Failed("JsonLoader: Request or response is invalid, cannot complete http request");
				return;
			}
			if (bSuccess)
			{
				this->LoadJsonFromBuffer(HttpResponse->GetContentAsString());
			}
			else
			{
				this->_Failed("JsonLoader: Http request failed with error code : " + FString::FromInt(HttpResponse->GetResponseCode()));
			}
	});
	Request->ProcessRequest();
}

void UJsonLoader::_Completed(UJsonValueWrapper* JsonValue)
{
	if (this->Completed.IsBound())
	{
		this->Completed.Broadcast(JsonValue, FString());
	}
	this->Active = false;
	this->RemoveFromRoot();
}

void UJsonLoader::_Failed(const FString& ErrorReason)
{
	UE_LOG(LogTemp, Warning, TEXT("JsonLoader: Failed reason : %s "), *ErrorReason);
	if (this->Failed.IsBound())
	{
		this->Failed.Broadcast(nullptr, ErrorReason);
	}
	this->Active = false;
	this->RemoveFromRoot();
}

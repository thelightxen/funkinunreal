// Copyright 2021 RLoris

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

class FJsonXmlHelperModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};

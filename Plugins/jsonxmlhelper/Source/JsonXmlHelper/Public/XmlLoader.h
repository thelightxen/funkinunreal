// Copyright 2021 RLoris

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "XmlNodeWrapper.h"
#include "Engine/World.h"
#include "XmlLoader.generated.h"

UENUM(BlueprintType)
enum class EXmlLoaderSource : uint8
{
	FILE		UMETA(DisplayName = "LoadFromFile"),
	BUFFER		UMETA(DisplayName = "LoadFromString"),
	URL			UMETA(DisplayName = "LoadFromUrl")
};

USTRUCT(BlueprintType)
struct FXmlLoaderOptions {
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite, Category = "XML")
	EXmlLoaderSource Source = EXmlLoaderSource::BUFFER;
	UPROPERTY(BlueprintReadWrite, Category = "XML")
	FString From;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FXmlLoaderOutputPin, UXmlNodeWrapper*, XmlNode, const FString&, ErrorReason);
/**
 * 
 */
UCLASS()
class JSONXMLHELPER_API UXmlLoader : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintAssignable)
	FXmlLoaderOutputPin Completed;
	UPROPERTY(BlueprintAssignable)
	FXmlLoaderOutputPin Failed;
public:
	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"), Category = "XML")
	static UXmlLoader* LoadXml(UObject* WorldContextObject, const FXmlLoaderOptions& Options);
	virtual void Activate() override;
private:
	void LoadXmlFromFile();
	void LoadXmlFromBuffer(const FString& Buffer);
	void LoadXmlFromUrl();
	void _Completed(UXmlNodeWrapper* XmlNode);
	void _Failed(const FString& ErrorReason);
private:
	const UObject* WorldContextObject = nullptr;
	bool Active = false;
	FXmlLoaderOptions Options;
};

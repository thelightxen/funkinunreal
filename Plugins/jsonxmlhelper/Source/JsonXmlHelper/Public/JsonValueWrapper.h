// Copyright 2021 RLoris

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "XmlFile.h"
#include "JsonObjectConverter.h"
#include "Serialization/JsonWriter.h"
#include "JsonValueWrapper.generated.h"

UENUM(BlueprintType)
enum class EJsonValueType : uint8
{
	None		UMETA(DisplayName = "None"),
	Null		UMETA(DisplayName = "Null"),
	String		UMETA(DisplayName = "String"),
	Number		UMETA(DisplayName = "Number"),
	Boolean		UMETA(DisplayName = "Boolean"),
	Array		UMETA(DisplayName = "Array"),
	Object		UMETA(DisplayName = "Object")
};
/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class JSONXMLHELPER_API UJsonValueWrapper : public UObject
{
	GENERATED_BODY()
private:
	TSharedPtr<FJsonValue> JsonValue;
public:
	UFUNCTION(BlueprintPure, Category = "JSON", CustomThunk, meta = (CustomStructureParam = "Value"))
	static void CreateJsonCustomValue(UJsonValueWrapper*& Out, const UStruct* Value);
	DECLARE_FUNCTION(execCreateJsonCustomValue)
	{
		P_GET_OBJECT_REF(UJsonValueWrapper, Out);
		Stack.Step(Stack.Object, NULL);

		FProperty* Prop = Stack.MostRecentProperty;
		void* Ptr = Stack.MostRecentPropertyAddress;

		P_FINISH;

		Out = UJsonValueWrapper::CreateJsonValue(UJsonValueWrapper::CustomToJsonValue(Prop, Ptr));
	}
	UFUNCTION(BlueprintPure, Category = "JSON")
	static UJsonValueWrapper* CreateJsonArrayValue(TArray<UJsonValueWrapper*> Values);
	UFUNCTION(BlueprintPure, Category = "JSON")
	static UJsonValueWrapper* CreateJsonObjectValue(TMap<FString, UJsonValueWrapper*> Values);
	UFUNCTION(BlueprintPure, Category = "JSON")
	static UJsonValueWrapper* CreateJsonStringValue(FString Value);
	UFUNCTION(BlueprintPure, Category = "JSON", meta = (DisplayName = "Create Json Number Value", Keywords = "float number json"))
	static UJsonValueWrapper* CreateJsonFloatValue(float Value);
	UFUNCTION(BlueprintPure, Category = "JSON", meta = (DisplayName = "Create Json Number Value", Keywords = "int number json"))
	static UJsonValueWrapper* CreateJsonIntValue(int32 Value);
	UFUNCTION(BlueprintPure, Category = "JSON", meta = (DisplayName = "Create Json Number Value", Keywords = "int64 number json"))
	static UJsonValueWrapper* CreateJsonInt64Value(int64 Value);
	UFUNCTION(BlueprintPure, Category = "JSON", meta = (DisplayName = "Create Json Number Value", Keywords = "byte number json"))
	static UJsonValueWrapper* CreateJsonByteValue(uint8 Value);
	UFUNCTION(BlueprintPure, Category = "JSON")
	static UJsonValueWrapper* CreateJsonBooleanValue(bool Value);
	UFUNCTION(BlueprintPure, Category = "JSON")
	static UJsonValueWrapper* CreateJsonNullValue();
	UFUNCTION(BlueprintPure, Category = "JSON")
	static UJsonValueWrapper* Parse(const FString& JsonString, bool& Success);
public:
	// member
	UFUNCTION(BlueprintPure, Category = "JSON")
	EJsonValueType GetType();
	UFUNCTION(BlueprintPure, Category = "JSON")
	bool IsType(EJsonValueType Type);
	UFUNCTION(BlueprintPure, Category = "JSON")
	void Stringify(FString& JsonString, bool& Success, bool PrettyPrint = false);
	UFUNCTION(BlueprintPure, Category = "JSON")
	bool HasField(FString Pattern, EJsonValueType FilterType = EJsonValueType::None);
	UFUNCTION(BlueprintPure, Category = "JSON")
	bool FindField(FString Pattern, UJsonValueWrapper*& Value, EJsonValueType FilterType = EJsonValueType::None);
	UFUNCTION(BlueprintPure, Category = "JSON")
	bool GetFieldNames(TArray<FString>& FieldNames);
	UFUNCTION(BlueprintPure, Category = "JSON")
	bool Equals(UJsonValueWrapper* const& Other);
	// accessors
	UFUNCTION(BlueprintPure, Category = "JSON")
	bool IsNullValue();
	UFUNCTION(BlueprintPure, Category = "JSON", meta = (DisplayName = "Get Number Value", Keywords = "byte number json"))
	bool GetByteValue(uint8& Value, uint8 Default = 0);
	UFUNCTION(BlueprintPure, Category = "JSON", meta = (DisplayName = "Get Number Value", Keywords = "int number json"))
	bool GetIntValue(int32& Value, int32 Default = 0);
	UFUNCTION(BlueprintPure, Category = "JSON", meta = (DisplayName = "Get Number Value", Keywords = "Int64 number json"))
	bool GetInt64Value(int64& Value, int64 Default = 0);
	UFUNCTION(BlueprintPure, Category = "JSON", meta = (DisplayName = "Get Number Value", Keywords = "float number json"))
	bool GetFloatValue(float& Value, float Default = 0.0f);
	UFUNCTION(BlueprintPure, Category = "JSON")
	bool GetStringValue(FString& Value, FString Default = "");
	UFUNCTION(BlueprintPure, Category = "JSON")
	bool GetBooleanValue(bool& Value, bool Default = false);
	UFUNCTION(BlueprintPure, Category = "JSON")
	bool GetObjectValue(TMap<FString, UJsonValueWrapper*>& Values);
	UFUNCTION(BlueprintPure, Category = "JSON")
	bool GetArrayValue(TArray<UJsonValueWrapper*>& Values);
	UFUNCTION(BlueprintPure, Category = "JSON", CustomThunk, meta = (CustomStructureParam = "Value"))
	void GetCustomValue(bool& Success, UStruct*& Value);
	DECLARE_FUNCTION(execGetCustomValue)
	{
		UJsonValueWrapper* Target = (UJsonValueWrapper*) P_THIS_OBJECT;
		P_GET_UBOOL_REF(Success);

		Stack.Step(Stack.Object, NULL);

		FProperty* Prop = Stack.MostRecentProperty;
		void* Ptr = Stack.MostRecentPropertyAddress;

		P_FINISH;

		int32 TotalErrors = 0;

		Success = false;

		TSharedRef<FJsonValue> InJsonValue = UJsonValueWrapper::JsonValueToCustom(Prop, Target->JsonValue, TotalErrors);

		if (InJsonValue->Type != EJson::None && InJsonValue->Type != EJson::Null)
		{
			Success = FJsonObjectConverter::JsonValueToUProperty(InJsonValue, Prop, Ptr, 0, 0);
			Success &= (TotalErrors == 0);
		}
	}
	// utility wrapper object
	UFUNCTION(BlueprintCallable, Category = "JSON", meta = (DisplayName = "RemoveField", Keywords = "json map object utility remove field"))
	UJsonValueWrapper* RemoveField(FString FieldName, bool& Success);
	UFUNCTION(BlueprintCallable, Category = "JSON", meta = (DisplayName = "AddField", Keywords = "json map object utility add field"))
	UJsonValueWrapper* AddField(FString FieldName, UJsonValueWrapper* const& Value, bool& Success);
	// utility wrapper array
	UFUNCTION(BlueprintCallable, Category = "JSON", meta = (DisplayName = "RemoveValueAt", Keywords = "json array utility remove index"))
	UJsonValueWrapper* RemoveValueAt(int32 Index, bool& Success);
	UFUNCTION(BlueprintCallable, Category = "JSON", meta = (DisplayName = "RemoveValue", Keywords = "json array utility remove value"))
	UJsonValueWrapper* RemoveValue(UJsonValueWrapper* const& Value, bool& Success);
	UFUNCTION(BlueprintCallable, Category = "JSON", meta = (DisplayName = "AddValue", Keywords = "json array utility add index"))
	UJsonValueWrapper* AddValue(UJsonValueWrapper* const& Value, bool& Success, int32 Index = -1);
public:
	static TSharedRef<FJsonValue> JsonValueToCustom(FProperty* Property, TSharedPtr<FJsonValue> Value, int32& TotalErrors);
	static TSharedRef<FJsonValue> CustomToJsonValue(FProperty* Property, void* ValuePtr);
	static UJsonValueWrapper* CreateJsonValue(TSharedRef<FJsonValue> Value);
private:
	TSharedPtr<FJsonValue> GetField(TSharedPtr<FJsonValue> Value, FString Pattern, EJsonValueType FilterType);
	template <class PrintPolicy> static bool Stringify(UJsonValueWrapper* Wrapper, FString& JsonString, const TSharedRef<TJsonWriter<TCHAR, PrintPolicy>>& JsonWriter);
};

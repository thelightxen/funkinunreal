// Copyright 2021 RLoris

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "XmlParser.h"
#include "JsonObjectConverter.h"
#include "XmlNodeWrapper.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class JSONXMLHELPER_API UXmlNodeWrapper : public UObject
{
	GENERATED_BODY()
private:
	UXmlNodeWrapper* Parent = nullptr;
	FString Tag;
	FString Content;
	TMap<FString, FString> Attributes;
	TArray<UXmlNodeWrapper*> Children;
public:
	UFUNCTION(BlueprintPure, Category = "XML", CustomThunk, meta = (CustomStructureParam = "Value"))
	static void CreateCustomXmlNode(bool& Valid, UXmlNodeWrapper*& Out, const UStruct* Value);
	DECLARE_FUNCTION(execCreateCustomXmlNode)
	{
		P_GET_UBOOL_REF(Valid);
		P_GET_OBJECT_REF(UXmlNodeWrapper, Out);
		Stack.Step(Stack.Object, NULL);

		FProperty* Prop = Stack.MostRecentProperty;
		void* Ptr = Stack.MostRecentPropertyAddress;

		P_FINISH;

		Valid = false;
		Out = UXmlNodeWrapper::CustomToXml(Prop, Ptr);
		if (Out)
		{
			if (Prop == nullptr || !Out->SetTag(Prop->GetCPPType(), Out))
			{
				Out->Tag = "root";
			}
			Valid = true;
		}
	}
	UFUNCTION(BlueprintPure, Category = "XML")
	static UXmlNodeWrapper* CreateXmlNode(FString TagName, bool& Valid);
	UFUNCTION(BlueprintPure, Category = "XML")
	static UXmlNodeWrapper* Parse(const FString& XmlString, bool& Success);
public:
	~UXmlNodeWrapper();
	UFUNCTION(BlueprintPure, Category = "XML", meta=(DisplayName="IsRootNode"))
	bool IsRootNode();
	UFUNCTION(BlueprintPure, Category = "XML", meta = (DisplayName = "IsLeafNode"))
	bool IsLeafNode();
	UFUNCTION(BlueprintPure, Category = "XML")
	bool GetParent(UXmlNodeWrapper*& Node);
	UFUNCTION(BlueprintPure, Category = "XML")
	void GetChildren(TArray<UXmlNodeWrapper*>& Children);
	UFUNCTION(BlueprintPure, Category = "XML")
	int32 GetChildrenCount();
	UFUNCTION(BlueprintPure, Category = "XML")
	void GetChildrenTags(TArray<FString>& Tags);
	UFUNCTION(BlueprintPure, Category = "XML")
	bool GetAttribute(FString Name, FString& Value);
	UFUNCTION(BlueprintPure, Category = "XML")
	void GetAttributes(TMap<FString, FString>& Values);
	UFUNCTION(BlueprintPure, Category = "XML")
	FString GetTag();
	UFUNCTION(BlueprintPure, Category = "XML")
	FString GetContent();
	UFUNCTION(BlueprintPure, Category = "XML", CustomThunk, meta = (CustomStructureParam = "Value"))
	void GetCustomValue(bool& Success, UStruct*& Value);
	DECLARE_FUNCTION(execGetCustomValue)
	{
		UXmlNodeWrapper* Target = (UXmlNodeWrapper*) P_THIS_OBJECT;
		P_GET_UBOOL_REF(Success);

		Stack.Step(Stack.Object, NULL);

		FProperty* Prop = Stack.MostRecentProperty;
		void* Ptr = Stack.MostRecentPropertyAddress;

		P_FINISH;

		TSharedRef<FJsonValue> InJsonValue = UXmlNodeWrapper::XmlToCustom(Prop, Target);

		Success = false;

		if (InJsonValue->Type != EJson::None && InJsonValue->Type != EJson::Null)
		{
			Success = FJsonObjectConverter::JsonValueToUProperty(InJsonValue, Prop, Ptr, 0, 0);
		}
	}
	UFUNCTION(BlueprintPure, Category = "XML")
	bool GetFirstChild(UXmlNodeWrapper*& Node);
	UFUNCTION(BlueprintPure, Category = "XML")
	bool GetLastChild(UXmlNodeWrapper*& Node);
	UFUNCTION(BlueprintPure, Category = "XML")
	bool GetNext(UXmlNodeWrapper*& Node);
	UFUNCTION(BlueprintPure, Category = "XML")
	bool GetPrev(UXmlNodeWrapper*& Node);
	UFUNCTION(BlueprintPure, Category = "XML")
	bool HasNode(FString Pattern);
	UFUNCTION(BlueprintPure, Category = "XML")
	bool FindNode(FString Pattern, UXmlNodeWrapper*& Node);
	// 
	UFUNCTION(BlueprintCallable, Category = "XML")
	void SetContent(FString NewContent, UXmlNodeWrapper*& This);
	UFUNCTION(BlueprintCallable, Category = "XML")
	bool AddAttribute(FString Name, FString Value, UXmlNodeWrapper*& This);
	UFUNCTION(BlueprintCallable, Category = "XML")
	bool SetTag(FString NewTagName, UXmlNodeWrapper*& This);
	UFUNCTION(BlueprintCallable, Category = "XML")
	bool AddChild(UXmlNodeWrapper* const & Child, UXmlNodeWrapper*& This, int32 Index = -1);
	UFUNCTION(BlueprintCallable, Category = "XML")
	bool AddNext(UXmlNodeWrapper* const & Next, UXmlNodeWrapper*& This);
	UFUNCTION(BlueprintCallable, Category = "XML")
	bool AddPrev(UXmlNodeWrapper* const & Prev, UXmlNodeWrapper*& This);
	UFUNCTION(BlueprintCallable, Category = "XML")
	bool Detach(UXmlNodeWrapper*& This);
	//
	UFUNCTION(BlueprintPure, Category = "XML")
	void Stringify(FString& XmlString, bool& Success, bool PrettyPrint = false);
	UFUNCTION(BlueprintPure, Category = "XML")
	bool IsAttached(UXmlNodeWrapper* Other);
	UFUNCTION(BlueprintPure, Category = "XML")
	UXmlNodeWrapper* GetRootNode();
private:
	static TSharedRef<FJsonValue> XmlToCustom(FProperty* Property, UXmlNodeWrapper* Node);
	static UXmlNodeWrapper* CustomToXml(FProperty* Property, void* ValuePtr);
	FString Stringify(int32 Depth);
	// used for parsing
	static UXmlNodeWrapper* CreateXmlNode(UXmlNodeWrapper* Parent, FXmlNode* Node);
	UXmlNodeWrapper* FindChild(const FString& TagName);
	static bool IsValidTagName(const FString& TagName);
	static bool IsValidAttribute(const FString& Attribute);
	// escape content characters
	static FString EscapeAttribute(const FString& Value);
	static FString EscapeContent(const FString& Content);
	// convert back characters
	static FString ConvertAttribute(const FString& Value);
	static FString ConvertContent(const FString& Content);
	// create text representation node
	static FString CreateTagNode(const FString& Tag, const FString& Content, TMap<FString, FString> Attributes, int32 Depth = 0, bool WithCR = true);
	UXmlNodeWrapper* GetNode(UXmlNodeWrapper* Node, const FString& Pattern);
};

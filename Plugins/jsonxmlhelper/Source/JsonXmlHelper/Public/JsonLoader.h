// Copyright 2021 RLoris

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "JsonValueWrapper.h"
#include "Engine/World.h"
#include "JsonLoader.generated.h"

UENUM(BlueprintType)
enum class EJsonLoaderSource : uint8
{
	FILE		UMETA(DisplayName = "LoadFromFile"),
	BUFFER		UMETA(DisplayName = "LoadFromString"),
	URL			UMETA(DisplayName = "LoadFromUrl")
};

USTRUCT(BlueprintType)
struct FJsonLoaderOptions {
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite, Category = "JSON")
	EJsonLoaderSource Source = EJsonLoaderSource::BUFFER;
	UPROPERTY(BlueprintReadWrite, Category = "JSON")
	FString From;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FJsonLoaderOutputPin, UJsonValueWrapper*, JsonValue, const FString&, ErrorReason);

/**
 * 
 */
UCLASS()
class JSONXMLHELPER_API UJsonLoader : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintAssignable)
	FJsonLoaderOutputPin Completed;
	UPROPERTY(BlueprintAssignable)
	FJsonLoaderOutputPin Failed;
public:
	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"), Category = "JSON")
	static UJsonLoader* LoadJson(UObject* WorldContextObject, const FJsonLoaderOptions& Options);
	virtual void Activate() override;
private:
	void LoadJsonFromFile();
	void LoadJsonFromBuffer(const FString& Buffer);
	void LoadJsonFromUrl();
	void _Completed(UJsonValueWrapper* JsonValue);
	void _Failed(const FString& ErrorReason);
private:
	const UObject* WorldContextObject = nullptr;
	bool Active = false;
	FJsonLoaderOptions Options;
};

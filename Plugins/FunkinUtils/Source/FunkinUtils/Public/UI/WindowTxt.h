// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Layout/Visibility.h"
#include "Widgets/DeclarativeSyntaxSupport.h"
#include "Input/Reply.h"
#include "Widgets/SCompoundWidget.h"
#include "Widgets/Layout/SBorder.h"
#include "FunkinUnrealEditor/Parsers/FunkinTxtParser.h"

class FActiveTimerHandle;
class IDetailsView;
class SWindow;
class SBox;
class UFunkinTxtParser;


class SWindowTxt : public SCompoundWidget
{
public:

	SLATE_BEGIN_ARGS(SWindowTxt) {}

	/** A reference to the parent window */
	SLATE_ARGUMENT(TSharedPtr<SWindow>, ParentWindow)


		SLATE_END_ARGS()

public:

	void Construct(const FArguments& InArgs);

	FString PickedFullPath;


private:
	/** The frequency at which to tick to scc module when inside a modal window */
	static const float RefreshFrequency;

	/** The parent window of this widget */
	TWeakPtr<SWindow> ParentWindowPtr;

	/** Holds the details view. */
	TSharedPtr<class IDetailsView> DetailsView;

	/** The currently displayed settings widget container */
	TSharedPtr<SBox> SettingsBorder;

	/** The handle to the active scc module tick */
	TWeakPtr<FActiveTimerHandle> ActiveTimerHandle;

	void OnTxtPathPicked(const FString& PickedPath);

	EVisibility GetThrobberVisibility() const;

	bool ParsingProcess;

	FString GetTxtPathString() const;

	FReply OnParse();

};


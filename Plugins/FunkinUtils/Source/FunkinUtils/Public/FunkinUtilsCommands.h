// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "FunkinUtilsStyle.h"

class FFunkinUtilsCommands : public TCommands<FFunkinUtilsCommands>
{
public:

	FFunkinUtilsCommands()
		: TCommands<FFunkinUtilsCommands>(TEXT("FunkinUtils"), NSLOCTEXT("Contexts", "FunkinUtils", "FunkinUtils Plugin"), NAME_None, FFunkinUtilsStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > PluginActionJson, PluginActionTxt, PluginActionXml;
	TSharedPtr< FUICommandInfo > FunkinSubMenuAction;
};

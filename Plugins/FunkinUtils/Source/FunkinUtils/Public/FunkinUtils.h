// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Editor/LevelEditor/Private/SLevelEditor.h"
#include "Modules/ModuleManager.h"
#include "UI/WindowJson.h"
#include "UI/WindowXml.h"
#include "UI/WindowTxt.h"
#include "Widgets/SWindow.h"

class FToolBarBuilder;
class FMenuBuilder;
class SWindow;

// UI
class SWindowJson;
class SWindowTxt;
class SWindowXml;

class FFunkinUtilsModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	/** This function will be bound to Command. */
	void PluginButtonClickedJSON();
	void PluginButtonClickedTXT();
	void PluginButtonClickedXML();

private:

	void RegisterMenus();

	void RegisterFunkinUtils(UToolMenu* InMenu);

	TSharedPtr<class SWindowJson> WindowJSONP;
	TSharedPtr<class SWindowTxt> WindowTXTP;
	TSharedPtr<class SWindowXml> WindowXMLP;

private:
	TSharedPtr<class FUICommandList> PluginCommands;

	TSharedPtr<FUICommandList> SubCommands;
};

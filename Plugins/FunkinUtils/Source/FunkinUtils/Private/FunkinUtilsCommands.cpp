// Copyright Epic Games, Inc. All Rights Reserved.

#include "FunkinUtilsCommands.h"

#define LOCTEXT_NAMESPACE "FFunkinUtilsModule"

void FFunkinUtilsCommands::RegisterCommands()
{
	UI_COMMAND(PluginActionJson, "FunkinUtils", "Parse JSON Chart File (PsychEngine only, KadeEngine non-stable!).", EUserInterfaceActionType::Button, FInputGesture());
	UI_COMMAND(PluginActionTxt, "FunkinUtils", "Parse Adobe Animate XML File (will create Texture2D asset, and Paper2DSprites for SubTextures).", EUserInterfaceActionType::Button, FInputGesture());
	UI_COMMAND(PluginActionXml, "FunkinUtils", "Parse TXT File, for example Data/introText.txt (from PsychEngine).", EUserInterfaceActionType::Button, FInputGesture());
	UI_COMMAND(FunkinSubMenuAction, "FunkinUtils", "Submenu open", EUserInterfaceActionType::Button, FInputGesture());
}

#undef LOCTEXT_NAMESPACE

// Copyright Epic Games, Inc. All Rights Reserved.

#include "FunkinUtils.h"
#include "FunkinUtilsStyle.h"
#include "FunkinUtilsCommands.h"
#include "Misc/MessageDialog.h"
#include "ToolMenus.h"
#include "LevelEditor.h"

static const FName FunkinUtilsTabName("FunkinUtils");

#define LOCTEXT_NAMESPACE "FFunkinUtilsModule"

void FFunkinUtilsModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	
	FFunkinUtilsStyle::Initialize();
	FFunkinUtilsStyle::ReloadTextures();

	FFunkinUtilsCommands::Register();
	
	PluginCommands = MakeShareable(new FUICommandList);

	PluginCommands->MapAction(
		FFunkinUtilsCommands::Get().PluginActionJson,
		FExecuteAction::CreateRaw(this, &FFunkinUtilsModule::PluginButtonClickedJSON),
		FCanExecuteAction());

	PluginCommands->MapAction(
		FFunkinUtilsCommands::Get().PluginActionTxt,
		FExecuteAction::CreateRaw(this, &FFunkinUtilsModule::PluginButtonClickedTXT),
		FCanExecuteAction());

	PluginCommands->MapAction(
		FFunkinUtilsCommands::Get().PluginActionXml,
		FExecuteAction::CreateRaw(this, &FFunkinUtilsModule::PluginButtonClickedXML),
		FCanExecuteAction());

	UToolMenus::RegisterStartupCallback(FSimpleMulticastDelegate::FDelegate::CreateRaw(this, &FFunkinUtilsModule::RegisterMenus));
}

void FFunkinUtilsModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	UToolMenus::UnRegisterStartupCallback(this);

	UToolMenus::UnregisterOwner(this);

	FFunkinUtilsStyle::Shutdown();

	FFunkinUtilsCommands::Unregister();
}

void FFunkinUtilsModule::PluginButtonClickedJSON()
{

	TSharedPtr<SWindow> WindowJSON = SNew(SWindow)
		.Title(FText::FromString(TEXT("Parse JSON")))
		.HasCloseButton(true)
		.SupportsMaximize(false)
		.SupportsMinimize(false)
		.SizingRule(ESizingRule::Autosized);

	WindowJSON->SetContent(
		SAssignNew(WindowJSONP, SWindowJson)
		.ParentWindow(WindowJSON)
	);

	TSharedPtr<SWindow> RootWindow = FGlobalTabmanager::Get()->GetRootWindow();

	if (RootWindow.IsValid())
	{

		FSlateApplication::Get().AddWindowAsNativeChild(WindowJSON.ToSharedRef(), RootWindow.ToSharedRef());

	}
	else
	{

		FSlateApplication::Get().AddWindow(WindowJSON.ToSharedRef());

	}

}

void FFunkinUtilsModule::PluginButtonClickedTXT()
{

	TSharedPtr<SWindow> WindowTXT = SNew(SWindow)
		.Title(FText::FromString(TEXT("Parse TXT")))
		.HasCloseButton(true)
		.SupportsMaximize(false)
		.SupportsMinimize(false)
		.SizingRule(ESizingRule::Autosized);

	WindowTXT->SetContent(
		SAssignNew(WindowTXTP, SWindowTxt)
		.ParentWindow(WindowTXT)
	);

	TSharedPtr<SWindow> RootWindow = FGlobalTabmanager::Get()->GetRootWindow();

	if (RootWindow.IsValid())
	{

		FSlateApplication::Get().AddWindowAsNativeChild(WindowTXT.ToSharedRef(), RootWindow.ToSharedRef());

	}
	else
	{

		FSlateApplication::Get().AddWindow(WindowTXT.ToSharedRef());

	}

}

void FFunkinUtilsModule::PluginButtonClickedXML()
{

	TSharedPtr<SWindow> WindowXML = SNew(SWindow)
		.Title(FText::FromString(TEXT("Parse XML")))
		.HasCloseButton(true)
		.SupportsMaximize(false)
		.SupportsMinimize(false)
		.SizingRule(ESizingRule::Autosized);

	WindowXML->SetContent(
		SAssignNew(WindowXMLP, SWindowXml)
		.ParentWindow(WindowXML)
	);

	TSharedPtr<SWindow> RootWindow = FGlobalTabmanager::Get()->GetRootWindow();

	if (RootWindow.IsValid())
	{

		FSlateApplication::Get().AddWindowAsNativeChild(WindowXML.ToSharedRef(), RootWindow.ToSharedRef());

	}
	else
	{

		FSlateApplication::Get().AddWindow(WindowXML.ToSharedRef());

	}

}

void FFunkinUtilsModule::RegisterMenus()
{
	// Owner will be used for cleanup in call to UToolMenus::UnregisterOwner
	FToolMenuOwnerScoped OwnerScoped(this);

	{

		UToolMenu* ToolbarMenu = UToolMenus::Get()->ExtendMenu("LevelEditor.LevelEditorToolBar");
		{
			FToolMenuSection& Section = ToolbarMenu->FindOrAddSection("Modes");
			{
				FToolMenuEntry& Entry = Section.AddEntry(

					FToolMenuEntry::InitComboButton(FName(TEXT("FunkinUtils")),
						FToolUIActionChoice(),
						FNewToolMenuChoice(FNewToolMenuDelegate::CreateRaw(this, &FFunkinUtilsModule::RegisterFunkinUtils)),
						FText::FromString(TEXT("FunkinUtils")),
						FText::FromString(TEXT("FunkinUtils")),
						FSlateIcon(FFunkinUtilsStyle::Get().GetStyleSetName(), "FunkinUtils.PluginToolAction")

					)
				);

			}
		}

	}
	{

		UToolMenu* NewMenu = UToolMenus::Get()->ExtendMenu("LevelEditor.MainMenu");
		{
			FToolMenuSection& Section = NewMenu->FindOrAddSection("Edit");
			{
				Section.AddMenuEntry(
					FFunkinUtilsCommands::Get().FunkinSubMenuAction,
					FText::FromString(TEXT("FunkinUtils")),
					FText::FromString(TEXT("FunkinUtils")),
					FSlateIcon()
				);
			}
		}

	}
	{
		UToolMenu* Menu = UToolMenus::Get()->ExtendMenu("LevelEditor.MainMenu.FunkinSubMenuAction");
		{

			RegisterFunkinUtils(Menu);

		}
	}
	
}

void FFunkinUtilsModule::RegisterFunkinUtils(UToolMenu* InMenu)
{
	{
		FToolMenuSection& Section = InMenu->AddSection("Utils", FText::FromString(TEXT("Utils")));

		Section.AddMenuEntryWithCommandList(FFunkinUtilsCommands::Get().PluginActionJson, PluginCommands, FText::FromString(TEXT("Parse JSON")));;
		Section.AddSeparator(FName(TEXT("Separator_One")));
		Section.AddMenuEntryWithCommandList(FFunkinUtilsCommands::Get().PluginActionXml, PluginCommands, FText::FromString(TEXT("Parse XML (Adobe Animate)")));
		Section.AddSeparator(FName(TEXT("Separator_Two")));
		Section.AddMenuEntryWithCommandList(FFunkinUtilsCommands::Get().PluginActionTxt, PluginCommands, FText::FromString(TEXT("Parse TXT")));

	}
}




#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FFunkinUtilsModule, FunkinUtils)
// Copyright Epic Games, Inc. All Rights Reserved.

#include "UI/WindowXml.h"


#include "Widgets/SBoxPanel.h"
#include "Framework/Application/SlateApplication.h"
#include "Widgets/Text/STextBlock.h"
#include "Widgets/Layout/SBox.h"
#include "Widgets/Layout/SUniformGridPanel.h"
#include "Widgets/Input/SButton.h"
#include "Widgets/Images/SThrobber.h"
#include "Widgets/Layout/SExpandableArea.h"
#include "Framework/Notifications/NotificationManager.h"
#include "Widgets/Notifications/SNotificationList.h"
#include "EditorStyleSet.h"
#include "Input/Reply.h"
#include "Widgets/Input/SFilePathPicker.h"
#include "EditorDirectories.h"
#define LOCTEXT_NAMESPACE "SWindowXml"

const float SWindowXml::RefreshFrequency = 0.5f;

void SWindowXml::Construct(const FArguments& InArgs)
{
	ParentWindowPtr = InArgs._ParentWindow;

	const FText FileFilterType = NSLOCTEXT("FunkinUtilsFiles", "XML", "XML");

	#if PLATFORM_WINDOWS
		const FString FileFilterText = FString::Printf(TEXT("%s (*.xml)|*.xml"), *FileFilterType.ToString());
	#else
		const FString FileFilterText = FString::Printf(TEXT("%s"), *FileFilterType.ToString());
	#endif

	ChildSlot
	[
		SNew(SBox)
		.WidthOverride(300.f)
		[
				SNew(SVerticalBox)
				+SVerticalBox::Slot()
				.AutoHeight()
				.Padding(16.0f, 20.0f, 16.0f, 0.0f)
				[
					SNew(SHorizontalBox)
					+SHorizontalBox::Slot()
					.FillWidth(1.0f)
					[
						SNew(STextBlock)
						.Text(LOCTEXT("XMLPathLabel", "XML Path"))
					]
					+SHorizontalBox::Slot()
					.FillWidth(2.0f)
					[
						SNew(SFilePathPicker)
						.BrowseButtonImage(FEditorStyle::GetBrush("PropertyWindow.Button_Ellipsis"))
						.BrowseButtonStyle(FEditorStyle::Get(), "HoverHintOnly")
						.BrowseDirectory(FEditorDirectories::Get().GetLastDirectory(ELastDirectory::GENERIC_OPEN))
						.BrowseTitle(LOCTEXT("BinaryPathBrowseTitle", "File picker..."))
						.FilePath(this, &SWindowXml::GetXmlPathString)
						.FileTypeFilter(FileFilterText)
						.OnPathPicked(this, &SWindowXml::OnXmlPathPicked)
					]
				]
	
				+SVerticalBox::Slot()
				.VAlign(VAlign_Bottom)
				.Padding(8.0f, 11.0f, 8.0f, 16.0f)
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					.AutoWidth()
					.VAlign(VAlign_Center)
					.HAlign(HAlign_Left)
					[
						SNew(SThrobber)
						.Visibility(this, &SWindowXml::GetThrobberVisibility)
					]
					+ SHorizontalBox::Slot()
					.FillWidth(1.0f)
					.HAlign(HAlign_Right)
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot()
						.AutoWidth()
						.Padding(FMargin(5.0f, 0.0f))
						[
							SNew(SButton)
							.VAlign(VAlign_Center)
							.Text(LOCTEXT("Parse", "Parse!"))
							.OnClicked(this, &SWindowXml::OnParse)
						]

				]
			]
		]
	];
}


void SWindowXml::OnXmlPathPicked(const FString& PickedPath)
{

	PickedFullPath = FPaths::ConvertRelativePathToFull(PickedPath);

}

FString SWindowXml::GetXmlPathString() const
{
	return PickedFullPath;
}


EVisibility SWindowXml::GetThrobberVisibility() const
{
	if (ParsingProcess) {

		return EVisibility::Visible;

	}
	else {

		return EVisibility::Collapsed;

	}
}

FReply SWindowXml::OnParse()
{

	ParsingProcess = true;

	if (UFunkinXmlParser::ParserXML(PickedFullPath, true))
	{

		FNotificationInfo Info(LOCTEXT("Parsed", "FunkinXmlParser.cpp : Success!"));
		Info.bFireAndForget = true;
		Info.bUseSuccessFailIcons = true;
		TSharedPtr<SNotificationItem> Notification = FSlateNotificationManager::Get().AddNotification(Info);
		Notification->SetCompletionState(SNotificationItem::CS_Success);

		ParsingProcess = false;

		GEditor->PlayEditorSound(TEXT("/Engine/EditorSounds/Notifications/CompileSuccess_Cue.CompileSuccess_Cue"));
	}
	else
	{

		FNotificationInfo Info(LOCTEXT("ParsedFailed", "FunkinXmlParser.cpp : Failed :("));
		Info.bFireAndForget = true;
		Info.bUseSuccessFailIcons = true;
		TSharedPtr<SNotificationItem> Notification = FSlateNotificationManager::Get().AddNotification(Info);
		Notification->SetCompletionState(SNotificationItem::CS_Fail);

		ParsingProcess = false;

		GEditor->PlayEditorSound(TEXT("/Engine/EditorSounds/Notifications/CompileFailed_Cue.CompileFailed_Cue"));
	}


	return FReply::Handled();
}

#undef LOCTEXT_NAMESPACE


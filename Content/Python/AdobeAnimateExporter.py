import unreal
import xml.etree.ElementTree as ET
from os import path
import os

# Main
xml_file = "C:/Users/TheLightXen/Documents/UnrealProjects/FunkinUnreal/Content/Python/menu_options.xml"
# Main


# ------------------------- #
# ----------Import--------- #
# ------------------------- #

# XML

dirname = os.path.dirname(__file__)

xml_file_parsed = ET.parse(xml_file).getroot()
image = xml_file_parsed.get('imagePath')

image_name, image_format = path.splitext(image)

AssetTools = unreal.AssetToolsHelpers.get_asset_tools()
AssetImportTask = unreal.AssetImportTask()
AssetImportTask.set_editor_property('filename', dirname + '/' + image)
AssetImportTask.set_editor_property('destination_path', 'Game/Python/Exports')
AssetImportTask.set_editor_property('save', True)
AssetTools.import_asset_tasks([AssetImportTask])

for tag in xml_file_parsed.findall('SubTexture'):
    print(tag)
    


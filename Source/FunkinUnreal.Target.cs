// (c) All content belongs to Ninjamuffin99, code of the Funkin' Unreal belongs to TheLightXen. Powered by Unreal Engine 4 (by Epic Games).

using UnrealBuildTool;
using System.Collections.Generic;

public class FunkinUnrealTarget : TargetRules
{
	public FunkinUnrealTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "FunkinUnreal" } );
	}
}

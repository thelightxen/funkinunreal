// (c) All content belongs to Ninjamuffin99, code of the Funkin' Unreal belongs to TheLightXen. Powered by Unreal Engine 4 (by Epic Games).

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "../Runtime/FunkinPaperSprite.h"
#include "../FunkinGameInstance.h"
#include "../Runtime/FunkinFlipbook.h"
#include "TextAsset.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/NoExportTypes.h"
#include "FunkinUtils.generated.h"

/**
 * 
 */
UCLASS()
class FUNKINUNREAL_API UFunkinUtils : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	// Sprites

	UFUNCTION(BlueprintPure, category = "FunkinUnreal|Sprites", meta = (HidePin = "WorldContextObject", DefaultToSelf = "WorldContextObject"))
		static const TArray<UFunkinPaperSprite*> PrepareSprites(UObject* WorldContextObject, TSoftObjectPtr<UTexture2D> SpriteSheet, UTextAsset* XML, FString AnimationFilter = "NULL");

	UFUNCTION(BlueprintPure, category = "FunkinUnreal|Sprites|Animation")
		static const UFunkinFlipbook* MakeAnimation(TArray<UFunkinPaperSprite*> FunkinSprites);


	// GetGameInst

	UFUNCTION(BlueprintPure, category = "FunkinUnreal", meta = (CompactNodeTitle = "FunkinInstance", HidePin = "WorldContextObject", DefaultToSelf = "WorldContextObject"))
		static UFunkinGameInstance* FunkinInstance(UObject* WorldContextObject);
};

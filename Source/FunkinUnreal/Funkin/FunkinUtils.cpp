// (c) All content belongs to Ninjamuffin99, code of the Funkin' Unreal belongs to TheLightXen. Powered by Unreal Engine 4 (by Epic Games).


#include "FunkinUnreal/Funkin/FunkinUtils.h"
#include "PaperFlipbook.h"
#include "UObject/SoftObjectPtr.h"
#include "../Modules/EasyXMLParseManager.h"

const TArray<UFunkinPaperSprite*> UFunkinUtils::PrepareSprites(UObject* WorldContextObject, TSoftObjectPtr<UTexture2D> SpriteSheet, UTextAsset* XML, FString AnimationFilter)
{

	TArray<UFunkinPaperSprite*> Sprites;

	EEasyXMLParserErrorCode Result;
	FString ErrorCode;

	UEasyXMLElement* Root = UEasyXMLParseManager::LoadFromString(XML->Text.ToString(), Result, ErrorCode);

	switch (Result)
	{
	case EEasyXMLParserErrorCode::Successed:

		EEasyXMLParserFound Found;

		FunkinInstance(WorldContextObject->GetWorld())->SpritesLoadedPerLevel += Root->ReadElements("TextureAtlas.SubTexture", Found).Num();

		for (UEasyXMLElement* SubTexture : Root->ReadElements("TextureAtlas.SubTexture", Found))
		{

			switch (Found)
			{
			case EEasyXMLParserFound::Found:

				if (AnimationFilter != "NULL" && !AnimationFilter.IsEmpty())
				{

					if (SubTexture->ReadString("@name").Contains(AnimationFilter))
					{

						UFunkinPaperSprite* SpriteUwU = NewObject<UFunkinPaperSprite>();

						SpriteUwU->SetFunkinSourceTexture(SpriteSheet);

						SpriteUwU->SetFunkinSourceDimension(FVector2D(SubTexture->ReadFloat("@width"), SubTexture->ReadFloat("@height")));

						SpriteUwU->SetFunkinSourceUV(FVector2D(SubTexture->ReadFloat("@x"), SubTexture->ReadFloat("@y")));

						SpriteUwU->SetFunkinPivotMode(Top_Left, FVector2D::ZeroVector);

						SpriteUwU->SetFunkinPivotMode(Custom, SpriteUwU->GetFunkinPivotPosition() + FVector2D(SubTexture->ReadFloat("@frameX"), SubTexture->ReadFloat("@frameY")));

						Sprites.Add(SpriteUwU);

						FunkinInstance(WorldContextObject->GetWorld())->SpritesLoadedPerLevel -= (FunkinInstance(WorldContextObject->GetWorld())->SpritesLoadedPerLevel > 0) ? 1 : 0;

					}

				}
				else
				{

					UFunkinPaperSprite* SpriteUwU = NewObject<UFunkinPaperSprite>();

					SpriteUwU->SetFunkinSourceDimension(FVector2D(SubTexture->ReadFloat("@width"), SubTexture->ReadFloat("@height")));

					SpriteUwU->SetFunkinSourceUV(FVector2D(SubTexture->ReadFloat("@x"), SubTexture->ReadFloat("@y")));

					SpriteUwU->SetFunkinPivotMode(Top_Left, FVector2D::ZeroVector);

					SpriteUwU->SetFunkinPivotMode(Custom, SpriteUwU->GetFunkinPivotPosition() + FVector2D(SubTexture->ReadFloat("@frameX"), SubTexture->ReadFloat("@frameY")));

					Sprites.Add(SpriteUwU);

					FunkinInstance(WorldContextObject->GetWorld())->SpritesLoadedPerLevel -= (FunkinInstance(WorldContextObject->GetWorld())->SpritesLoadedPerLevel > 0) ? 1 : 0;

				}



				break;

			case EEasyXMLParserFound::NotFound:



				break;
			}

		}


		break;

	case EEasyXMLParserErrorCode::Failed:

		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, ErrorCode);

		break;
	}

	return Sprites;

}
const UFunkinFlipbook* UFunkinUtils::MakeAnimation(TArray<UFunkinPaperSprite*> FunkinSprites)
{

	UFunkinFlipbook* Flipbook = NewObject<UFunkinFlipbook>();

	Flipbook->SetKeyFrames(FunkinSprites);

	return Flipbook;

}

UFunkinGameInstance* UFunkinUtils::FunkinInstance(UObject* WorldContextObject)
{

	return Cast<UFunkinGameInstance>(UGameplayStatics::GetGameInstance(WorldContextObject->GetWorld()));

}
// (c) All content belongs to Ninjamuffin99, code of the Funkin' Unreal belongs to TheLightXen. Powered by Unreal Engine 4 (by Epic Games).

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "FunkinGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class FUNKINUNREAL_API UFunkinGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadOnly, category=State)
	int32 SpritesLoadedPerLevel = 0;
	
};

// (c) All content belongs to Ninjamuffin99, code of the Funkin' Unreal belongs to TheLightXen. Powered by Unreal Engine 4 (by Epic Games).

#pragma once

#include "CoreMinimal.h"
#include "PaperSprite.h"
#include "FunkinPaperSprite.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum ESpritePivotModeCustom
{
	Top_Left,
	Top_Center,
	Top_Right,
	Center_Left,
	Center_Center,
	Center_Right,
	Bottom_Left,
	Bottom_Center,
	Bottom_Right,
	Custom
};

UCLASS(BlueprintType)
class FUNKINUNREAL_API UFunkinPaperSprite : public UPaperSprite
{
	GENERATED_UCLASS_BODY()

public:

	UFUNCTION(BlueprintCallable)
		void SetFunkinPivotMode(TEnumAsByte<ESpritePivotModeCustom> InPivotMode, FVector2D CustomTextureSpacePivot);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		FVector2D GetFunkinPivotPosition() const;

	UFUNCTION(BlueprintCallable)
	void SetFunkinSourceTexture(TSoftObjectPtr<UTexture2D> Texture)
	{
			
		SourceTexture = Texture;

	};

	UFUNCTION(BlueprintCallable)
	void SetFunkinSourceUV(FVector2D NewSourceUV)
	{

		SourceUV = NewSourceUV;

	};

	UFUNCTION(BlueprintCallable)
	void SetFunkinSourceDimension(FVector2D NewSourceDimension)
	{

		SourceDimension = NewSourceDimension;

	};
};

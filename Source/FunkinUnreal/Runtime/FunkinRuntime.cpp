// (c) TheLightXen.

#include "FunkinRuntime.h"
#include "FunkinUnreal/Modules/EasyXMLParseManager.h"
#include "PaperSprite.h"
#include "Kismet/KismetRenderingLibrary.h"
/*
UPaperFlipbook* FunkinRuntimeLoad(UTextAsset* TextAsset, UTexture2D* Image)
{

	EEasyXMLParserErrorCode ErrorCode;
	FString ErrorText;
	UEasyXMLElement* FileXML = UEasyXMLParseManager::LoadFromString(TextAsset->Text.ToString(), ErrorCode, ErrorText);

	//EEasyXMLParserFound Find;

	for (int32 i = 0; i < FileXML->UEasyXMLElement::ReadElements("TextureAtlas.SubTexture", Find).Num(); ++i)
	{

		TSubclassOf<UPaperSprite> Sprite = UPaperSprite::StaticClass();

		UTexture2D* Texture = Cast<UTexture2D>(Image);

		FAssetToolsModule& AssetToolsModule = FModuleManager::Get().LoadModuleChecked<FAssetToolsModule>("AssetTools");
		FContentBrowserModule& ContentBrowserModule = FModuleManager::LoadModuleChecked<FContentBrowserModule>("ContentBrowser");

		TArray<UObject*> ObjectsToSync;

		// Create the factory used to generate the sprite
		UFactoryFunkinSprite* SpriteFactory = NewObject<UFactoryFunkinSprite>();
		SpriteFactory->InitialTexture = Texture;
		SpriteFactory->InitialSourceDimension = FIntPoint(SubTexture->UEasyXMLElement::ReadInt("@width"), SubTexture->UEasyXMLElement::ReadInt("@height"));
		SpriteFactory->InitialSourceUV = FIntPoint(SubTexture->UEasyXMLElement::ReadInt("@x"), SubTexture->UEasyXMLElement::ReadInt("@y"));
		SpriteFactory->bUseSourceRegion = true;
		SpriteFactory->InitialCustomPivotPoint = FVector2D(SubTexture->UEasyXMLElement::ReadInt("@frameX"), SubTexture->UEasyXMLElement::ReadInt("@frameY"));

		// Create the sprite
		FString Name;
		FString PackageName;

		// Get a unique name for the sprite
		const FString DefaultSuffix = TEXT("_fue");
		AssetToolsModule.Get().CreateUniqueAssetName(Texture->GetOutermost()->GetName(), DefaultSuffix, PackageName, Name);
		const FString PackagePath = FPackageName::GetLongPackagePath(PackageName);

		if (UObject* NewAsset =

			AssetToolsModule.Get().CreateAsset(
				SubTexture->UEasyXMLElement::ReadString("@name")
				.Replace(TEXT(" "), TEXT("_")), "/Game/Exporters/" + imagePath_Name
				.Replace(TEXT(" "), TEXT("_")),
				UPaperSprite::StaticClass(),
				SpriteFactory)
			)
		{

			ObjectsToSync.Add(NewAsset);

		}


		if (ObjectsToSync.Num() > 0)
		{

			ContentBrowserModule.Get().SyncBrowserToAssets(ObjectsToSync);

		}

	}


}
*/
// (c) All content belongs to Ninjamuffin99, code of the Funkin' Unreal belongs to TheLightXen. Powered by Unreal Engine 4 (by Epic Games).

#include "FunkinUnreal/Runtime/FunkinPaperSprite.h"

UFunkinPaperSprite::UFunkinPaperSprite(class FObjectInitializer const&)
{


}


void UFunkinPaperSprite::SetFunkinPivotMode(TEnumAsByte<ESpritePivotModeCustom> InPivotMode, FVector2D CustomTextureSpacePivot)
{

	switch (InPivotMode)
	{
	case ESpritePivotModeCustom::Top_Left:

		SetPivotMode(ESpritePivotMode::Type::Top_Left, CustomTextureSpacePivot);

		break;

	case ESpritePivotModeCustom::Top_Center:

		SetPivotMode(ESpritePivotMode::Type::Top_Center, CustomTextureSpacePivot);

		break;

	case ESpritePivotModeCustom::Custom:

		SetPivotMode(ESpritePivotMode::Type::Custom, CustomTextureSpacePivot);

		break;

	case ESpritePivotModeCustom::Center_Right:

		SetPivotMode(ESpritePivotMode::Type::Center_Right, CustomTextureSpacePivot);

		break;

	case ESpritePivotModeCustom::Center_Left:

		SetPivotMode(ESpritePivotMode::Type::Center_Left, CustomTextureSpacePivot);

		break;

	case ESpritePivotModeCustom::Center_Center:

		SetPivotMode(ESpritePivotMode::Type::Center_Center, CustomTextureSpacePivot);

		break;

	case ESpritePivotModeCustom::Bottom_Right:

		SetPivotMode(ESpritePivotMode::Type::Bottom_Right, CustomTextureSpacePivot);

		break;

	case ESpritePivotModeCustom::Bottom_Left:

		SetPivotMode(ESpritePivotMode::Type::Bottom_Left, CustomTextureSpacePivot);

		break;

	case ESpritePivotModeCustom::Bottom_Center:

		SetPivotMode(ESpritePivotMode::Type::Bottom_Center, CustomTextureSpacePivot);

		break;

	}

}

FVector2D UFunkinPaperSprite::GetFunkinPivotPosition() const
{
	FVector2D RawPivot = GetRawPivotPosition();

	if (bSnapPivotToPixelGrid)
	{
		RawPivot.X = FMath::RoundToFloat(RawPivot.X);
		RawPivot.Y = FMath::RoundToFloat(RawPivot.Y);
	}

	return RawPivot;
}




// (c) All content belongs to Ninjamuffin99, code of the Funkin' Unreal belongs to TheLightXen. Powered by Unreal Engine 4 (by Epic Games).

#pragma once

#include "CoreMinimal.h"
#include "PaperFlipbook.h"
#include "FunkinPaperSprite.h"
#include "FunkinFlipbook.generated.h"


UCLASS()
class FUNKINUNREAL_API UFunkinFlipbook : public UPaperFlipbook
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable)
	void SetKeyFrames(TArray<UFunkinPaperSprite*> Sprites)
	{

		if (Sprites.Num() > 0)
		{

			for (int32 i = 0; i < Sprites.Num(); i++)
			{

				FPaperFlipbookKeyFrame Frame;

				Frame.Sprite = Sprites[i];

				KeyFrames.Add(Frame);

			}

		}

		

	};

};

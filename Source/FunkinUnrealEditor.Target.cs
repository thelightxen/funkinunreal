// (c) All content belongs to Ninjamuffin99, code of the Funkin' Unreal belongs to TheLightXen. Powered by Unreal Engine 4 (by Epic Games).

using UnrealBuildTool;
using System.Collections.Generic;

public class FunkinUnrealEditorTarget : TargetRules
{
	public FunkinUnrealEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "FunkinUnrealEditor" } );
	}
}

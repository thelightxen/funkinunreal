// (c) All content belongs to Ninjamuffin99, code of the Funkin' Unreal belongs to TheLightXen. Powered by Unreal Engine 4 (by Epic Games).

#pragma once

#include "CoreMinimal.h"
#include "FunkinTxtParser.generated.h"

/**
 * 
 */
UCLASS()
class FUNKINUNREALEDITOR_API UFunkinTxtParser : public UObject
{
	GENERATED_BODY()

public: 

	UFUNCTION()
		static bool ParserTXT(FString Path);
	
};

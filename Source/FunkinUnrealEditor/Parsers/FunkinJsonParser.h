// (c) All content belongs to Ninjamuffin99, code of the Funkin' Unreal belongs to TheLightXen. Powered by Unreal Engine 4 (by Epic Games).

#pragma once

#include "CoreMinimal.h"
#include "JsonValueWrapper.h"
#include "FunkinJsonParser.generated.h"

UCLASS()
class FUNKINUNREALEDITOR_API UFunkinJsonParser : public UObject
{
	GENERATED_BODY()

public:


	UFUNCTION()
		static bool ParserJSON(FString JsonPath);

	UFUNCTION()
		static bool NotesParsed(UJsonValueWrapper* Field, FString& Zero, FString& One, FString& Two);

};

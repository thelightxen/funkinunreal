// (c) All content belongs to Ninjamuffin99, code of the Funkin' Unreal belongs to TheLightXen. Powered by Unreal Engine 4 (by Epic Games).


#include "FunkinUnrealEditor/Parsers/FunkinJsonParser.h"
#include "Misc/Paths.h"
#include "JsonValueWrapper.h"
#include "VictoryBPFunctionLibrary.h"
#include "Misc/ConfigCacheIni.h"

bool UFunkinJsonParser::ParserJSON(FString JsonPath)
{

	FString FinalParse;

	if (FPaths::FileExists(JsonPath)) {

		FString JsonContent;

		if (FFileHelper::LoadFileToString(JsonContent, *JsonPath))
		{
			bool success_parsed;

			FinalParse = "[{\"Name\": \"";

			class UJsonValueWrapper* Field;

			FString Song, Player1, Player2, validScore, sections, needsVoices, speed, bpm;

			class UJsonValueWrapper* Parsed = UJsonValueWrapper::Parse(JsonContent, success_parsed);

			if (Parsed->FindField("song/song", Field) && Field->GetStringValue(Song))
			{

				FinalParse.Append(Song);
				FinalParse.Append("\", \"song\":{\"sectionLengths\": [] , ");

			}

			FinalParse.Append("\"player1\": \"");

			if (Parsed->FindField("song/player1", Field) && Field->GetStringValue(Player1))
			{

				FinalParse.Append(Player1);

			}

			if (Parsed->FindField("song/notes/", Field))
			{
				FinalParse.Append("\", \"notes\": [");
			}

			TArray<UJsonValueWrapper*> Arr;
			Field->GetArrayValue(Arr);

			for (int32 i = 0; i < Arr.Num(); i++)
			{

				FinalParse.Append("{ \"sectionNotes\": [");

				class UJsonValueWrapper* Field_sectionNotes;

				TArray<UJsonValueWrapper*> Array_sectionNotes;

				if (Arr[i]->FindField("sectionNotes", Field_sectionNotes) && Field_sectionNotes->GetArrayValue(Array_sectionNotes))
				{

					for (int32 i1 = 0; i1 < Array_sectionNotes.Num(); i1++)
					{

						FString Zero, One, Two;

						if (NotesParsed(Array_sectionNotes[i1], Zero, One, Two))
						{

							FinalParse.Append("{\"0\": ");
							FinalParse.Append(Zero);
							FinalParse.Append(", \"1\": ");
							FinalParse.Append(One);
							FinalParse.Append(", \"2\": ");
							FinalParse.Append(Two);

							if (Array_sectionNotes.Num() - 1 == i1)
							{

								FinalParse.Append("}");

							}
							else {

								FinalParse.Append("},");

							}


						}
					}

					// After sectionNotes

					FinalParse.Append("],");
					FinalParse.Append("\"lengthInSteps\": ");

					class UJsonValueWrapper* Field_lengthInSteps;
					FString Field_lengthInSteps_json;

					class UJsonValueWrapper* Field_typeOfSection;
					FString Field_typeOfSection_json;

					class UJsonValueWrapper* Field_mustHitSection;
					FString Field_mustHitSection_json;

					if (Arr[i]->FindField("lengthInSteps", Field_lengthInSteps) && Field_lengthInSteps->GetStringValue(Field_lengthInSteps_json))
					{

						FinalParse.Append(Field_lengthInSteps_json);

					}

					FinalParse.Append(",\"typeOfSection\": ");

					if (Arr[i]->FindField("typeOfSection", Field_typeOfSection) && Field_typeOfSection->GetStringValue(Field_typeOfSection_json))
					{

						FinalParse.Append(Field_typeOfSection_json);

					}

					FinalParse.Append(",\"mustHitSection\": ");

					if (Arr[i]->FindField("mustHitSection", Field_mustHitSection) && Field_mustHitSection->GetStringValue(Field_mustHitSection_json))
					{

						FinalParse.Append(Field_mustHitSection_json);

					}



					// End of Notes

					if (Arr.Num() - 1 == i)
					{

						FinalParse.Append("}");

					}
					else 
					{

						FinalParse.Append("},");

					}

				}


			}

			FinalParse.Append("], \"player2\": \"");

			if (Parsed->FindField("song/player2", Field) && Field->GetStringValue(Player2) && Player2 != "")
			{

				FinalParse.Append(Player2);

			}
			else 
			{

				FinalParse.Append("dad");

			}

			FinalParse.Append("\", \"song\": \"");

			if (Parsed->FindField("song/song", Field) && Field->GetStringValue(Song) && Song != "")
			{

				FinalParse.Append(Song);

			}
			else
			{

				FinalParse.Append("None");

			}

			FinalParse.Append("\", \"validScore\": ");

			if (Parsed->FindField("song/validScore", Field) && Field->GetStringValue(validScore) && validScore != "")
			{

				FinalParse.Append(validScore);
				
			}
			else
			{

				FinalParse.Append("0");

			}

			FinalParse.Append(", \"sections\": ");

			if (Parsed->FindField("song/sections", Field) && Field->GetStringValue(sections) && sections != "")
			{

				FinalParse.Append(sections);

			}
			else
			{

				FinalParse.Append("0");

			}

			FinalParse.Append(", \"needsVoices\": ");

			if (Parsed->FindField("song/needsVoices", Field) && Field->GetStringValue(needsVoices) && needsVoices != "")
			{

				FinalParse.Append(needsVoices);

			}
			else
			{

				FinalParse.Append("false");

			}

			FinalParse.Append(", \"speed\": ");

			if (Parsed->FindField("song/speed", Field) && Field->GetStringValue(speed) && speed != "")
			{

				FinalParse.Append(speed);

			}
			else
			{

				FinalParse.Append("0");

			}

			FinalParse.Append(", \"bpm\": ");

			if (Parsed->FindField("song/bpm", Field) && Field->GetStringValue(bpm) && bpm != "")
			{

				FinalParse.Append(bpm);

			}
			else
			{

				FinalParse.Append("0");

			}

			FinalParse.Append("}}]");

			return UVictoryBPFunctionLibrary::FileIO__SaveStringTextToFile(UVictoryBPFunctionLibrary::VictoryPaths__GameRootDirectory(), "test.txt", FinalParse, true);
		} 
		else 
		{

			return false;

		}

	}
	else 
	{

		return false;

	}
}


bool UFunkinJsonParser::NotesParsed(UJsonValueWrapper* Field, FString& Zero, FString& One, FString& Two)
{

	class UJsonValueWrapper* Field0;
	class UJsonValueWrapper* Field1;
	class UJsonValueWrapper* Field2;

	if ( ( Field->FindField("0", Field0) && Field0->GetStringValue(Zero) ) && ( Field->FindField("1", Field1) && Field1->GetStringValue(One) ) && ( Field->FindField("2", Field2) && Field2->GetStringValue(Two) ) )
	{

		return true;

	}
	else
	{

		return false;

	}

}
// (c) All content belongs to Ninjamuffin99, code of the Funkin' Unreal belongs to TheLightXen. Powered by Unreal Engine 4 (by Epic Games).


#include "FunkinUnrealEditor/Parsers/FunkinXmlParser.h"
#include "FunkinUnreal/Modules/EasyXMLParseManager.h"
#include "Misc/Paths.h"
#include "Factories/Factory.h"
#include "PaperSprite.h"
#include "AutomatedAssetImportData.h"
#include "IAssetTools.h"
#include "AssetToolsModule.h"
#include "FunkinUnrealEditor/Factory/FactoryFunkinSprite.h"
#include "VictoryBPFunctionLibrary.h"
#include "ContentBrowserModule.h"
#include "IContentBrowserSingleton.h"

bool UFunkinXmlParser::ParserXML(FString Path, bool Absolute)
{
	EEasyXMLParserErrorCode ErrorCode;
	FString ErrorText;
	UEasyXMLElement* FileXML = UEasyXMLParseManager::LoadFromFile(Path, Absolute, ErrorCode, ErrorText);
	FString Path_Path, Path_Name, Path_Extension; // xml file
	FPaths::Split(Path, Path_Path, Path_Name, Path_Extension);
	FString imagePath = Path_Path.Replace(TEXT("\\"), TEXT("/"), ESearchCase::CaseSensitive) + "/" + FileXML->ReadString("TextureAtlas.@imagePath");
	FString imagePath_Path, imagePath_Name, imagePath_Extension; // sprite file
	FPaths::Split(imagePath, imagePath_Path, imagePath_Name, imagePath_Extension);
	UAutomatedAssetImportData* AssetImportData = NewObject<UAutomatedAssetImportData>();
	AssetImportData->DestinationPath = "/Game/Exporters/" + imagePath_Name.Replace(TEXT(" "), TEXT("_"));
	AssetImportData->Filenames = { imagePath };
	AssetImportData->bReplaceExisting = true;
	IAssetTools& AssetTools = FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools").Get();
	class UFactoryFunkinSprite* Paper2dSP = NewObject<UFactoryFunkinSprite>();
	TArray<UObject*> ArrAss = AssetTools.ImportAssetsAutomated(AssetImportData);
	EEasyXMLParserFound Find;

	//UPaperSprite* ParsedPaper2D;

	for (int32 i = 0; i < FileXML->UEasyXMLElement::ReadElements("TextureAtlas.SubTexture", Find).Num(); ++i)
	{
		UEasyXMLElement* SubTexture = FileXML->UEasyXMLElement::ReadElements("TextureAtlas.SubTexture", Find)[i];

		//UVictoryBPFunctionLibrary::FileIO__SaveStringTextToFile(UVictoryBPFunctionLibrary::VictoryPaths__GameRootDirectory(), SubTexture->UEasyXMLElement::ReadString("@name"), "/Game/Exporters/" + imagePath_Name, true);

		TSubclassOf<UPaperSprite> Sprite = UPaperSprite::StaticClass();
		
		for (UObject* ImpAss : ArrAss)
		{

			UTexture2D* Texture = Cast<UTexture2D>(ImpAss);

			FAssetToolsModule& AssetToolsModule = FModuleManager::Get().LoadModuleChecked<FAssetToolsModule>("AssetTools");
			FContentBrowserModule& ContentBrowserModule = FModuleManager::LoadModuleChecked<FContentBrowserModule>("ContentBrowser");

			TArray<UObject*> ObjectsToSync;

			// Create the factory used to generate the sprite
			UFactoryFunkinSprite* SpriteFactory = NewObject<UFactoryFunkinSprite>();
			SpriteFactory->InitialTexture = Texture;
			SpriteFactory->InitialSourceDimension = FIntPoint(SubTexture->UEasyXMLElement::ReadInt("@width"), SubTexture->UEasyXMLElement::ReadInt("@height"));
			SpriteFactory->InitialSourceUV = FIntPoint(SubTexture->UEasyXMLElement::ReadInt("@x"), SubTexture->UEasyXMLElement::ReadInt("@y"));
			SpriteFactory->bUseSourceRegion = true;
			SpriteFactory->InitialCustomPivotPoint = FVector2D(SubTexture->UEasyXMLElement::ReadInt("@frameX"), SubTexture->UEasyXMLElement::ReadInt("@frameY"));

			// Create the sprite
			FString Name;
			FString PackageName;

			// Get a unique name for the sprite
			const FString DefaultSuffix = TEXT("_fue");
			AssetToolsModule.Get().CreateUniqueAssetName(Texture->GetOutermost()->GetName(), DefaultSuffix, /*out*/ PackageName, /*out*/ Name);
			const FString PackagePath = FPackageName::GetLongPackagePath(PackageName);

			if (UObject* NewAsset = 
				
				AssetToolsModule.Get().CreateAsset(
				SubTexture->UEasyXMLElement::ReadString("@name")
				.Replace(TEXT(" "), TEXT("_")), "/Game/Exporters/" + imagePath_Name
				.Replace(TEXT(" "), TEXT("_")), 
				UPaperSprite::StaticClass(), 
				SpriteFactory)
				)
			{

				ObjectsToSync.Add(NewAsset);

			}


			if (ObjectsToSync.Num() > 0)
			{

				ContentBrowserModule.Get().SyncBrowserToAssets(ObjectsToSync);

			}
		}
	}

	return true;
}

void UFunkinXmlParser::CreateSpriteFromTexture(UTexture2D* Texture)
{

}
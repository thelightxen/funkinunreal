// (c) All content belongs to Ninjamuffin99, code of the Funkin' Unreal belongs to TheLightXen. Powered by Unreal Engine 4 (by Epic Games).


#include "FunkinUnrealEditor/Parsers/FunkinTxtParser.h"
#include "VictoryBPFunctionLibrary.h"

bool UFunkinTxtParser::ParserTXT(FString Path)
{

	TArray<FString> ParseText = { "Key,SourceString" };

	int32 arraySize;

	TArray<FString> stringArray;

	if (UVictoryBPFunctionLibrary::LoadStringArrayFromFile(stringArray, arraySize, Path, true))
	{

		for (int32 i = 0; i < stringArray.Num(); ++i)
		{
			FString LocalString;

			LocalString.Append("\"");
			LocalString.Append(stringArray[i]);
			LocalString.Append("\",\"");
			LocalString.Append(stringArray[i]);
			LocalString.Append("\"");


			ParseText.Add(LocalString);


		}

		return UVictoryBPFunctionLibrary::FileIO__SaveStringArrayToFile(UVictoryBPFunctionLibrary::VictoryPaths__ConfigDir(), "ParsedTxt.csv", ParseText, true);

		FString protocolFile = "file://";
		protocolFile.Append(UVictoryBPFunctionLibrary::VictoryPaths__ConfigDir());

		UVictoryBPFunctionLibrary::Open_URL_In_Web_Browser(protocolFile);

	}
	else
	{

		return false;

	}

	

}
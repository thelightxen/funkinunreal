// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VICTORYBPLIBRARY_VictoryISM_generated_h
#error "VictoryISM.generated.h already included, missing '#pragma once' in VictoryISM.h"
#endif
#define VICTORYBPLIBRARY_VictoryISM_generated_h

#define FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_SPARSE_DATA
#define FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_RPC_WRAPPERS
#define FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAVictoryISM(); \
	friend struct Z_Construct_UClass_AVictoryISM_Statics; \
public: \
	DECLARE_CLASS(AVictoryISM, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VictoryBPLibrary"), NO_API) \
	DECLARE_SERIALIZER(AVictoryISM)


#define FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAVictoryISM(); \
	friend struct Z_Construct_UClass_AVictoryISM_Statics; \
public: \
	DECLARE_CLASS(AVictoryISM, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VictoryBPLibrary"), NO_API) \
	DECLARE_SERIALIZER(AVictoryISM)


#define FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AVictoryISM(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVictoryISM) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AVictoryISM); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVictoryISM); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AVictoryISM(AVictoryISM&&); \
	NO_API AVictoryISM(const AVictoryISM&); \
public:


#define FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AVictoryISM(AVictoryISM&&); \
	NO_API AVictoryISM(const AVictoryISM&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AVictoryISM); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVictoryISM); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVictoryISM)


#define FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_PRIVATE_PROPERTY_OFFSET
#define FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_12_PROLOG
#define FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_PRIVATE_PROPERTY_OFFSET \
	FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_SPARSE_DATA \
	FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_RPC_WRAPPERS \
	FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_INCLASS \
	FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_PRIVATE_PROPERTY_OFFSET \
	FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_SPARSE_DATA \
	FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_INCLASS_NO_PURE_DECLS \
	FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VICTORYBPLIBRARY_API UClass* StaticClass<class AVictoryISM>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FunkinUnreal_Plugins_Utils_Source_VictoryBPLibrary_Public_VictoryISM_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

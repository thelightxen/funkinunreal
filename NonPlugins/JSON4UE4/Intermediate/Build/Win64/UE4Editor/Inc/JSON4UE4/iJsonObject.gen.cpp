// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "iJsonUE4Plugin/Public/iJsonObject.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeiJsonObject() {}
// Cross Module References
	JSON4UE4_API UClass* Z_Construct_UClass_UiJsonObject_NoRegister();
	JSON4UE4_API UClass* Z_Construct_UClass_UiJsonObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_JSON4UE4();
// End Cross Module References
	DEFINE_FUNCTION(UiJsonObject::execConstructJsonObjectFromJsonString)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_OutFailReason);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ConstructJsonObjectFromJsonString(Z_Param_JsonString,Z_Param_Out_OutFailReason);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UiJsonObject::execConstructJsonObjectFromSelfJsonString)
	{
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_OutFailReason);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ConstructJsonObjectFromSelfJsonString(Z_Param_Out_OutFailReason);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UiJsonObject::execTryGetJsonObjectArrayField)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_FieldName);
		P_GET_TARRAY_REF(UiJsonObject*,Z_Param_Out_OutArray);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->TryGetJsonObjectArrayField(Z_Param_FieldName,Z_Param_Out_OutArray);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UiJsonObject::execTryGetNumberArrayField)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_FieldName);
		P_GET_TARRAY_REF(float,Z_Param_Out_OutArray);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->TryGetNumberArrayField(Z_Param_FieldName,Z_Param_Out_OutArray);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UiJsonObject::execTryGetStringArrayField)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_FieldName);
		P_GET_TARRAY_REF(FString,Z_Param_Out_OutArray);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->TryGetStringArrayField(Z_Param_FieldName,Z_Param_Out_OutArray);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UiJsonObject::execTryGetObjectField)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_FieldName);
		P_GET_OBJECT_REF(UiJsonObject,Z_Param_Out_OutiJsonObject);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->TryGetObjectField(Z_Param_FieldName,Z_Param_Out_OutiJsonObject);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UiJsonObject::execTryGetBoolField)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_FieldName);
		P_GET_UBOOL_REF(Z_Param_Out_OutBool);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->TryGetBoolField(Z_Param_FieldName,Z_Param_Out_OutBool);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UiJsonObject::execTryGetStringField)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_FieldName);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_OutString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->TryGetStringField(Z_Param_FieldName,Z_Param_Out_OutString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UiJsonObject::execTryGetNumberField)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_FieldName);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_OutNumber);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->TryGetNumberField(Z_Param_FieldName,Z_Param_Out_OutNumber);
		P_NATIVE_END;
	}
	void UiJsonObject::StaticRegisterNativesUiJsonObject()
	{
		UClass* Class = UiJsonObject::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ConstructJsonObjectFromJsonString", &UiJsonObject::execConstructJsonObjectFromJsonString },
			{ "ConstructJsonObjectFromSelfJsonString", &UiJsonObject::execConstructJsonObjectFromSelfJsonString },
			{ "TryGetBoolField", &UiJsonObject::execTryGetBoolField },
			{ "TryGetJsonObjectArrayField", &UiJsonObject::execTryGetJsonObjectArrayField },
			{ "TryGetNumberArrayField", &UiJsonObject::execTryGetNumberArrayField },
			{ "TryGetNumberField", &UiJsonObject::execTryGetNumberField },
			{ "TryGetObjectField", &UiJsonObject::execTryGetObjectField },
			{ "TryGetStringArrayField", &UiJsonObject::execTryGetStringArrayField },
			{ "TryGetStringField", &UiJsonObject::execTryGetStringField },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics
	{
		struct iJsonObject_eventConstructJsonObjectFromJsonString_Parms
		{
			FString JsonString;
			FText OutFailReason;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutFailReason;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonObject_eventConstructJsonObjectFromJsonString_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::NewProp_JsonString_MetaData)) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::NewProp_OutFailReason = { "OutFailReason", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonObject_eventConstructJsonObjectFromJsonString_Parms, OutFailReason), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((iJsonObject_eventConstructJsonObjectFromJsonString_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonObject_eventConstructJsonObjectFromJsonString_Parms), &Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::NewProp_JsonString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::NewProp_OutFailReason,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonObject" },
		{ "DisplayName", "ConstructJsonObjectFromJsonString" },
		{ "ModuleRelativePath", "Public/iJsonObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonObject, nullptr, "ConstructJsonObjectFromJsonString", nullptr, nullptr, sizeof(iJsonObject_eventConstructJsonObjectFromJsonString_Parms), Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString_Statics
	{
		struct iJsonObject_eventConstructJsonObjectFromSelfJsonString_Parms
		{
			FText OutFailReason;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutFailReason;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString_Statics::NewProp_OutFailReason = { "OutFailReason", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonObject_eventConstructJsonObjectFromSelfJsonString_Parms, OutFailReason), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((iJsonObject_eventConstructJsonObjectFromSelfJsonString_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonObject_eventConstructJsonObjectFromSelfJsonString_Parms), &Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString_Statics::NewProp_OutFailReason,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonObject" },
		{ "DisplayName", "ConstructJsonObjectFromSelfJsonString" },
		{ "ModuleRelativePath", "Public/iJsonObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonObject, nullptr, "ConstructJsonObjectFromSelfJsonString", nullptr, nullptr, sizeof(iJsonObject_eventConstructJsonObjectFromSelfJsonString_Parms), Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics
	{
		struct iJsonObject_eventTryGetBoolField_Parms
		{
			FString FieldName;
			bool OutBool;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FieldName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FieldName;
		static void NewProp_OutBool_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_OutBool;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::NewProp_FieldName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::NewProp_FieldName = { "FieldName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonObject_eventTryGetBoolField_Parms, FieldName), METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::NewProp_FieldName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::NewProp_FieldName_MetaData)) };
	void Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::NewProp_OutBool_SetBit(void* Obj)
	{
		((iJsonObject_eventTryGetBoolField_Parms*)Obj)->OutBool = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::NewProp_OutBool = { "OutBool", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonObject_eventTryGetBoolField_Parms), &Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::NewProp_OutBool_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((iJsonObject_eventTryGetBoolField_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonObject_eventTryGetBoolField_Parms), &Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::NewProp_FieldName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::NewProp_OutBool,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonObject" },
		{ "DisplayName", "TryGetBoolField" },
		{ "ModuleRelativePath", "Public/iJsonObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonObject, nullptr, "TryGetBoolField", nullptr, nullptr, sizeof(iJsonObject_eventTryGetBoolField_Parms), Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonObject_TryGetBoolField()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonObject_TryGetBoolField_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics
	{
		struct iJsonObject_eventTryGetJsonObjectArrayField_Parms
		{
			FString FieldName;
			TArray<UiJsonObject*> OutArray;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FieldName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FieldName;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutArray_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutArray;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::NewProp_FieldName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::NewProp_FieldName = { "FieldName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonObject_eventTryGetJsonObjectArrayField_Parms, FieldName), METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::NewProp_FieldName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::NewProp_FieldName_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::NewProp_OutArray_Inner = { "OutArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UiJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::NewProp_OutArray = { "OutArray", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonObject_eventTryGetJsonObjectArrayField_Parms, OutArray), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((iJsonObject_eventTryGetJsonObjectArrayField_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonObject_eventTryGetJsonObjectArrayField_Parms), &Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::NewProp_FieldName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::NewProp_OutArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::NewProp_OutArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonObject" },
		{ "DisplayName", "TryGet iJsonObject ArrayField" },
		{ "ModuleRelativePath", "Public/iJsonObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonObject, nullptr, "TryGetJsonObjectArrayField", nullptr, nullptr, sizeof(iJsonObject_eventTryGetJsonObjectArrayField_Parms), Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics
	{
		struct iJsonObject_eventTryGetNumberArrayField_Parms
		{
			FString FieldName;
			TArray<float> OutArray;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FieldName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FieldName;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OutArray_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutArray;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::NewProp_FieldName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::NewProp_FieldName = { "FieldName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonObject_eventTryGetNumberArrayField_Parms, FieldName), METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::NewProp_FieldName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::NewProp_FieldName_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::NewProp_OutArray_Inner = { "OutArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::NewProp_OutArray = { "OutArray", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonObject_eventTryGetNumberArrayField_Parms, OutArray), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((iJsonObject_eventTryGetNumberArrayField_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonObject_eventTryGetNumberArrayField_Parms), &Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::NewProp_FieldName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::NewProp_OutArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::NewProp_OutArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonObject" },
		{ "DisplayName", "TryGetNumberArrayField" },
		{ "ModuleRelativePath", "Public/iJsonObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonObject, nullptr, "TryGetNumberArrayField", nullptr, nullptr, sizeof(iJsonObject_eventTryGetNumberArrayField_Parms), Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics
	{
		struct iJsonObject_eventTryGetNumberField_Parms
		{
			FString FieldName;
			float OutNumber;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FieldName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FieldName;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OutNumber;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::NewProp_FieldName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::NewProp_FieldName = { "FieldName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonObject_eventTryGetNumberField_Parms, FieldName), METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::NewProp_FieldName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::NewProp_FieldName_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::NewProp_OutNumber = { "OutNumber", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonObject_eventTryGetNumberField_Parms, OutNumber), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((iJsonObject_eventTryGetNumberField_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonObject_eventTryGetNumberField_Parms), &Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::NewProp_FieldName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::NewProp_OutNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonObject" },
		{ "DisplayName", "TryGetNumberField" },
		{ "ModuleRelativePath", "Public/iJsonObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonObject, nullptr, "TryGetNumberField", nullptr, nullptr, sizeof(iJsonObject_eventTryGetNumberField_Parms), Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonObject_TryGetNumberField()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonObject_TryGetNumberField_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics
	{
		struct iJsonObject_eventTryGetObjectField_Parms
		{
			FString FieldName;
			UiJsonObject* OutiJsonObject;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FieldName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FieldName;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutiJsonObject;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::NewProp_FieldName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::NewProp_FieldName = { "FieldName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonObject_eventTryGetObjectField_Parms, FieldName), METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::NewProp_FieldName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::NewProp_FieldName_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::NewProp_OutiJsonObject = { "OutiJsonObject", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonObject_eventTryGetObjectField_Parms, OutiJsonObject), Z_Construct_UClass_UiJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((iJsonObject_eventTryGetObjectField_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonObject_eventTryGetObjectField_Parms), &Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::NewProp_FieldName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::NewProp_OutiJsonObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonObject" },
		{ "DisplayName", "TryGetObjectField" },
		{ "ModuleRelativePath", "Public/iJsonObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonObject, nullptr, "TryGetObjectField", nullptr, nullptr, sizeof(iJsonObject_eventTryGetObjectField_Parms), Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonObject_TryGetObjectField()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonObject_TryGetObjectField_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics
	{
		struct iJsonObject_eventTryGetStringArrayField_Parms
		{
			FString FieldName;
			TArray<FString> OutArray;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FieldName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FieldName;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OutArray_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutArray;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::NewProp_FieldName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::NewProp_FieldName = { "FieldName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonObject_eventTryGetStringArrayField_Parms, FieldName), METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::NewProp_FieldName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::NewProp_FieldName_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::NewProp_OutArray_Inner = { "OutArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::NewProp_OutArray = { "OutArray", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonObject_eventTryGetStringArrayField_Parms, OutArray), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((iJsonObject_eventTryGetStringArrayField_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonObject_eventTryGetStringArrayField_Parms), &Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::NewProp_FieldName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::NewProp_OutArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::NewProp_OutArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonObject" },
		{ "DisplayName", "TryGetStringArrayField" },
		{ "ModuleRelativePath", "Public/iJsonObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonObject, nullptr, "TryGetStringArrayField", nullptr, nullptr, sizeof(iJsonObject_eventTryGetStringArrayField_Parms), Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics
	{
		struct iJsonObject_eventTryGetStringField_Parms
		{
			FString FieldName;
			FString OutString;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FieldName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FieldName;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OutString;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::NewProp_FieldName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::NewProp_FieldName = { "FieldName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonObject_eventTryGetStringField_Parms, FieldName), METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::NewProp_FieldName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::NewProp_FieldName_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::NewProp_OutString = { "OutString", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonObject_eventTryGetStringField_Parms, OutString), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((iJsonObject_eventTryGetStringField_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonObject_eventTryGetStringField_Parms), &Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::NewProp_FieldName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::NewProp_OutString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonObject" },
		{ "DisplayName", "TryGetStringField" },
		{ "ModuleRelativePath", "Public/iJsonObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonObject, nullptr, "TryGetStringField", nullptr, nullptr, sizeof(iJsonObject_eventTryGetStringField_Parms), Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonObject_TryGetStringField()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonObject_TryGetStringField_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UiJsonObject_NoRegister()
	{
		return UiJsonObject::StaticClass();
	}
	struct Z_Construct_UClass_UiJsonObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewJsonString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NewJsonString;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UiJsonObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_JSON4UE4,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UiJsonObject_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromJsonString, "ConstructJsonObjectFromJsonString" }, // 1562643308
		{ &Z_Construct_UFunction_UiJsonObject_ConstructJsonObjectFromSelfJsonString, "ConstructJsonObjectFromSelfJsonString" }, // 1017757463
		{ &Z_Construct_UFunction_UiJsonObject_TryGetBoolField, "TryGetBoolField" }, // 2061482489
		{ &Z_Construct_UFunction_UiJsonObject_TryGetJsonObjectArrayField, "TryGetJsonObjectArrayField" }, // 3273677176
		{ &Z_Construct_UFunction_UiJsonObject_TryGetNumberArrayField, "TryGetNumberArrayField" }, // 2129529656
		{ &Z_Construct_UFunction_UiJsonObject_TryGetNumberField, "TryGetNumberField" }, // 660322229
		{ &Z_Construct_UFunction_UiJsonObject_TryGetObjectField, "TryGetObjectField" }, // 2222298410
		{ &Z_Construct_UFunction_UiJsonObject_TryGetStringArrayField, "TryGetStringArrayField" }, // 549416689
		{ &Z_Construct_UFunction_UiJsonObject_TryGetStringField, "TryGetStringField" }, // 1892444378
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UiJsonObject_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "YeHaike|iJson|iJsonObject" },
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "iJsonObject.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/iJsonObject.h" },
		{ "ShortTooltip", "iJsonObject is used to store JSON." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UiJsonObject_Statics::NewProp_NewJsonString_MetaData[] = {
		{ "Category", "Json" },
		{ "ModuleRelativePath", "Public/iJsonObject.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UiJsonObject_Statics::NewProp_NewJsonString = { "NewJsonString", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UiJsonObject, NewJsonString), METADATA_PARAMS(Z_Construct_UClass_UiJsonObject_Statics::NewProp_NewJsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UiJsonObject_Statics::NewProp_NewJsonString_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UiJsonObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UiJsonObject_Statics::NewProp_NewJsonString,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UiJsonObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UiJsonObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UiJsonObject_Statics::ClassParams = {
		&UiJsonObject::StaticClass,
		"iConfigs",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UiJsonObject_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UiJsonObject_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UiJsonObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UiJsonObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UiJsonObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UiJsonObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UiJsonObject, 974667943);
	template<> JSON4UE4_API UClass* StaticClass<UiJsonObject>()
	{
		return UiJsonObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UiJsonObject(Z_Construct_UClass_UiJsonObject, &UiJsonObject::StaticClass, TEXT("/Script/JSON4UE4"), TEXT("UiJsonObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UiJsonObject);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

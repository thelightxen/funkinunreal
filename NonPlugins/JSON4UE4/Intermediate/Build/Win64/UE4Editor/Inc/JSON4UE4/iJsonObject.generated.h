// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UiJsonObject;
#ifdef JSON4UE4_iJsonObject_generated_h
#error "iJsonObject.generated.h already included, missing '#pragma once' in iJsonObject.h"
#endif
#define JSON4UE4_iJsonObject_generated_h

#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_SPARSE_DATA
#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execConstructJsonObjectFromJsonString); \
	DECLARE_FUNCTION(execConstructJsonObjectFromSelfJsonString); \
	DECLARE_FUNCTION(execTryGetJsonObjectArrayField); \
	DECLARE_FUNCTION(execTryGetNumberArrayField); \
	DECLARE_FUNCTION(execTryGetStringArrayField); \
	DECLARE_FUNCTION(execTryGetObjectField); \
	DECLARE_FUNCTION(execTryGetBoolField); \
	DECLARE_FUNCTION(execTryGetStringField); \
	DECLARE_FUNCTION(execTryGetNumberField);


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execConstructJsonObjectFromJsonString); \
	DECLARE_FUNCTION(execConstructJsonObjectFromSelfJsonString); \
	DECLARE_FUNCTION(execTryGetJsonObjectArrayField); \
	DECLARE_FUNCTION(execTryGetNumberArrayField); \
	DECLARE_FUNCTION(execTryGetStringArrayField); \
	DECLARE_FUNCTION(execTryGetObjectField); \
	DECLARE_FUNCTION(execTryGetBoolField); \
	DECLARE_FUNCTION(execTryGetStringField); \
	DECLARE_FUNCTION(execTryGetNumberField);


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUiJsonObject(); \
	friend struct Z_Construct_UClass_UiJsonObject_Statics; \
public: \
	DECLARE_CLASS(UiJsonObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/JSON4UE4"), NO_API) \
	DECLARE_SERIALIZER(UiJsonObject) \
	static const TCHAR* StaticConfigName() {return TEXT("iConfigs");} \



#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUiJsonObject(); \
	friend struct Z_Construct_UClass_UiJsonObject_Statics; \
public: \
	DECLARE_CLASS(UiJsonObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/JSON4UE4"), NO_API) \
	DECLARE_SERIALIZER(UiJsonObject) \
	static const TCHAR* StaticConfigName() {return TEXT("iConfigs");} \



#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UiJsonObject(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UiJsonObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UiJsonObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UiJsonObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UiJsonObject(UiJsonObject&&); \
	NO_API UiJsonObject(const UiJsonObject&); \
public:


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UiJsonObject(UiJsonObject&&); \
	NO_API UiJsonObject(const UiJsonObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UiJsonObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UiJsonObject); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UiJsonObject)


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_PRIVATE_PROPERTY_OFFSET
#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_21_PROLOG
#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_PRIVATE_PROPERTY_OFFSET \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_SPARSE_DATA \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_RPC_WRAPPERS \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_INCLASS \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_PRIVATE_PROPERTY_OFFSET \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_SPARSE_DATA \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_INCLASS_NO_PURE_DECLS \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> JSON4UE4_API UClass* StaticClass<class UiJsonObject>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

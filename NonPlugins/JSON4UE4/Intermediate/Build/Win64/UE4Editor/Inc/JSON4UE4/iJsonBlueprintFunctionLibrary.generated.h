// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UiJsonObject;
class UObject;
#ifdef JSON4UE4_iJsonBlueprintFunctionLibrary_generated_h
#error "iJsonBlueprintFunctionLibrary.generated.h already included, missing '#pragma once' in iJsonBlueprintFunctionLibrary.h"
#endif
#define JSON4UE4_iJsonBlueprintFunctionLibrary_generated_h

#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_SPARSE_DATA
#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execConvertRelativePathToFull); \
	DECLARE_FUNCTION(execGetGameConfigDir); \
	DECLARE_FUNCTION(execGetStringOfTabs_02); \
	DECLARE_FUNCTION(execGetStringOfTabs_01); \
	DECLARE_FUNCTION(execGetStringOfLineBreak); \
	DECLARE_FUNCTION(execGetJsonObjectFromJsonString); \
	DECLARE_FUNCTION(execLoadJsonStringFromFile); \
	DECLARE_FUNCTION(execLoadStringFromFile); \
	DECLARE_FUNCTION(execCreateiJsonObject);


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execConvertRelativePathToFull); \
	DECLARE_FUNCTION(execGetGameConfigDir); \
	DECLARE_FUNCTION(execGetStringOfTabs_02); \
	DECLARE_FUNCTION(execGetStringOfTabs_01); \
	DECLARE_FUNCTION(execGetStringOfLineBreak); \
	DECLARE_FUNCTION(execGetJsonObjectFromJsonString); \
	DECLARE_FUNCTION(execLoadJsonStringFromFile); \
	DECLARE_FUNCTION(execLoadStringFromFile); \
	DECLARE_FUNCTION(execCreateiJsonObject);


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUiJsonBlueprintFunctionLibrary(); \
	friend struct Z_Construct_UClass_UiJsonBlueprintFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UiJsonBlueprintFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/JSON4UE4"), NO_API) \
	DECLARE_SERIALIZER(UiJsonBlueprintFunctionLibrary)


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUiJsonBlueprintFunctionLibrary(); \
	friend struct Z_Construct_UClass_UiJsonBlueprintFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UiJsonBlueprintFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/JSON4UE4"), NO_API) \
	DECLARE_SERIALIZER(UiJsonBlueprintFunctionLibrary)


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UiJsonBlueprintFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UiJsonBlueprintFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UiJsonBlueprintFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UiJsonBlueprintFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UiJsonBlueprintFunctionLibrary(UiJsonBlueprintFunctionLibrary&&); \
	NO_API UiJsonBlueprintFunctionLibrary(const UiJsonBlueprintFunctionLibrary&); \
public:


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UiJsonBlueprintFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UiJsonBlueprintFunctionLibrary(UiJsonBlueprintFunctionLibrary&&); \
	NO_API UiJsonBlueprintFunctionLibrary(const UiJsonBlueprintFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UiJsonBlueprintFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UiJsonBlueprintFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UiJsonBlueprintFunctionLibrary)


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_PRIVATE_PROPERTY_OFFSET
#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_20_PROLOG
#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_PRIVATE_PROPERTY_OFFSET \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_SPARSE_DATA \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_RPC_WRAPPERS \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_INCLASS \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_PRIVATE_PROPERTY_OFFSET \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_SPARSE_DATA \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_INCLASS_NO_PURE_DECLS \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> JSON4UE4_API UClass* StaticClass<class UiJsonBlueprintFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonBlueprintFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UiJsonObject;
#ifdef JSON4UE4_iJsonActor_generated_h
#error "iJsonActor.generated.h already included, missing '#pragma once' in iJsonActor.h"
#endif
#define JSON4UE4_iJsonActor_generated_h

#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_SPARSE_DATA
#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetStringOfTabs_02); \
	DECLARE_FUNCTION(execGetStringOfTabs_01); \
	DECLARE_FUNCTION(execGetStringOfLineBreak); \
	DECLARE_FUNCTION(execGetiJsonObject); \
	DECLARE_FUNCTION(execSetiJsonObject); \
	DECLARE_FUNCTION(execGetJsonObjectFromSelfJsonString); \
	DECLARE_FUNCTION(execGetJsonObjectFromJsonString); \
	DECLARE_FUNCTION(execLoadJsonStringFromFile); \
	DECLARE_FUNCTION(execLoadStringFromFile);


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetStringOfTabs_02); \
	DECLARE_FUNCTION(execGetStringOfTabs_01); \
	DECLARE_FUNCTION(execGetStringOfLineBreak); \
	DECLARE_FUNCTION(execGetiJsonObject); \
	DECLARE_FUNCTION(execSetiJsonObject); \
	DECLARE_FUNCTION(execGetJsonObjectFromSelfJsonString); \
	DECLARE_FUNCTION(execGetJsonObjectFromJsonString); \
	DECLARE_FUNCTION(execLoadJsonStringFromFile); \
	DECLARE_FUNCTION(execLoadStringFromFile);


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAiJsonActor(); \
	friend struct Z_Construct_UClass_AiJsonActor_Statics; \
public: \
	DECLARE_CLASS(AiJsonActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/JSON4UE4"), NO_API) \
	DECLARE_SERIALIZER(AiJsonActor) \
	static const TCHAR* StaticConfigName() {return TEXT("iConfigs");} \



#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_INCLASS \
private: \
	static void StaticRegisterNativesAiJsonActor(); \
	friend struct Z_Construct_UClass_AiJsonActor_Statics; \
public: \
	DECLARE_CLASS(AiJsonActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/JSON4UE4"), NO_API) \
	DECLARE_SERIALIZER(AiJsonActor) \
	static const TCHAR* StaticConfigName() {return TEXT("iConfigs");} \



#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AiJsonActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AiJsonActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AiJsonActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AiJsonActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AiJsonActor(AiJsonActor&&); \
	NO_API AiJsonActor(const AiJsonActor&); \
public:


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AiJsonActor(AiJsonActor&&); \
	NO_API AiJsonActor(const AiJsonActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AiJsonActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AiJsonActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AiJsonActor)


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_PRIVATE_PROPERTY_OFFSET
#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_20_PROLOG
#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_PRIVATE_PROPERTY_OFFSET \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_SPARSE_DATA \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_RPC_WRAPPERS \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_INCLASS \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_PRIVATE_PROPERTY_OFFSET \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_SPARSE_DATA \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_INCLASS_NO_PURE_DECLS \
	FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> JSON4UE4_API UClass* StaticClass<class AiJsonActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FunkinUnreal_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

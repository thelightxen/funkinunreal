// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UiJsonObject;
#ifdef JSON4UE4_iJsonActor_generated_h
#error "iJsonActor.generated.h already included, missing '#pragma once' in iJsonActor.h"
#endif
#define JSON4UE4_iJsonActor_generated_h

#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_SPARSE_DATA
#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetStringOfTabs_02) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetStringOfTabs_02(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetStringOfTabs_01) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetStringOfTabs_01(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetStringOfLineBreak) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetStringOfLineBreak(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetiJsonObject) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UiJsonObject**)Z_Param__Result=P_THIS->GetiJsonObject(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetiJsonObject) \
	{ \
		P_GET_OBJECT(UiJsonObject,Z_Param_iJsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetiJsonObject(Z_Param_iJsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetJsonObjectFromSelfJsonString) \
	{ \
		P_GET_OBJECT_REF(UiJsonObject,Z_Param_Out_iJsonObject); \
		P_GET_PROPERTY_REF(UTextProperty,Z_Param_Out_OutFailReason); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetJsonObjectFromSelfJsonString(Z_Param_Out_iJsonObject,Z_Param_Out_OutFailReason); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetJsonObjectFromJsonString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_JsonString); \
		P_GET_OBJECT_REF(UiJsonObject,Z_Param_Out_iJsonObject); \
		P_GET_PROPERTY_REF(UTextProperty,Z_Param_Out_OutFailReason); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetJsonObjectFromJsonString(Z_Param_JsonString,Z_Param_Out_iJsonObject,Z_Param_Out_OutFailReason); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execLoadJsonStringFromFile) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FileName); \
		P_GET_PROPERTY_REF(UStrProperty,Z_Param_Out_JsonStringFromFile); \
		P_GET_OBJECT_REF(UiJsonObject,Z_Param_Out_iJsonObject); \
		P_GET_PROPERTY_REF(UTextProperty,Z_Param_Out_OutFailReason); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->LoadJsonStringFromFile(Z_Param_FileName,Z_Param_Out_JsonStringFromFile,Z_Param_Out_iJsonObject,Z_Param_Out_OutFailReason); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execLoadStringFromFile) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FileName); \
		P_GET_PROPERTY_REF(UStrProperty,Z_Param_Out_StringFromFile); \
		P_GET_PROPERTY_REF(UTextProperty,Z_Param_Out_OutFailReason); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->LoadStringFromFile(Z_Param_FileName,Z_Param_Out_StringFromFile,Z_Param_Out_OutFailReason); \
		P_NATIVE_END; \
	}


#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetStringOfTabs_02) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetStringOfTabs_02(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetStringOfTabs_01) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetStringOfTabs_01(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetStringOfLineBreak) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetStringOfLineBreak(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetiJsonObject) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UiJsonObject**)Z_Param__Result=P_THIS->GetiJsonObject(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetiJsonObject) \
	{ \
		P_GET_OBJECT(UiJsonObject,Z_Param_iJsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetiJsonObject(Z_Param_iJsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetJsonObjectFromSelfJsonString) \
	{ \
		P_GET_OBJECT_REF(UiJsonObject,Z_Param_Out_iJsonObject); \
		P_GET_PROPERTY_REF(UTextProperty,Z_Param_Out_OutFailReason); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetJsonObjectFromSelfJsonString(Z_Param_Out_iJsonObject,Z_Param_Out_OutFailReason); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetJsonObjectFromJsonString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_JsonString); \
		P_GET_OBJECT_REF(UiJsonObject,Z_Param_Out_iJsonObject); \
		P_GET_PROPERTY_REF(UTextProperty,Z_Param_Out_OutFailReason); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetJsonObjectFromJsonString(Z_Param_JsonString,Z_Param_Out_iJsonObject,Z_Param_Out_OutFailReason); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execLoadJsonStringFromFile) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FileName); \
		P_GET_PROPERTY_REF(UStrProperty,Z_Param_Out_JsonStringFromFile); \
		P_GET_OBJECT_REF(UiJsonObject,Z_Param_Out_iJsonObject); \
		P_GET_PROPERTY_REF(UTextProperty,Z_Param_Out_OutFailReason); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->LoadJsonStringFromFile(Z_Param_FileName,Z_Param_Out_JsonStringFromFile,Z_Param_Out_iJsonObject,Z_Param_Out_OutFailReason); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execLoadStringFromFile) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FileName); \
		P_GET_PROPERTY_REF(UStrProperty,Z_Param_Out_StringFromFile); \
		P_GET_PROPERTY_REF(UTextProperty,Z_Param_Out_OutFailReason); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->LoadStringFromFile(Z_Param_FileName,Z_Param_Out_StringFromFile,Z_Param_Out_OutFailReason); \
		P_NATIVE_END; \
	}


#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAiJsonActor(); \
	friend struct Z_Construct_UClass_AiJsonActor_Statics; \
public: \
	DECLARE_CLASS(AiJsonActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/JSON4UE4"), NO_API) \
	DECLARE_SERIALIZER(AiJsonActor) \
	static const TCHAR* StaticConfigName() {return TEXT("iConfigs");} \



#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_INCLASS \
private: \
	static void StaticRegisterNativesAiJsonActor(); \
	friend struct Z_Construct_UClass_AiJsonActor_Statics; \
public: \
	DECLARE_CLASS(AiJsonActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/JSON4UE4"), NO_API) \
	DECLARE_SERIALIZER(AiJsonActor) \
	static const TCHAR* StaticConfigName() {return TEXT("iConfigs");} \



#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AiJsonActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AiJsonActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AiJsonActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AiJsonActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AiJsonActor(AiJsonActor&&); \
	NO_API AiJsonActor(const AiJsonActor&); \
public:


#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AiJsonActor(AiJsonActor&&); \
	NO_API AiJsonActor(const AiJsonActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AiJsonActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AiJsonActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AiJsonActor)


#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_PRIVATE_PROPERTY_OFFSET
#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_20_PROLOG
#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_PRIVATE_PROPERTY_OFFSET \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_SPARSE_DATA \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_RPC_WRAPPERS \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_INCLASS \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_PRIVATE_PROPERTY_OFFSET \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_SPARSE_DATA \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_INCLASS_NO_PURE_DECLS \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> JSON4UE4_API UClass* StaticClass<class AiJsonActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

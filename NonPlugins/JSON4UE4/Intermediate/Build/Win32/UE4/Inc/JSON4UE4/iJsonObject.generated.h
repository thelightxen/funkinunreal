// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UiJsonObject;
#ifdef JSON4UE4_iJsonObject_generated_h
#error "iJsonObject.generated.h already included, missing '#pragma once' in iJsonObject.h"
#endif
#define JSON4UE4_iJsonObject_generated_h

#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_SPARSE_DATA
#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execConstructJsonObjectFromJsonString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_JsonString); \
		P_GET_PROPERTY_REF(UTextProperty,Z_Param_Out_OutFailReason); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->ConstructJsonObjectFromJsonString(Z_Param_JsonString,Z_Param_Out_OutFailReason); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execConstructJsonObjectFromSelfJsonString) \
	{ \
		P_GET_PROPERTY_REF(UTextProperty,Z_Param_Out_OutFailReason); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->ConstructJsonObjectFromSelfJsonString(Z_Param_Out_OutFailReason); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTryGetJsonObjectArrayField) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FieldName); \
		P_GET_TARRAY_REF(UiJsonObject*,Z_Param_Out_OutArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->TryGetJsonObjectArrayField(Z_Param_FieldName,Z_Param_Out_OutArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTryGetNumberArrayField) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FieldName); \
		P_GET_TARRAY_REF(float,Z_Param_Out_OutArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->TryGetNumberArrayField(Z_Param_FieldName,Z_Param_Out_OutArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTryGetStringArrayField) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FieldName); \
		P_GET_TARRAY_REF(FString,Z_Param_Out_OutArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->TryGetStringArrayField(Z_Param_FieldName,Z_Param_Out_OutArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTryGetObjectField) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FieldName); \
		P_GET_OBJECT_REF(UiJsonObject,Z_Param_Out_OutiJsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->TryGetObjectField(Z_Param_FieldName,Z_Param_Out_OutiJsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTryGetBoolField) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FieldName); \
		P_GET_UBOOL_REF(Z_Param_Out_OutBool); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->TryGetBoolField(Z_Param_FieldName,Z_Param_Out_OutBool); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTryGetStringField) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FieldName); \
		P_GET_PROPERTY_REF(UStrProperty,Z_Param_Out_OutString); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->TryGetStringField(Z_Param_FieldName,Z_Param_Out_OutString); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTryGetNumberField) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FieldName); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_OutNumber); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->TryGetNumberField(Z_Param_FieldName,Z_Param_Out_OutNumber); \
		P_NATIVE_END; \
	}


#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execConstructJsonObjectFromJsonString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_JsonString); \
		P_GET_PROPERTY_REF(UTextProperty,Z_Param_Out_OutFailReason); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->ConstructJsonObjectFromJsonString(Z_Param_JsonString,Z_Param_Out_OutFailReason); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execConstructJsonObjectFromSelfJsonString) \
	{ \
		P_GET_PROPERTY_REF(UTextProperty,Z_Param_Out_OutFailReason); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->ConstructJsonObjectFromSelfJsonString(Z_Param_Out_OutFailReason); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTryGetJsonObjectArrayField) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FieldName); \
		P_GET_TARRAY_REF(UiJsonObject*,Z_Param_Out_OutArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->TryGetJsonObjectArrayField(Z_Param_FieldName,Z_Param_Out_OutArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTryGetNumberArrayField) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FieldName); \
		P_GET_TARRAY_REF(float,Z_Param_Out_OutArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->TryGetNumberArrayField(Z_Param_FieldName,Z_Param_Out_OutArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTryGetStringArrayField) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FieldName); \
		P_GET_TARRAY_REF(FString,Z_Param_Out_OutArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->TryGetStringArrayField(Z_Param_FieldName,Z_Param_Out_OutArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTryGetObjectField) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FieldName); \
		P_GET_OBJECT_REF(UiJsonObject,Z_Param_Out_OutiJsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->TryGetObjectField(Z_Param_FieldName,Z_Param_Out_OutiJsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTryGetBoolField) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FieldName); \
		P_GET_UBOOL_REF(Z_Param_Out_OutBool); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->TryGetBoolField(Z_Param_FieldName,Z_Param_Out_OutBool); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTryGetStringField) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FieldName); \
		P_GET_PROPERTY_REF(UStrProperty,Z_Param_Out_OutString); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->TryGetStringField(Z_Param_FieldName,Z_Param_Out_OutString); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTryGetNumberField) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FieldName); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_OutNumber); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->TryGetNumberField(Z_Param_FieldName,Z_Param_Out_OutNumber); \
		P_NATIVE_END; \
	}


#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUiJsonObject(); \
	friend struct Z_Construct_UClass_UiJsonObject_Statics; \
public: \
	DECLARE_CLASS(UiJsonObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/JSON4UE4"), NO_API) \
	DECLARE_SERIALIZER(UiJsonObject) \
	static const TCHAR* StaticConfigName() {return TEXT("iConfigs");} \



#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUiJsonObject(); \
	friend struct Z_Construct_UClass_UiJsonObject_Statics; \
public: \
	DECLARE_CLASS(UiJsonObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/JSON4UE4"), NO_API) \
	DECLARE_SERIALIZER(UiJsonObject) \
	static const TCHAR* StaticConfigName() {return TEXT("iConfigs");} \



#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UiJsonObject(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UiJsonObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UiJsonObject); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UiJsonObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UiJsonObject(UiJsonObject&&); \
	NO_API UiJsonObject(const UiJsonObject&); \
public:


#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UiJsonObject(UiJsonObject&&); \
	NO_API UiJsonObject(const UiJsonObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UiJsonObject); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UiJsonObject); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UiJsonObject)


#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_PRIVATE_PROPERTY_OFFSET
#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_21_PROLOG
#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_PRIVATE_PROPERTY_OFFSET \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_SPARSE_DATA \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_RPC_WRAPPERS \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_INCLASS \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_PRIVATE_PROPERTY_OFFSET \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_SPARSE_DATA \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_INCLASS_NO_PURE_DECLS \
	Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> JSON4UE4_API UClass* StaticClass<class UiJsonObject>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Build___Portal_Promotion_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_JSON4UE4_Source_iJsonUE4Plugin_Public_iJsonObject_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "iJsonUE4Plugin/Public/iJsonBlueprintFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeiJsonBlueprintFunctionLibrary() {}
// Cross Module References
	JSON4UE4_API UClass* Z_Construct_UClass_UiJsonBlueprintFunctionLibrary_NoRegister();
	JSON4UE4_API UClass* Z_Construct_UClass_UiJsonBlueprintFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_JSON4UE4();
	JSON4UE4_API UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull();
	JSON4UE4_API UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_CreateiJsonObject();
	JSON4UE4_API UClass* Z_Construct_UClass_UiJsonObject_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	JSON4UE4_API UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetGameConfigDir();
	JSON4UE4_API UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString();
	JSON4UE4_API UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfLineBreak();
	JSON4UE4_API UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_01();
	JSON4UE4_API UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_02();
	JSON4UE4_API UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile();
	JSON4UE4_API UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile();
// End Cross Module References
	void UiJsonBlueprintFunctionLibrary::StaticRegisterNativesUiJsonBlueprintFunctionLibrary()
	{
		UClass* Class = UiJsonBlueprintFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ConvertRelativePathToFull", &UiJsonBlueprintFunctionLibrary::execConvertRelativePathToFull },
			{ "CreateiJsonObject", &UiJsonBlueprintFunctionLibrary::execCreateiJsonObject },
			{ "GetGameConfigDir", &UiJsonBlueprintFunctionLibrary::execGetGameConfigDir },
			{ "GetJsonObjectFromJsonString", &UiJsonBlueprintFunctionLibrary::execGetJsonObjectFromJsonString },
			{ "GetStringOfLineBreak", &UiJsonBlueprintFunctionLibrary::execGetStringOfLineBreak },
			{ "GetStringOfTabs_01", &UiJsonBlueprintFunctionLibrary::execGetStringOfTabs_01 },
			{ "GetStringOfTabs_02", &UiJsonBlueprintFunctionLibrary::execGetStringOfTabs_02 },
			{ "LoadJsonStringFromFile", &UiJsonBlueprintFunctionLibrary::execLoadJsonStringFromFile },
			{ "LoadStringFromFile", &UiJsonBlueprintFunctionLibrary::execLoadStringFromFile },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull_Statics
	{
		struct iJsonBlueprintFunctionLibrary_eventConvertRelativePathToFull_Parms
		{
			FString InPath;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventConvertRelativePathToFull_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull_Statics::NewProp_InPath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull_Statics::NewProp_InPath = { "InPath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventConvertRelativePathToFull_Parms, InPath), METADATA_PARAMS(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull_Statics::NewProp_InPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull_Statics::NewProp_InPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull_Statics::NewProp_InPath,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonBlueprintFunctionLibrary|Path" },
		{ "Comment", "/**\n\x09* Converts a relative path name to a fully qualified name relative to the process BaseDir().\n\x09*/" },
		{ "ModuleRelativePath", "Public/iJsonBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Converts a relative path name to a fully qualified name relative to the process BaseDir()." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonBlueprintFunctionLibrary, nullptr, "ConvertRelativePathToFull", nullptr, nullptr, sizeof(iJsonBlueprintFunctionLibrary_eventConvertRelativePathToFull_Parms), Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_CreateiJsonObject_Statics
	{
		struct iJsonBlueprintFunctionLibrary_eventCreateiJsonObject_Parms
		{
			UObject* Outer;
			UiJsonObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Outer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_CreateiJsonObject_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventCreateiJsonObject_Parms, ReturnValue), Z_Construct_UClass_UiJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_CreateiJsonObject_Statics::NewProp_Outer = { "Outer", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventCreateiJsonObject_Parms, Outer), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_CreateiJsonObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_CreateiJsonObject_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_CreateiJsonObject_Statics::NewProp_Outer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_CreateiJsonObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonBlueprintFunctionLibrary" },
		{ "Comment", "/*UFUNCTION(BlueprintCallable, meta = (DisplayName = \"Make One Instance of iUE4Threader\"), Category = \"YeHaike|iThread|iUE4Threader\")\n\x09static AiUE4Threader* MakeInstanceOfiUE4Threader(FString In_Name, TScriptInterface<IiUE4ThreaderDelegate> In_Delegate);*/" },
		{ "DisplayName", "Create iJsonObject" },
		{ "Keywords", "Create iJsonObject" },
		{ "ModuleRelativePath", "Public/iJsonBlueprintFunctionLibrary.h" },
		{ "ToolTip", "UFUNCTION(BlueprintCallable, meta = (DisplayName = \"Make One Instance of iUE4Threader\"), Category = \"YeHaike|iThread|iUE4Threader\")\n       static AiUE4Threader* MakeInstanceOfiUE4Threader(FString In_Name, TScriptInterface<IiUE4ThreaderDelegate> In_Delegate);" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_CreateiJsonObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonBlueprintFunctionLibrary, nullptr, "CreateiJsonObject", nullptr, nullptr, sizeof(iJsonBlueprintFunctionLibrary_eventCreateiJsonObject_Parms), Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_CreateiJsonObject_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_CreateiJsonObject_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_CreateiJsonObject_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_CreateiJsonObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_CreateiJsonObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_CreateiJsonObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetGameConfigDir_Statics
	{
		struct iJsonBlueprintFunctionLibrary_eventGetGameConfigDir_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetGameConfigDir_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventGetGameConfigDir_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetGameConfigDir_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetGameConfigDir_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetGameConfigDir_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonBlueprintFunctionLibrary|Path" },
		{ "Comment", "/**\n\x09* Returns the directory the root configuration files are located.\n\x09*\n\x09* @return root config directory\n\x09*/" },
		{ "ModuleRelativePath", "Public/iJsonBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Returns the directory the root configuration files are located.\n\n@return root config directory" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetGameConfigDir_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonBlueprintFunctionLibrary, nullptr, "GetGameConfigDir", nullptr, nullptr, sizeof(iJsonBlueprintFunctionLibrary_eventGetGameConfigDir_Parms), Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetGameConfigDir_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetGameConfigDir_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetGameConfigDir_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetGameConfigDir_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetGameConfigDir()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetGameConfigDir_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics
	{
		struct iJsonBlueprintFunctionLibrary_eventGetJsonObjectFromJsonString_Parms
		{
			FString JsonString;
			UiJsonObject* iJsonObject;
			FText OutFailReason;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutFailReason;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_iJsonObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((iJsonBlueprintFunctionLibrary_eventGetJsonObjectFromJsonString_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonBlueprintFunctionLibrary_eventGetJsonObjectFromJsonString_Parms), &Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::NewProp_OutFailReason = { "OutFailReason", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventGetJsonObjectFromJsonString_Parms, OutFailReason), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::NewProp_iJsonObject = { "iJsonObject", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventGetJsonObjectFromJsonString_Parms, iJsonObject), Z_Construct_UClass_UiJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventGetJsonObjectFromJsonString_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::NewProp_JsonString_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::NewProp_OutFailReason,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::NewProp_iJsonObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::NewProp_JsonString,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonBlueprintFunctionLibrary" },
		{ "DisplayName", "GetJsonObjectFromJsonString" },
		{ "ModuleRelativePath", "Public/iJsonBlueprintFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonBlueprintFunctionLibrary, nullptr, "GetJsonObjectFromJsonString", nullptr, nullptr, sizeof(iJsonBlueprintFunctionLibrary_eventGetJsonObjectFromJsonString_Parms), Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfLineBreak_Statics
	{
		struct iJsonBlueprintFunctionLibrary_eventGetStringOfLineBreak_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfLineBreak_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventGetStringOfLineBreak_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfLineBreak_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfLineBreak_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfLineBreak_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonBlueprintFunctionLibrary" },
		{ "Comment", "/**Get line break string(like \"\\r\\n\")*/" },
		{ "DisplayName", "GetStringOfLineBreak" },
		{ "ModuleRelativePath", "Public/iJsonBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Get line break string(like \"\\r\\n\")" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfLineBreak_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonBlueprintFunctionLibrary, nullptr, "GetStringOfLineBreak", nullptr, nullptr, sizeof(iJsonBlueprintFunctionLibrary_eventGetStringOfLineBreak_Parms), Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfLineBreak_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfLineBreak_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfLineBreak_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfLineBreak_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfLineBreak()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfLineBreak_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_01_Statics
	{
		struct iJsonBlueprintFunctionLibrary_eventGetStringOfTabs_01_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_01_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventGetStringOfTabs_01_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_01_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_01_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_01_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonBlueprintFunctionLibrary" },
		{ "Comment", "/**Get Tab string(like \"\x09\")*/" },
		{ "DisplayName", "GetStringOfTabs_01" },
		{ "ModuleRelativePath", "Public/iJsonBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Get Tab string(like \"   \")" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_01_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonBlueprintFunctionLibrary, nullptr, "GetStringOfTabs_01", nullptr, nullptr, sizeof(iJsonBlueprintFunctionLibrary_eventGetStringOfTabs_01_Parms), Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_01_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_01_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_01_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_01_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_01()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_01_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_02_Statics
	{
		struct iJsonBlueprintFunctionLibrary_eventGetStringOfTabs_02_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_02_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventGetStringOfTabs_02_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_02_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_02_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_02_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonBlueprintFunctionLibrary" },
		{ "Comment", "/**Get Tab string(like \"\\t\")*/" },
		{ "DisplayName", "GetStringOfTabs_02" },
		{ "ModuleRelativePath", "Public/iJsonBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Get Tab string(like \"\\t\")" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_02_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonBlueprintFunctionLibrary, nullptr, "GetStringOfTabs_02", nullptr, nullptr, sizeof(iJsonBlueprintFunctionLibrary_eventGetStringOfTabs_02_Parms), Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_02_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_02_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_02_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_02_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_02()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_02_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics
	{
		struct iJsonBlueprintFunctionLibrary_eventLoadJsonStringFromFile_Parms
		{
			FString FileName;
			FString JsonStringFromFile;
			UiJsonObject* iJsonObject;
			FText OutFailReason;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutFailReason;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_iJsonObject;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_JsonStringFromFile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((iJsonBlueprintFunctionLibrary_eventLoadJsonStringFromFile_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonBlueprintFunctionLibrary_eventLoadJsonStringFromFile_Parms), &Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::NewProp_OutFailReason = { "OutFailReason", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventLoadJsonStringFromFile_Parms, OutFailReason), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::NewProp_iJsonObject = { "iJsonObject", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventLoadJsonStringFromFile_Parms, iJsonObject), Z_Construct_UClass_UiJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::NewProp_JsonStringFromFile = { "JsonStringFromFile", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventLoadJsonStringFromFile_Parms, JsonStringFromFile), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::NewProp_FileName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::NewProp_FileName = { "FileName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventLoadJsonStringFromFile_Parms, FileName), METADATA_PARAMS(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::NewProp_FileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::NewProp_FileName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::NewProp_OutFailReason,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::NewProp_iJsonObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::NewProp_JsonStringFromFile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::NewProp_FileName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonBlueprintFunctionLibrary" },
		{ "DisplayName", "LoadJsonStringFromFile" },
		{ "ModuleRelativePath", "Public/iJsonBlueprintFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonBlueprintFunctionLibrary, nullptr, "LoadJsonStringFromFile", nullptr, nullptr, sizeof(iJsonBlueprintFunctionLibrary_eventLoadJsonStringFromFile_Parms), Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics
	{
		struct iJsonBlueprintFunctionLibrary_eventLoadStringFromFile_Parms
		{
			FString FileName;
			FString StringFromFile;
			FText OutFailReason;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutFailReason;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringFromFile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((iJsonBlueprintFunctionLibrary_eventLoadStringFromFile_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonBlueprintFunctionLibrary_eventLoadStringFromFile_Parms), &Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::NewProp_OutFailReason = { "OutFailReason", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventLoadStringFromFile_Parms, OutFailReason), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::NewProp_StringFromFile = { "StringFromFile", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventLoadStringFromFile_Parms, StringFromFile), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::NewProp_FileName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::NewProp_FileName = { "FileName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonBlueprintFunctionLibrary_eventLoadStringFromFile_Parms, FileName), METADATA_PARAMS(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::NewProp_FileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::NewProp_FileName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::NewProp_OutFailReason,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::NewProp_StringFromFile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::NewProp_FileName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonBlueprintFunctionLibrary" },
		{ "DisplayName", "LoadStringFromFile" },
		{ "ModuleRelativePath", "Public/iJsonBlueprintFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UiJsonBlueprintFunctionLibrary, nullptr, "LoadStringFromFile", nullptr, nullptr, sizeof(iJsonBlueprintFunctionLibrary_eventLoadStringFromFile_Parms), Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UiJsonBlueprintFunctionLibrary_NoRegister()
	{
		return UiJsonBlueprintFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UiJsonBlueprintFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UiJsonBlueprintFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_JSON4UE4,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UiJsonBlueprintFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_ConvertRelativePathToFull, "ConvertRelativePathToFull" }, // 2716355884
		{ &Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_CreateiJsonObject, "CreateiJsonObject" }, // 930336389
		{ &Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetGameConfigDir, "GetGameConfigDir" }, // 3293022158
		{ &Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetJsonObjectFromJsonString, "GetJsonObjectFromJsonString" }, // 3409876887
		{ &Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfLineBreak, "GetStringOfLineBreak" }, // 194963842
		{ &Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_01, "GetStringOfTabs_01" }, // 1884394497
		{ &Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_GetStringOfTabs_02, "GetStringOfTabs_02" }, // 1085270806
		{ &Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadJsonStringFromFile, "LoadJsonStringFromFile" }, // 2171936612
		{ &Z_Construct_UFunction_UiJsonBlueprintFunctionLibrary_LoadStringFromFile, "LoadStringFromFile" }, // 3255513309
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UiJsonBlueprintFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "iJsonBlueprintFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/iJsonBlueprintFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UiJsonBlueprintFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UiJsonBlueprintFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UiJsonBlueprintFunctionLibrary_Statics::ClassParams = {
		&UiJsonBlueprintFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UiJsonBlueprintFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UiJsonBlueprintFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UiJsonBlueprintFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UiJsonBlueprintFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UiJsonBlueprintFunctionLibrary, 637581489);
	template<> JSON4UE4_API UClass* StaticClass<UiJsonBlueprintFunctionLibrary>()
	{
		return UiJsonBlueprintFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UiJsonBlueprintFunctionLibrary(Z_Construct_UClass_UiJsonBlueprintFunctionLibrary, &UiJsonBlueprintFunctionLibrary::StaticClass, TEXT("/Script/JSON4UE4"), TEXT("UiJsonBlueprintFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UiJsonBlueprintFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

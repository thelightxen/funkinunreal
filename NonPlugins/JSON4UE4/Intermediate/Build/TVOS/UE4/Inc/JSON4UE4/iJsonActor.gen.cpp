// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "iJsonUE4Plugin/Public/iJsonActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeiJsonActor() {}
// Cross Module References
	JSON4UE4_API UClass* Z_Construct_UClass_AiJsonActor_NoRegister();
	JSON4UE4_API UClass* Z_Construct_UClass_AiJsonActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_JSON4UE4();
	JSON4UE4_API UFunction* Z_Construct_UFunction_AiJsonActor_GetiJsonObject();
	JSON4UE4_API UClass* Z_Construct_UClass_UiJsonObject_NoRegister();
	JSON4UE4_API UFunction* Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString();
	JSON4UE4_API UFunction* Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString();
	JSON4UE4_API UFunction* Z_Construct_UFunction_AiJsonActor_GetStringOfLineBreak();
	JSON4UE4_API UFunction* Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_01();
	JSON4UE4_API UFunction* Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_02();
	JSON4UE4_API UFunction* Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile();
	JSON4UE4_API UFunction* Z_Construct_UFunction_AiJsonActor_LoadStringFromFile();
	JSON4UE4_API UFunction* Z_Construct_UFunction_AiJsonActor_SetiJsonObject();
// End Cross Module References
	void AiJsonActor::StaticRegisterNativesAiJsonActor()
	{
		UClass* Class = AiJsonActor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetiJsonObject", &AiJsonActor::execGetiJsonObject },
			{ "GetJsonObjectFromJsonString", &AiJsonActor::execGetJsonObjectFromJsonString },
			{ "GetJsonObjectFromSelfJsonString", &AiJsonActor::execGetJsonObjectFromSelfJsonString },
			{ "GetStringOfLineBreak", &AiJsonActor::execGetStringOfLineBreak },
			{ "GetStringOfTabs_01", &AiJsonActor::execGetStringOfTabs_01 },
			{ "GetStringOfTabs_02", &AiJsonActor::execGetStringOfTabs_02 },
			{ "LoadJsonStringFromFile", &AiJsonActor::execLoadJsonStringFromFile },
			{ "LoadStringFromFile", &AiJsonActor::execLoadStringFromFile },
			{ "SetiJsonObject", &AiJsonActor::execSetiJsonObject },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AiJsonActor_GetiJsonObject_Statics
	{
		struct iJsonActor_eventGetiJsonObject_Parms
		{
			UiJsonObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AiJsonActor_GetiJsonObject_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventGetiJsonObject_Parms, ReturnValue), Z_Construct_UClass_UiJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AiJsonActor_GetiJsonObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_GetiJsonObject_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AiJsonActor_GetiJsonObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonActor" },
		{ "DisplayName", "Get iJsonObject" },
		{ "ModuleRelativePath", "Public/iJsonActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AiJsonActor_GetiJsonObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AiJsonActor, nullptr, "GetiJsonObject", nullptr, nullptr, sizeof(iJsonActor_eventGetiJsonObject_Parms), Z_Construct_UFunction_AiJsonActor_GetiJsonObject_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_GetiJsonObject_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AiJsonActor_GetiJsonObject_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_GetiJsonObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AiJsonActor_GetiJsonObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AiJsonActor_GetiJsonObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics
	{
		struct iJsonActor_eventGetJsonObjectFromJsonString_Parms
		{
			FString JsonString;
			UiJsonObject* iJsonObject;
			FText OutFailReason;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutFailReason;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_iJsonObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((iJsonActor_eventGetJsonObjectFromJsonString_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonActor_eventGetJsonObjectFromJsonString_Parms), &Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::NewProp_OutFailReason = { "OutFailReason", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventGetJsonObjectFromJsonString_Parms, OutFailReason), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::NewProp_iJsonObject = { "iJsonObject", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventGetJsonObjectFromJsonString_Parms, iJsonObject), Z_Construct_UClass_UiJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventGetJsonObjectFromJsonString_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::NewProp_JsonString_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::NewProp_OutFailReason,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::NewProp_iJsonObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::NewProp_JsonString,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonActor" },
		{ "DisplayName", "GetJsonObjectFromJsonString" },
		{ "ModuleRelativePath", "Public/iJsonActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AiJsonActor, nullptr, "GetJsonObjectFromJsonString", nullptr, nullptr, sizeof(iJsonActor_eventGetJsonObjectFromJsonString_Parms), Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics
	{
		struct iJsonActor_eventGetJsonObjectFromSelfJsonString_Parms
		{
			UiJsonObject* iJsonObject;
			FText OutFailReason;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutFailReason;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_iJsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((iJsonActor_eventGetJsonObjectFromSelfJsonString_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonActor_eventGetJsonObjectFromSelfJsonString_Parms), &Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics::NewProp_OutFailReason = { "OutFailReason", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventGetJsonObjectFromSelfJsonString_Parms, OutFailReason), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics::NewProp_iJsonObject = { "iJsonObject", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventGetJsonObjectFromSelfJsonString_Parms, iJsonObject), Z_Construct_UClass_UiJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics::NewProp_OutFailReason,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics::NewProp_iJsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonActor" },
		{ "DisplayName", "GetJsonObjectFromSelfJsonString" },
		{ "ModuleRelativePath", "Public/iJsonActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AiJsonActor, nullptr, "GetJsonObjectFromSelfJsonString", nullptr, nullptr, sizeof(iJsonActor_eventGetJsonObjectFromSelfJsonString_Parms), Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AiJsonActor_GetStringOfLineBreak_Statics
	{
		struct iJsonActor_eventGetStringOfLineBreak_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AiJsonActor_GetStringOfLineBreak_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventGetStringOfLineBreak_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AiJsonActor_GetStringOfLineBreak_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_GetStringOfLineBreak_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AiJsonActor_GetStringOfLineBreak_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonActor" },
		{ "Comment", "/**Get line break string(like \"\\r\\n\")*/" },
		{ "DisplayName", "GetStringOfLineBreak" },
		{ "ModuleRelativePath", "Public/iJsonActor.h" },
		{ "ToolTip", "Get line break string(like \"\\r\\n\")" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AiJsonActor_GetStringOfLineBreak_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AiJsonActor, nullptr, "GetStringOfLineBreak", nullptr, nullptr, sizeof(iJsonActor_eventGetStringOfLineBreak_Parms), Z_Construct_UFunction_AiJsonActor_GetStringOfLineBreak_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_GetStringOfLineBreak_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AiJsonActor_GetStringOfLineBreak_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_GetStringOfLineBreak_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AiJsonActor_GetStringOfLineBreak()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AiJsonActor_GetStringOfLineBreak_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_01_Statics
	{
		struct iJsonActor_eventGetStringOfTabs_01_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_01_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventGetStringOfTabs_01_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_01_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_01_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_01_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonActor" },
		{ "Comment", "/**Get Tab string(like \"\x09\")*/" },
		{ "DisplayName", "GetStringOfTabs_01" },
		{ "ModuleRelativePath", "Public/iJsonActor.h" },
		{ "ToolTip", "Get Tab string(like \"   \")" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_01_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AiJsonActor, nullptr, "GetStringOfTabs_01", nullptr, nullptr, sizeof(iJsonActor_eventGetStringOfTabs_01_Parms), Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_01_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_01_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_01_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_01_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_01()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_01_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_02_Statics
	{
		struct iJsonActor_eventGetStringOfTabs_02_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_02_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventGetStringOfTabs_02_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_02_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_02_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_02_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonActor" },
		{ "Comment", "/**Get Tab string(like \"\\t\")*/" },
		{ "DisplayName", "GetStringOfTabs_02" },
		{ "ModuleRelativePath", "Public/iJsonActor.h" },
		{ "ToolTip", "Get Tab string(like \"\\t\")" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_02_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AiJsonActor, nullptr, "GetStringOfTabs_02", nullptr, nullptr, sizeof(iJsonActor_eventGetStringOfTabs_02_Parms), Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_02_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_02_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_02_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_02_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_02()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_02_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics
	{
		struct iJsonActor_eventLoadJsonStringFromFile_Parms
		{
			FString FileName;
			FString JsonStringFromFile;
			UiJsonObject* iJsonObject;
			FText OutFailReason;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutFailReason;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_iJsonObject;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_JsonStringFromFile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((iJsonActor_eventLoadJsonStringFromFile_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonActor_eventLoadJsonStringFromFile_Parms), &Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::NewProp_OutFailReason = { "OutFailReason", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventLoadJsonStringFromFile_Parms, OutFailReason), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::NewProp_iJsonObject = { "iJsonObject", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventLoadJsonStringFromFile_Parms, iJsonObject), Z_Construct_UClass_UiJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::NewProp_JsonStringFromFile = { "JsonStringFromFile", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventLoadJsonStringFromFile_Parms, JsonStringFromFile), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::NewProp_FileName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::NewProp_FileName = { "FileName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventLoadJsonStringFromFile_Parms, FileName), METADATA_PARAMS(Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::NewProp_FileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::NewProp_FileName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::NewProp_OutFailReason,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::NewProp_iJsonObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::NewProp_JsonStringFromFile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::NewProp_FileName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonActor" },
		{ "DisplayName", "LoadJsonStringFromFile" },
		{ "ModuleRelativePath", "Public/iJsonActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AiJsonActor, nullptr, "LoadJsonStringFromFile", nullptr, nullptr, sizeof(iJsonActor_eventLoadJsonStringFromFile_Parms), Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics
	{
		struct iJsonActor_eventLoadStringFromFile_Parms
		{
			FString FileName;
			FString StringFromFile;
			FText OutFailReason;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutFailReason;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringFromFile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((iJsonActor_eventLoadStringFromFile_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(iJsonActor_eventLoadStringFromFile_Parms), &Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::NewProp_OutFailReason = { "OutFailReason", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventLoadStringFromFile_Parms, OutFailReason), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::NewProp_StringFromFile = { "StringFromFile", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventLoadStringFromFile_Parms, StringFromFile), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::NewProp_FileName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::NewProp_FileName = { "FileName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventLoadStringFromFile_Parms, FileName), METADATA_PARAMS(Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::NewProp_FileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::NewProp_FileName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::NewProp_OutFailReason,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::NewProp_StringFromFile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::NewProp_FileName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonActor" },
		{ "DisplayName", "LoadStringFromFile" },
		{ "ModuleRelativePath", "Public/iJsonActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AiJsonActor, nullptr, "LoadStringFromFile", nullptr, nullptr, sizeof(iJsonActor_eventLoadStringFromFile_Parms), Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AiJsonActor_LoadStringFromFile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AiJsonActor_LoadStringFromFile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AiJsonActor_SetiJsonObject_Statics
	{
		struct iJsonActor_eventSetiJsonObject_Parms
		{
			UiJsonObject* iJsonObject;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_iJsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AiJsonActor_SetiJsonObject_Statics::NewProp_iJsonObject = { "iJsonObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(iJsonActor_eventSetiJsonObject_Parms, iJsonObject), Z_Construct_UClass_UiJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AiJsonActor_SetiJsonObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AiJsonActor_SetiJsonObject_Statics::NewProp_iJsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AiJsonActor_SetiJsonObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "YeHaike|iJson|iJsonActor" },
		{ "DisplayName", "Set iJsonObject" },
		{ "ModuleRelativePath", "Public/iJsonActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AiJsonActor_SetiJsonObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AiJsonActor, nullptr, "SetiJsonObject", nullptr, nullptr, sizeof(iJsonActor_eventSetiJsonObject_Parms), Z_Construct_UFunction_AiJsonActor_SetiJsonObject_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_SetiJsonObject_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AiJsonActor_SetiJsonObject_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AiJsonActor_SetiJsonObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AiJsonActor_SetiJsonObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AiJsonActor_SetiJsonObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AiJsonActor_NoRegister()
	{
		return AiJsonActor::StaticClass();
	}
	struct Z_Construct_UClass_AiJsonActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewJsonString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NewJsonString;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AiJsonActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_JSON4UE4,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AiJsonActor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AiJsonActor_GetiJsonObject, "GetiJsonObject" }, // 3580163459
		{ &Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromJsonString, "GetJsonObjectFromJsonString" }, // 1757030653
		{ &Z_Construct_UFunction_AiJsonActor_GetJsonObjectFromSelfJsonString, "GetJsonObjectFromSelfJsonString" }, // 3143456577
		{ &Z_Construct_UFunction_AiJsonActor_GetStringOfLineBreak, "GetStringOfLineBreak" }, // 86461838
		{ &Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_01, "GetStringOfTabs_01" }, // 213089760
		{ &Z_Construct_UFunction_AiJsonActor_GetStringOfTabs_02, "GetStringOfTabs_02" }, // 2314647982
		{ &Z_Construct_UFunction_AiJsonActor_LoadJsonStringFromFile, "LoadJsonStringFromFile" }, // 803302503
		{ &Z_Construct_UFunction_AiJsonActor_LoadStringFromFile, "LoadStringFromFile" }, // 493941052
		{ &Z_Construct_UFunction_AiJsonActor_SetiJsonObject, "SetiJsonObject" }, // 861249577
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AiJsonActor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "ActorsOfYeHaike" },
		{ "IncludePath", "iJsonActor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/iJsonActor.h" },
		{ "ShortTooltip", "AiJsonActor is used to process JSON String" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AiJsonActor_Statics::NewProp_NewJsonString_MetaData[] = {
		{ "Category", "Json" },
		{ "ModuleRelativePath", "Public/iJsonActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_AiJsonActor_Statics::NewProp_NewJsonString = { "NewJsonString", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AiJsonActor, NewJsonString), METADATA_PARAMS(Z_Construct_UClass_AiJsonActor_Statics::NewProp_NewJsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AiJsonActor_Statics::NewProp_NewJsonString_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AiJsonActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AiJsonActor_Statics::NewProp_NewJsonString,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AiJsonActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AiJsonActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AiJsonActor_Statics::ClassParams = {
		&AiJsonActor::StaticClass,
		"iConfigs",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AiJsonActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AiJsonActor_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AiJsonActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AiJsonActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AiJsonActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AiJsonActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AiJsonActor, 2070288049);
	template<> JSON4UE4_API UClass* StaticClass<AiJsonActor>()
	{
		return AiJsonActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AiJsonActor(Z_Construct_UClass_AiJsonActor, &AiJsonActor::StaticClass, TEXT("/Script/JSON4UE4"), TEXT("AiJsonActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AiJsonActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

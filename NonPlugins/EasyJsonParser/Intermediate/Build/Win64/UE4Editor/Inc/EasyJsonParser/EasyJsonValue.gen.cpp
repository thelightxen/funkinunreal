// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EasyJsonParser/Public/EasyJsonValue.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEasyJsonValue() {}
// Cross Module References
	EASYJSONPARSER_API UClass* Z_Construct_UClass_UEasyJsonValue_NoRegister();
	EASYJSONPARSER_API UClass* Z_Construct_UClass_UEasyJsonValue();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_EasyJsonParser();
// End Cross Module References
	DEFINE_FUNCTION(UEasyJsonValue::execGetBoolValue)
	{
		P_GET_UBOOL(Z_Param_DefaultValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetBoolValue(Z_Param_DefaultValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEasyJsonValue::execGetStringValue)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DefaultValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetStringValue(Z_Param_DefaultValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEasyJsonValue::execGetFloatValue)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_DefaultValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetFloatValue(Z_Param_DefaultValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEasyJsonValue::execGetIntValue)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_DefaultValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetIntValue(Z_Param_DefaultValue);
		P_NATIVE_END;
	}
	void UEasyJsonValue::StaticRegisterNativesUEasyJsonValue()
	{
		UClass* Class = UEasyJsonValue::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetBoolValue", &UEasyJsonValue::execGetBoolValue },
			{ "GetFloatValue", &UEasyJsonValue::execGetFloatValue },
			{ "GetIntValue", &UEasyJsonValue::execGetIntValue },
			{ "GetStringValue", &UEasyJsonValue::execGetStringValue },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics
	{
		struct EasyJsonValue_eventGetBoolValue_Parms
		{
			bool DefaultValue;
			bool ReturnValue;
		};
		static void NewProp_DefaultValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_DefaultValue;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics::NewProp_DefaultValue_SetBit(void* Obj)
	{
		((EasyJsonValue_eventGetBoolValue_Parms*)Obj)->DefaultValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EasyJsonValue_eventGetBoolValue_Parms), &Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics::NewProp_DefaultValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EasyJsonValue_eventGetBoolValue_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EasyJsonValue_eventGetBoolValue_Parms), &Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics::NewProp_DefaultValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "EasyJsonParser|GetValue" },
		{ "CPP_Default_DefaultValue", "false" },
		{ "ModuleRelativePath", "Public/EasyJsonValue.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEasyJsonValue, nullptr, "GetBoolValue", nullptr, nullptr, sizeof(EasyJsonValue_eventGetBoolValue_Parms), Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEasyJsonValue_GetBoolValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEasyJsonValue_GetBoolValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEasyJsonValue_GetFloatValue_Statics
	{
		struct EasyJsonValue_eventGetFloatValue_Parms
		{
			float DefaultValue;
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UEasyJsonValue_GetFloatValue_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonValue_eventGetFloatValue_Parms, DefaultValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UEasyJsonValue_GetFloatValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonValue_eventGetFloatValue_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEasyJsonValue_GetFloatValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonValue_GetFloatValue_Statics::NewProp_DefaultValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonValue_GetFloatValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonValue_GetFloatValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "EasyJsonParser|GetValue" },
		{ "CPP_Default_DefaultValue", "0.000000" },
		{ "ModuleRelativePath", "Public/EasyJsonValue.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEasyJsonValue_GetFloatValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEasyJsonValue, nullptr, "GetFloatValue", nullptr, nullptr, sizeof(EasyJsonValue_eventGetFloatValue_Parms), Z_Construct_UFunction_UEasyJsonValue_GetFloatValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonValue_GetFloatValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonValue_GetFloatValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonValue_GetFloatValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEasyJsonValue_GetFloatValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEasyJsonValue_GetFloatValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEasyJsonValue_GetIntValue_Statics
	{
		struct EasyJsonValue_eventGetIntValue_Parms
		{
			int32 DefaultValue;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEasyJsonValue_GetIntValue_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonValue_eventGetIntValue_Parms, DefaultValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEasyJsonValue_GetIntValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonValue_eventGetIntValue_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEasyJsonValue_GetIntValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonValue_GetIntValue_Statics::NewProp_DefaultValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonValue_GetIntValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonValue_GetIntValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "EasyJsonParser|GetValue" },
		{ "CPP_Default_DefaultValue", "0" },
		{ "ModuleRelativePath", "Public/EasyJsonValue.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEasyJsonValue_GetIntValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEasyJsonValue, nullptr, "GetIntValue", nullptr, nullptr, sizeof(EasyJsonValue_eventGetIntValue_Parms), Z_Construct_UFunction_UEasyJsonValue_GetIntValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonValue_GetIntValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonValue_GetIntValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonValue_GetIntValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEasyJsonValue_GetIntValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEasyJsonValue_GetIntValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEasyJsonValue_GetStringValue_Statics
	{
		struct EasyJsonValue_eventGetStringValue_Parms
		{
			FString DefaultValue;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEasyJsonValue_GetStringValue_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonValue_eventGetStringValue_Parms, DefaultValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEasyJsonValue_GetStringValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonValue_eventGetStringValue_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEasyJsonValue_GetStringValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonValue_GetStringValue_Statics::NewProp_DefaultValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonValue_GetStringValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonValue_GetStringValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "EasyJsonParser|GetValue" },
		{ "CPP_Default_DefaultValue", "" },
		{ "ModuleRelativePath", "Public/EasyJsonValue.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEasyJsonValue_GetStringValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEasyJsonValue, nullptr, "GetStringValue", nullptr, nullptr, sizeof(EasyJsonValue_eventGetStringValue_Parms), Z_Construct_UFunction_UEasyJsonValue_GetStringValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonValue_GetStringValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonValue_GetStringValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonValue_GetStringValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEasyJsonValue_GetStringValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEasyJsonValue_GetStringValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEasyJsonValue_NoRegister()
	{
		return UEasyJsonValue::StaticClass();
	}
	struct Z_Construct_UClass_UEasyJsonValue_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEasyJsonValue_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_EasyJsonParser,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEasyJsonValue_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEasyJsonValue_GetBoolValue, "GetBoolValue" }, // 1282409819
		{ &Z_Construct_UFunction_UEasyJsonValue_GetFloatValue, "GetFloatValue" }, // 888405110
		{ &Z_Construct_UFunction_UEasyJsonValue_GetIntValue, "GetIntValue" }, // 2078532734
		{ &Z_Construct_UFunction_UEasyJsonValue_GetStringValue, "GetStringValue" }, // 3934569914
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEasyJsonValue_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "EasyJsonValue.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/EasyJsonValue.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEasyJsonValue_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEasyJsonValue>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEasyJsonValue_Statics::ClassParams = {
		&UEasyJsonValue::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEasyJsonValue_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEasyJsonValue_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEasyJsonValue()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEasyJsonValue_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEasyJsonValue, 693168578);
	template<> EASYJSONPARSER_API UClass* StaticClass<UEasyJsonValue>()
	{
		return UEasyJsonValue::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEasyJsonValue(Z_Construct_UClass_UEasyJsonValue, &UEasyJsonValue::StaticClass, TEXT("/Script/EasyJsonParser"), TEXT("UEasyJsonValue"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEasyJsonValue);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

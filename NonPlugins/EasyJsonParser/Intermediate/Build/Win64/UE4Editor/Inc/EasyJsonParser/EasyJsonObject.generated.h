// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EEasyJsonParserFound : uint8;
class UEasyJsonObject;
#ifdef EASYJSONPARSER_EasyJsonObject_generated_h
#error "EasyJsonObject.generated.h already included, missing '#pragma once' in EasyJsonObject.h"
#endif
#define EASYJSONPARSER_EasyJsonObject_generated_h

#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_SPARSE_DATA
#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execReadObjects); \
	DECLARE_FUNCTION(execReadObject); \
	DECLARE_FUNCTION(execReadBool); \
	DECLARE_FUNCTION(execReadString); \
	DECLARE_FUNCTION(execReadFloat); \
	DECLARE_FUNCTION(execReadInt);


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execReadObjects); \
	DECLARE_FUNCTION(execReadObject); \
	DECLARE_FUNCTION(execReadBool); \
	DECLARE_FUNCTION(execReadString); \
	DECLARE_FUNCTION(execReadFloat); \
	DECLARE_FUNCTION(execReadInt);


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEasyJsonObject(); \
	friend struct Z_Construct_UClass_UEasyJsonObject_Statics; \
public: \
	DECLARE_CLASS(UEasyJsonObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EasyJsonParser"), NO_API) \
	DECLARE_SERIALIZER(UEasyJsonObject)


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUEasyJsonObject(); \
	friend struct Z_Construct_UClass_UEasyJsonObject_Statics; \
public: \
	DECLARE_CLASS(UEasyJsonObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EasyJsonParser"), NO_API) \
	DECLARE_SERIALIZER(UEasyJsonObject)


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEasyJsonObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEasyJsonObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEasyJsonObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEasyJsonObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEasyJsonObject(UEasyJsonObject&&); \
	NO_API UEasyJsonObject(const UEasyJsonObject&); \
public:


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEasyJsonObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEasyJsonObject(UEasyJsonObject&&); \
	NO_API UEasyJsonObject(const UEasyJsonObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEasyJsonObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEasyJsonObject); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEasyJsonObject)


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_PRIVATE_PROPERTY_OFFSET
#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_12_PROLOG
#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_PRIVATE_PROPERTY_OFFSET \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_SPARSE_DATA \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_RPC_WRAPPERS \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_INCLASS \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_PRIVATE_PROPERTY_OFFSET \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_SPARSE_DATA \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_INCLASS_NO_PURE_DECLS \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> EASYJSONPARSER_API UClass* StaticClass<class UEasyJsonObject>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonObject_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

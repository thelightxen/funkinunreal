// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EasyJsonParser/Public/EasyJsonParseManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEasyJsonParseManager() {}
// Cross Module References
	EASYJSONPARSER_API UClass* Z_Construct_UClass_UEasyJsonParseManager_NoRegister();
	EASYJSONPARSER_API UClass* Z_Construct_UClass_UEasyJsonParseManager();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_EasyJsonParser();
	EASYJSONPARSER_API UEnum* Z_Construct_UEnum_EasyJsonParser_EEasyJsonParserErrorCode();
	EASYJSONPARSER_API UClass* Z_Construct_UClass_UEasyJsonObject_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UEasyJsonParseManager::execLoadFromString)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_GET_ENUM_REF(EEasyJsonParserErrorCode,Z_Param_Out_Result);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UEasyJsonObject**)Z_Param__Result=UEasyJsonParseManager::LoadFromString(Z_Param_JsonString,(EEasyJsonParserErrorCode&)(Z_Param_Out_Result));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEasyJsonParseManager::execLoadFromFile)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_FilePath);
		P_GET_UBOOL(Z_Param_IsAblolute);
		P_GET_ENUM_REF(EEasyJsonParserErrorCode,Z_Param_Out_Result);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UEasyJsonObject**)Z_Param__Result=UEasyJsonParseManager::LoadFromFile(Z_Param_FilePath,Z_Param_IsAblolute,(EEasyJsonParserErrorCode&)(Z_Param_Out_Result));
		P_NATIVE_END;
	}
	void UEasyJsonParseManager::StaticRegisterNativesUEasyJsonParseManager()
	{
		UClass* Class = UEasyJsonParseManager::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "LoadFromFile", &UEasyJsonParseManager::execLoadFromFile },
			{ "LoadFromString", &UEasyJsonParseManager::execLoadFromString },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics
	{
		struct EasyJsonParseManager_eventLoadFromFile_Parms
		{
			FString FilePath;
			bool IsAblolute;
			EEasyJsonParserErrorCode Result;
			UEasyJsonObject* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FilePath;
		static void NewProp_IsAblolute_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsAblolute;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Result_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::NewProp_FilePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::NewProp_FilePath = { "FilePath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonParseManager_eventLoadFromFile_Parms, FilePath), METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::NewProp_FilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::NewProp_FilePath_MetaData)) };
	void Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::NewProp_IsAblolute_SetBit(void* Obj)
	{
		((EasyJsonParseManager_eventLoadFromFile_Parms*)Obj)->IsAblolute = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::NewProp_IsAblolute = { "IsAblolute", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EasyJsonParseManager_eventLoadFromFile_Parms), &Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::NewProp_IsAblolute_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::NewProp_Result_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonParseManager_eventLoadFromFile_Parms, Result), Z_Construct_UEnum_EasyJsonParser_EEasyJsonParserErrorCode, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonParseManager_eventLoadFromFile_Parms, ReturnValue), Z_Construct_UClass_UEasyJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::NewProp_FilePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::NewProp_IsAblolute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::NewProp_Result_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::Function_MetaDataParams[] = {
		{ "Category", "EasyJsonParser" },
		{ "Comment", "/**\n\x09 * load Json file\n\x09 * @param FilePath - Json file path\n\x09 * @param IsAblolute - true:FilePath is absolute path, false:Relative path from \"Content\"\n\x09 * @return Json object\n\x09 */" },
		{ "ExpandEnumAsExecs", "Result" },
		{ "ModuleRelativePath", "Public/EasyJsonParseManager.h" },
		{ "ToolTip", "load Json file\n@param FilePath - Json file path\n@param IsAblolute - true:FilePath is absolute path, false:Relative path from \"Content\"\n@return Json object" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEasyJsonParseManager, nullptr, "LoadFromFile", nullptr, nullptr, sizeof(EasyJsonParseManager_eventLoadFromFile_Parms), Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics
	{
		struct EasyJsonParseManager_eventLoadFromString_Parms
		{
			FString JsonString;
			EEasyJsonParserErrorCode Result;
			UEasyJsonObject* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Result_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonParseManager_eventLoadFromString_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::NewProp_JsonString_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::NewProp_Result_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonParseManager_eventLoadFromString_Parms, Result), Z_Construct_UEnum_EasyJsonParser_EEasyJsonParserErrorCode, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonParseManager_eventLoadFromString_Parms, ReturnValue), Z_Construct_UClass_UEasyJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::NewProp_JsonString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::NewProp_Result_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::Function_MetaDataParams[] = {
		{ "Category", "EasyJsonParser" },
		{ "Comment", "/**\n\x09 * load Json string\n\x09 * @param JsonString - Json file path\n\x09 * @return Json object\n\x09 */" },
		{ "ExpandEnumAsExecs", "Result" },
		{ "ModuleRelativePath", "Public/EasyJsonParseManager.h" },
		{ "ToolTip", "load Json string\n@param JsonString - Json file path\n@return Json object" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEasyJsonParseManager, nullptr, "LoadFromString", nullptr, nullptr, sizeof(EasyJsonParseManager_eventLoadFromString_Parms), Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEasyJsonParseManager_NoRegister()
	{
		return UEasyJsonParseManager::StaticClass();
	}
	struct Z_Construct_UClass_UEasyJsonParseManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEasyJsonParseManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_EasyJsonParser,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEasyJsonParseManager_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEasyJsonParseManager_LoadFromFile, "LoadFromFile" }, // 760214293
		{ &Z_Construct_UFunction_UEasyJsonParseManager_LoadFromString, "LoadFromString" }, // 4200431266
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEasyJsonParseManager_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "EasyJsonParseManager.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/EasyJsonParseManager.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEasyJsonParseManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEasyJsonParseManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEasyJsonParseManager_Statics::ClassParams = {
		&UEasyJsonParseManager::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEasyJsonParseManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEasyJsonParseManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEasyJsonParseManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEasyJsonParseManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEasyJsonParseManager, 1848189214);
	template<> EASYJSONPARSER_API UClass* StaticClass<UEasyJsonParseManager>()
	{
		return UEasyJsonParseManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEasyJsonParseManager(Z_Construct_UClass_UEasyJsonParseManager, &UEasyJsonParseManager::StaticClass, TEXT("/Script/EasyJsonParser"), TEXT("UEasyJsonParseManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEasyJsonParseManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

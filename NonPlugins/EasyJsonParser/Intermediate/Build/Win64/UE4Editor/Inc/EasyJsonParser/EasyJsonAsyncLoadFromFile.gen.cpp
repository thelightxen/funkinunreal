// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EasyJsonParser/Public/EasyJsonAsyncLoadFromFile.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEasyJsonAsyncLoadFromFile() {}
// Cross Module References
	EASYJSONPARSER_API UFunction* Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromFile_Result__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_EasyJsonParser();
	EASYJSONPARSER_API UClass* Z_Construct_UClass_UEasyJsonObject_NoRegister();
	EASYJSONPARSER_API UClass* Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_NoRegister();
	EASYJSONPARSER_API UClass* Z_Construct_UClass_UEasyJsonAsyncLoadFromFile();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintAsyncActionBase();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromFile_Result__DelegateSignature_Statics
	{
		struct _Script_EasyJsonParser_eventEasyJsonAsyncLoadFromFile_Result_Parms
		{
			UEasyJsonObject* JsonObject;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromFile_Result__DelegateSignature_Statics::NewProp_JsonObject = { "JsonObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_EasyJsonParser_eventEasyJsonAsyncLoadFromFile_Result_Parms, JsonObject), Z_Construct_UClass_UEasyJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromFile_Result__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromFile_Result__DelegateSignature_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromFile_Result__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/EasyJsonAsyncLoadFromFile.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromFile_Result__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_EasyJsonParser, nullptr, "EasyJsonAsyncLoadFromFile_Result__DelegateSignature", nullptr, nullptr, sizeof(_Script_EasyJsonParser_eventEasyJsonAsyncLoadFromFile_Result_Parms), Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromFile_Result__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromFile_Result__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromFile_Result__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromFile_Result__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromFile_Result__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromFile_Result__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UEasyJsonAsyncLoadFromFile::execAsyncLoadFromFile)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_PROPERTY(FStrProperty,Z_Param_FilePath);
		P_GET_UBOOL(Z_Param_IsAblolute);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UEasyJsonAsyncLoadFromFile**)Z_Param__Result=UEasyJsonAsyncLoadFromFile::AsyncLoadFromFile(Z_Param_WorldContextObject,Z_Param_FilePath,Z_Param_IsAblolute);
		P_NATIVE_END;
	}
	void UEasyJsonAsyncLoadFromFile::StaticRegisterNativesUEasyJsonAsyncLoadFromFile()
	{
		UClass* Class = UEasyJsonAsyncLoadFromFile::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AsyncLoadFromFile", &UEasyJsonAsyncLoadFromFile::execAsyncLoadFromFile },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics
	{
		struct EasyJsonAsyncLoadFromFile_eventAsyncLoadFromFile_Parms
		{
			UObject* WorldContextObject;
			FString FilePath;
			bool IsAblolute;
			UEasyJsonAsyncLoadFromFile* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FilePath;
		static void NewProp_IsAblolute_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsAblolute;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonAsyncLoadFromFile_eventAsyncLoadFromFile_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::NewProp_FilePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::NewProp_FilePath = { "FilePath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonAsyncLoadFromFile_eventAsyncLoadFromFile_Parms, FilePath), METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::NewProp_FilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::NewProp_FilePath_MetaData)) };
	void Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::NewProp_IsAblolute_SetBit(void* Obj)
	{
		((EasyJsonAsyncLoadFromFile_eventAsyncLoadFromFile_Parms*)Obj)->IsAblolute = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::NewProp_IsAblolute = { "IsAblolute", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EasyJsonAsyncLoadFromFile_eventAsyncLoadFromFile_Parms), &Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::NewProp_IsAblolute_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonAsyncLoadFromFile_eventAsyncLoadFromFile_Parms, ReturnValue), Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::NewProp_FilePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::NewProp_IsAblolute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "EasyJsonParser" },
		{ "ModuleRelativePath", "Public/EasyJsonAsyncLoadFromFile.h" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEasyJsonAsyncLoadFromFile, nullptr, "AsyncLoadFromFile", nullptr, nullptr, sizeof(EasyJsonAsyncLoadFromFile_eventAsyncLoadFromFile_Parms), Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_NoRegister()
	{
		return UEasyJsonAsyncLoadFromFile::StaticClass();
	}
	struct Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Successed_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_Successed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Failed_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_Failed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__JsonFile_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp__JsonFile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__IsAblolute_MetaData[];
#endif
		static void NewProp__IsAblolute_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp__IsAblolute;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintAsyncActionBase,
		(UObject* (*)())Z_Construct_UPackage__Script_EasyJsonParser,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEasyJsonAsyncLoadFromFile_AsyncLoadFromFile, "AsyncLoadFromFile" }, // 1659596708
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EasyJsonAsyncLoadFromFile.h" },
		{ "ModuleRelativePath", "Public/EasyJsonAsyncLoadFromFile.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp_Successed_MetaData[] = {
		{ "ModuleRelativePath", "Public/EasyJsonAsyncLoadFromFile.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp_Successed = { "Successed", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEasyJsonAsyncLoadFromFile, Successed), Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromFile_Result__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp_Successed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp_Successed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp_Failed_MetaData[] = {
		{ "ModuleRelativePath", "Public/EasyJsonAsyncLoadFromFile.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp_Failed = { "Failed", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEasyJsonAsyncLoadFromFile, Failed), Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromFile_Result__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp_Failed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp_Failed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp__JsonFile_MetaData[] = {
		{ "ModuleRelativePath", "Public/EasyJsonAsyncLoadFromFile.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp__JsonFile = { "_JsonFile", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEasyJsonAsyncLoadFromFile, _JsonFile), METADATA_PARAMS(Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp__JsonFile_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp__JsonFile_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp__IsAblolute_MetaData[] = {
		{ "ModuleRelativePath", "Public/EasyJsonAsyncLoadFromFile.h" },
	};
#endif
	void Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp__IsAblolute_SetBit(void* Obj)
	{
		((UEasyJsonAsyncLoadFromFile*)Obj)->_IsAblolute = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp__IsAblolute = { "_IsAblolute", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEasyJsonAsyncLoadFromFile), &Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp__IsAblolute_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp__IsAblolute_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp__IsAblolute_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp_Successed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp_Failed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp__JsonFile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::NewProp__IsAblolute,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEasyJsonAsyncLoadFromFile>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::ClassParams = {
		&UEasyJsonAsyncLoadFromFile::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEasyJsonAsyncLoadFromFile()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEasyJsonAsyncLoadFromFile_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEasyJsonAsyncLoadFromFile, 2272048366);
	template<> EASYJSONPARSER_API UClass* StaticClass<UEasyJsonAsyncLoadFromFile>()
	{
		return UEasyJsonAsyncLoadFromFile::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEasyJsonAsyncLoadFromFile(Z_Construct_UClass_UEasyJsonAsyncLoadFromFile, &UEasyJsonAsyncLoadFromFile::StaticClass, TEXT("/Script/EasyJsonParser"), TEXT("UEasyJsonAsyncLoadFromFile"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEasyJsonAsyncLoadFromFile);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

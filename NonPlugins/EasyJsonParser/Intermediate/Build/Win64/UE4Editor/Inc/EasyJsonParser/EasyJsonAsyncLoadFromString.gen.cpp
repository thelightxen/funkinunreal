// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EasyJsonParser/Public/EasyJsonAsyncLoadFromString.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEasyJsonAsyncLoadFromString() {}
// Cross Module References
	EASYJSONPARSER_API UFunction* Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromString_Result__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_EasyJsonParser();
	EASYJSONPARSER_API UClass* Z_Construct_UClass_UEasyJsonObject_NoRegister();
	EASYJSONPARSER_API UClass* Z_Construct_UClass_UEasyJsonAsyncLoadFromString_NoRegister();
	EASYJSONPARSER_API UClass* Z_Construct_UClass_UEasyJsonAsyncLoadFromString();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintAsyncActionBase();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromString_Result__DelegateSignature_Statics
	{
		struct _Script_EasyJsonParser_eventEasyJsonAsyncLoadFromString_Result_Parms
		{
			UEasyJsonObject* JsonObject;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromString_Result__DelegateSignature_Statics::NewProp_JsonObject = { "JsonObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_EasyJsonParser_eventEasyJsonAsyncLoadFromString_Result_Parms, JsonObject), Z_Construct_UClass_UEasyJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromString_Result__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromString_Result__DelegateSignature_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromString_Result__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/EasyJsonAsyncLoadFromString.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromString_Result__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_EasyJsonParser, nullptr, "EasyJsonAsyncLoadFromString_Result__DelegateSignature", nullptr, nullptr, sizeof(_Script_EasyJsonParser_eventEasyJsonAsyncLoadFromString_Result_Parms), Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromString_Result__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromString_Result__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromString_Result__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromString_Result__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromString_Result__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromString_Result__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UEasyJsonAsyncLoadFromString::execAsyncLoadFromString)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UEasyJsonAsyncLoadFromString**)Z_Param__Result=UEasyJsonAsyncLoadFromString::AsyncLoadFromString(Z_Param_WorldContextObject,Z_Param_JsonString);
		P_NATIVE_END;
	}
	void UEasyJsonAsyncLoadFromString::StaticRegisterNativesUEasyJsonAsyncLoadFromString()
	{
		UClass* Class = UEasyJsonAsyncLoadFromString::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AsyncLoadFromString", &UEasyJsonAsyncLoadFromString::execAsyncLoadFromString },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics
	{
		struct EasyJsonAsyncLoadFromString_eventAsyncLoadFromString_Parms
		{
			UObject* WorldContextObject;
			FString JsonString;
			UEasyJsonAsyncLoadFromString* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonAsyncLoadFromString_eventAsyncLoadFromString_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonAsyncLoadFromString_eventAsyncLoadFromString_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::NewProp_JsonString_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonAsyncLoadFromString_eventAsyncLoadFromString_Parms, ReturnValue), Z_Construct_UClass_UEasyJsonAsyncLoadFromString_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::NewProp_JsonString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "EasyJsonParser" },
		{ "ModuleRelativePath", "Public/EasyJsonAsyncLoadFromString.h" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEasyJsonAsyncLoadFromString, nullptr, "AsyncLoadFromString", nullptr, nullptr, sizeof(EasyJsonAsyncLoadFromString_eventAsyncLoadFromString_Parms), Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEasyJsonAsyncLoadFromString_NoRegister()
	{
		return UEasyJsonAsyncLoadFromString::StaticClass();
	}
	struct Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Successed_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_Successed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Failed_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_Failed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__JsonString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp__JsonString;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintAsyncActionBase,
		(UObject* (*)())Z_Construct_UPackage__Script_EasyJsonParser,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEasyJsonAsyncLoadFromString_AsyncLoadFromString, "AsyncLoadFromString" }, // 1634162089
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EasyJsonAsyncLoadFromString.h" },
		{ "ModuleRelativePath", "Public/EasyJsonAsyncLoadFromString.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::NewProp_Successed_MetaData[] = {
		{ "ModuleRelativePath", "Public/EasyJsonAsyncLoadFromString.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::NewProp_Successed = { "Successed", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEasyJsonAsyncLoadFromString, Successed), Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromString_Result__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::NewProp_Successed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::NewProp_Successed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::NewProp_Failed_MetaData[] = {
		{ "ModuleRelativePath", "Public/EasyJsonAsyncLoadFromString.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::NewProp_Failed = { "Failed", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEasyJsonAsyncLoadFromString, Failed), Z_Construct_UDelegateFunction_EasyJsonParser_EasyJsonAsyncLoadFromString_Result__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::NewProp_Failed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::NewProp_Failed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::NewProp__JsonString_MetaData[] = {
		{ "ModuleRelativePath", "Public/EasyJsonAsyncLoadFromString.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::NewProp__JsonString = { "_JsonString", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEasyJsonAsyncLoadFromString, _JsonString), METADATA_PARAMS(Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::NewProp__JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::NewProp__JsonString_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::NewProp_Successed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::NewProp_Failed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::NewProp__JsonString,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEasyJsonAsyncLoadFromString>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::ClassParams = {
		&UEasyJsonAsyncLoadFromString::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEasyJsonAsyncLoadFromString()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEasyJsonAsyncLoadFromString, 2632481679);
	template<> EASYJSONPARSER_API UClass* StaticClass<UEasyJsonAsyncLoadFromString>()
	{
		return UEasyJsonAsyncLoadFromString::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEasyJsonAsyncLoadFromString(Z_Construct_UClass_UEasyJsonAsyncLoadFromString, &UEasyJsonAsyncLoadFromString::StaticClass, TEXT("/Script/EasyJsonParser"), TEXT("UEasyJsonAsyncLoadFromString"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEasyJsonAsyncLoadFromString);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

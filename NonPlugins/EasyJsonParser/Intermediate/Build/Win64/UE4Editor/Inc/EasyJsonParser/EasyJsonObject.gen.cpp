// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EasyJsonParser/Public/EasyJsonObject.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEasyJsonObject() {}
// Cross Module References
	EASYJSONPARSER_API UClass* Z_Construct_UClass_UEasyJsonObject_NoRegister();
	EASYJSONPARSER_API UClass* Z_Construct_UClass_UEasyJsonObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_EasyJsonParser();
	EASYJSONPARSER_API UEnum* Z_Construct_UEnum_EasyJsonParser_EEasyJsonParserFound();
// End Cross Module References
	DEFINE_FUNCTION(UEasyJsonObject::execReadObjects)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_AccessString);
		P_GET_ENUM_REF(EEasyJsonParserFound,Z_Param_Out_Result);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UEasyJsonObject*>*)Z_Param__Result=P_THIS->ReadObjects(Z_Param_AccessString,(EEasyJsonParserFound&)(Z_Param_Out_Result));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEasyJsonObject::execReadObject)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_AccessString);
		P_GET_ENUM_REF(EEasyJsonParserFound,Z_Param_Out_Result);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UEasyJsonObject**)Z_Param__Result=P_THIS->ReadObject(Z_Param_AccessString,(EEasyJsonParserFound&)(Z_Param_Out_Result));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEasyJsonObject::execReadBool)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_AccessString);
		P_GET_UBOOL(Z_Param_DefaultValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ReadBool(Z_Param_AccessString,Z_Param_DefaultValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEasyJsonObject::execReadString)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_AccessString);
		P_GET_PROPERTY(FStrProperty,Z_Param_DefaultValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->ReadString(Z_Param_AccessString,Z_Param_DefaultValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEasyJsonObject::execReadFloat)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_AccessString);
		P_GET_PROPERTY(FFloatProperty,Z_Param_DefaultValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->ReadFloat(Z_Param_AccessString,Z_Param_DefaultValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEasyJsonObject::execReadInt)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_AccessString);
		P_GET_PROPERTY(FIntProperty,Z_Param_DefaultValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->ReadInt(Z_Param_AccessString,Z_Param_DefaultValue);
		P_NATIVE_END;
	}
	void UEasyJsonObject::StaticRegisterNativesUEasyJsonObject()
	{
		UClass* Class = UEasyJsonObject::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ReadBool", &UEasyJsonObject::execReadBool },
			{ "ReadFloat", &UEasyJsonObject::execReadFloat },
			{ "ReadInt", &UEasyJsonObject::execReadInt },
			{ "ReadObject", &UEasyJsonObject::execReadObject },
			{ "ReadObjects", &UEasyJsonObject::execReadObjects },
			{ "ReadString", &UEasyJsonObject::execReadString },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics
	{
		struct EasyJsonObject_eventReadBool_Parms
		{
			FString AccessString;
			bool DefaultValue;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccessString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AccessString;
		static void NewProp_DefaultValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_DefaultValue;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::NewProp_AccessString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::NewProp_AccessString = { "AccessString", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonObject_eventReadBool_Parms, AccessString), METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::NewProp_AccessString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::NewProp_AccessString_MetaData)) };
	void Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::NewProp_DefaultValue_SetBit(void* Obj)
	{
		((EasyJsonObject_eventReadBool_Parms*)Obj)->DefaultValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EasyJsonObject_eventReadBool_Parms), &Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::NewProp_DefaultValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EasyJsonObject_eventReadBool_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EasyJsonObject_eventReadBool_Parms), &Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::NewProp_AccessString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::NewProp_DefaultValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::Function_MetaDataParams[] = {
		{ "Category", "EasyJsonParser|ReadValue" },
		{ "CPP_Default_DefaultValue", "false" },
		{ "ModuleRelativePath", "Public/EasyJsonObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEasyJsonObject, nullptr, "ReadBool", nullptr, nullptr, sizeof(EasyJsonObject_eventReadBool_Parms), Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEasyJsonObject_ReadBool()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEasyJsonObject_ReadBool_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics
	{
		struct EasyJsonObject_eventReadFloat_Parms
		{
			FString AccessString;
			float DefaultValue;
			float ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccessString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AccessString;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::NewProp_AccessString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::NewProp_AccessString = { "AccessString", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonObject_eventReadFloat_Parms, AccessString), METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::NewProp_AccessString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::NewProp_AccessString_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonObject_eventReadFloat_Parms, DefaultValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonObject_eventReadFloat_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::NewProp_AccessString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::NewProp_DefaultValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "EasyJsonParser|ReadValue" },
		{ "CPP_Default_DefaultValue", "0.000000" },
		{ "ModuleRelativePath", "Public/EasyJsonObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEasyJsonObject, nullptr, "ReadFloat", nullptr, nullptr, sizeof(EasyJsonObject_eventReadFloat_Parms), Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEasyJsonObject_ReadFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEasyJsonObject_ReadFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics
	{
		struct EasyJsonObject_eventReadInt_Parms
		{
			FString AccessString;
			int32 DefaultValue;
			int32 ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccessString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AccessString;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::NewProp_AccessString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::NewProp_AccessString = { "AccessString", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonObject_eventReadInt_Parms, AccessString), METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::NewProp_AccessString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::NewProp_AccessString_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonObject_eventReadInt_Parms, DefaultValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonObject_eventReadInt_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::NewProp_AccessString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::NewProp_DefaultValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::Function_MetaDataParams[] = {
		{ "Category", "EasyJsonParser|ReadValue" },
		{ "CPP_Default_DefaultValue", "0" },
		{ "ModuleRelativePath", "Public/EasyJsonObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEasyJsonObject, nullptr, "ReadInt", nullptr, nullptr, sizeof(EasyJsonObject_eventReadInt_Parms), Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEasyJsonObject_ReadInt()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEasyJsonObject_ReadInt_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics
	{
		struct EasyJsonObject_eventReadObject_Parms
		{
			FString AccessString;
			EEasyJsonParserFound Result;
			UEasyJsonObject* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccessString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AccessString;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Result_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::NewProp_AccessString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::NewProp_AccessString = { "AccessString", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonObject_eventReadObject_Parms, AccessString), METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::NewProp_AccessString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::NewProp_AccessString_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::NewProp_Result_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonObject_eventReadObject_Parms, Result), Z_Construct_UEnum_EasyJsonParser_EEasyJsonParserFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonObject_eventReadObject_Parms, ReturnValue), Z_Construct_UClass_UEasyJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::NewProp_AccessString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::NewProp_Result_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "EasyJsonParser|ReadValue" },
		{ "ExpandEnumAsExecs", "Result" },
		{ "ModuleRelativePath", "Public/EasyJsonObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEasyJsonObject, nullptr, "ReadObject", nullptr, nullptr, sizeof(EasyJsonObject_eventReadObject_Parms), Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEasyJsonObject_ReadObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEasyJsonObject_ReadObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics
	{
		struct EasyJsonObject_eventReadObjects_Parms
		{
			FString AccessString;
			EEasyJsonParserFound Result;
			TArray<UEasyJsonObject*> ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccessString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AccessString;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Result_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::NewProp_AccessString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::NewProp_AccessString = { "AccessString", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonObject_eventReadObjects_Parms, AccessString), METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::NewProp_AccessString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::NewProp_AccessString_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::NewProp_Result_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonObject_eventReadObjects_Parms, Result), Z_Construct_UEnum_EasyJsonParser_EEasyJsonParserFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UEasyJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonObject_eventReadObjects_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::NewProp_AccessString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::NewProp_Result_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::Function_MetaDataParams[] = {
		{ "Category", "EasyJsonParser|ReadValue" },
		{ "ExpandEnumAsExecs", "Result" },
		{ "ModuleRelativePath", "Public/EasyJsonObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEasyJsonObject, nullptr, "ReadObjects", nullptr, nullptr, sizeof(EasyJsonObject_eventReadObjects_Parms), Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEasyJsonObject_ReadObjects()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEasyJsonObject_ReadObjects_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics
	{
		struct EasyJsonObject_eventReadString_Parms
		{
			FString AccessString;
			FString DefaultValue;
			FString ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccessString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AccessString;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::NewProp_AccessString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::NewProp_AccessString = { "AccessString", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonObject_eventReadString_Parms, AccessString), METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::NewProp_AccessString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::NewProp_AccessString_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonObject_eventReadString_Parms, DefaultValue), METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EasyJsonObject_eventReadString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::NewProp_AccessString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::NewProp_DefaultValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::Function_MetaDataParams[] = {
		{ "Category", "EasyJsonParser|ReadValue" },
		{ "CPP_Default_DefaultValue", "" },
		{ "ModuleRelativePath", "Public/EasyJsonObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEasyJsonObject, nullptr, "ReadString", nullptr, nullptr, sizeof(EasyJsonObject_eventReadString_Parms), Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEasyJsonObject_ReadString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEasyJsonObject_ReadString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEasyJsonObject_NoRegister()
	{
		return UEasyJsonObject::StaticClass();
	}
	struct Z_Construct_UClass_UEasyJsonObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEasyJsonObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_EasyJsonParser,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEasyJsonObject_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEasyJsonObject_ReadBool, "ReadBool" }, // 747504196
		{ &Z_Construct_UFunction_UEasyJsonObject_ReadFloat, "ReadFloat" }, // 547841961
		{ &Z_Construct_UFunction_UEasyJsonObject_ReadInt, "ReadInt" }, // 1246133655
		{ &Z_Construct_UFunction_UEasyJsonObject_ReadObject, "ReadObject" }, // 3510805213
		{ &Z_Construct_UFunction_UEasyJsonObject_ReadObjects, "ReadObjects" }, // 3261050953
		{ &Z_Construct_UFunction_UEasyJsonObject_ReadString, "ReadString" }, // 3308336870
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEasyJsonObject_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "EasyJsonObject.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/EasyJsonObject.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEasyJsonObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEasyJsonObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEasyJsonObject_Statics::ClassParams = {
		&UEasyJsonObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEasyJsonObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEasyJsonObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEasyJsonObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEasyJsonObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEasyJsonObject, 290552459);
	template<> EASYJSONPARSER_API UClass* StaticClass<UEasyJsonObject>()
	{
		return UEasyJsonObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEasyJsonObject(Z_Construct_UClass_UEasyJsonObject, &UEasyJsonObject::StaticClass, TEXT("/Script/EasyJsonParser"), TEXT("UEasyJsonObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEasyJsonObject);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

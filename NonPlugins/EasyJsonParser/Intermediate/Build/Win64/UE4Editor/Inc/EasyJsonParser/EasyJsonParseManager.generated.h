// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EEasyJsonParserErrorCode : uint8;
class UEasyJsonObject;
#ifdef EASYJSONPARSER_EasyJsonParseManager_generated_h
#error "EasyJsonParseManager.generated.h already included, missing '#pragma once' in EasyJsonParseManager.h"
#endif
#define EASYJSONPARSER_EasyJsonParseManager_generated_h

#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_SPARSE_DATA
#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execLoadFromString); \
	DECLARE_FUNCTION(execLoadFromFile);


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execLoadFromString); \
	DECLARE_FUNCTION(execLoadFromFile);


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEasyJsonParseManager(); \
	friend struct Z_Construct_UClass_UEasyJsonParseManager_Statics; \
public: \
	DECLARE_CLASS(UEasyJsonParseManager, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EasyJsonParser"), NO_API) \
	DECLARE_SERIALIZER(UEasyJsonParseManager)


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUEasyJsonParseManager(); \
	friend struct Z_Construct_UClass_UEasyJsonParseManager_Statics; \
public: \
	DECLARE_CLASS(UEasyJsonParseManager, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EasyJsonParser"), NO_API) \
	DECLARE_SERIALIZER(UEasyJsonParseManager)


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEasyJsonParseManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEasyJsonParseManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEasyJsonParseManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEasyJsonParseManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEasyJsonParseManager(UEasyJsonParseManager&&); \
	NO_API UEasyJsonParseManager(const UEasyJsonParseManager&); \
public:


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEasyJsonParseManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEasyJsonParseManager(UEasyJsonParseManager&&); \
	NO_API UEasyJsonParseManager(const UEasyJsonParseManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEasyJsonParseManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEasyJsonParseManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEasyJsonParseManager)


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_PRIVATE_PROPERTY_OFFSET
#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_13_PROLOG
#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_PRIVATE_PROPERTY_OFFSET \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_SPARSE_DATA \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_RPC_WRAPPERS \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_INCLASS \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_PRIVATE_PROPERTY_OFFSET \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_SPARSE_DATA \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_INCLASS_NO_PURE_DECLS \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> EASYJSONPARSER_API UClass* StaticClass<class UEasyJsonParseManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonParseManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UEasyJsonObject;
class UObject;
class UEasyJsonAsyncLoadFromString;
#ifdef EASYJSONPARSER_EasyJsonAsyncLoadFromString_generated_h
#error "EasyJsonAsyncLoadFromString.generated.h already included, missing '#pragma once' in EasyJsonAsyncLoadFromString.h"
#endif
#define EASYJSONPARSER_EasyJsonAsyncLoadFromString_generated_h

#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_10_DELEGATE \
struct _Script_EasyJsonParser_eventEasyJsonAsyncLoadFromString_Result_Parms \
{ \
	UEasyJsonObject* JsonObject; \
}; \
static inline void FEasyJsonAsyncLoadFromString_Result_DelegateWrapper(const FMulticastScriptDelegate& EasyJsonAsyncLoadFromString_Result, UEasyJsonObject* JsonObject) \
{ \
	_Script_EasyJsonParser_eventEasyJsonAsyncLoadFromString_Result_Parms Parms; \
	Parms.JsonObject=JsonObject; \
	EasyJsonAsyncLoadFromString_Result.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_SPARSE_DATA
#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAsyncLoadFromString);


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAsyncLoadFromString);


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEasyJsonAsyncLoadFromString(); \
	friend struct Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics; \
public: \
	DECLARE_CLASS(UEasyJsonAsyncLoadFromString, UBlueprintAsyncActionBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EasyJsonParser"), NO_API) \
	DECLARE_SERIALIZER(UEasyJsonAsyncLoadFromString)


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUEasyJsonAsyncLoadFromString(); \
	friend struct Z_Construct_UClass_UEasyJsonAsyncLoadFromString_Statics; \
public: \
	DECLARE_CLASS(UEasyJsonAsyncLoadFromString, UBlueprintAsyncActionBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EasyJsonParser"), NO_API) \
	DECLARE_SERIALIZER(UEasyJsonAsyncLoadFromString)


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEasyJsonAsyncLoadFromString(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEasyJsonAsyncLoadFromString) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEasyJsonAsyncLoadFromString); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEasyJsonAsyncLoadFromString); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEasyJsonAsyncLoadFromString(UEasyJsonAsyncLoadFromString&&); \
	NO_API UEasyJsonAsyncLoadFromString(const UEasyJsonAsyncLoadFromString&); \
public:


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEasyJsonAsyncLoadFromString(UEasyJsonAsyncLoadFromString&&); \
	NO_API UEasyJsonAsyncLoadFromString(const UEasyJsonAsyncLoadFromString&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEasyJsonAsyncLoadFromString); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEasyJsonAsyncLoadFromString); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEasyJsonAsyncLoadFromString)


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO___JsonString() { return STRUCT_OFFSET(UEasyJsonAsyncLoadFromString, _JsonString); }


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_13_PROLOG
#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_PRIVATE_PROPERTY_OFFSET \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_SPARSE_DATA \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_RPC_WRAPPERS \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_INCLASS \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_PRIVATE_PROPERTY_OFFSET \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_SPARSE_DATA \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_INCLASS_NO_PURE_DECLS \
	FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> EASYJSONPARSER_API UClass* StaticClass<class UEasyJsonAsyncLoadFromString>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FunkinUnreal_Plugins_EasyJsonParser_Source_EasyJsonParser_Public_EasyJsonAsyncLoadFromString_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
